#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh 
source Universals.sh

DAS="DASs/2NI_PC-VoSv/MHDring-TM"

DeviceList="$DAS"




function PostDischargeAnalysis()
{
#    GeneralDAScommunication $DAS RawDataAcquiring
     
#    ln -s ../../../Devices/$DAS DAS_raw_data_dir
    ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir

    
    for i in `seq 2 17`; do awk '{print $1","'\$$i'}' DAS_raw_data_dir/ToPlot.lvm > ring_$((i-1)).csv; done;
    #Analysis
    notebook='null'
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox
}





function Analysis
{
local ShotNo=$(<$BasePath/shot_no)
local Vacuum_shot=$(<$BasePath/Operation/Discharge/Parameters/vacuum_shot)

if [ -e $BasePath/Production/Tags/b_plasma-NO ]; then return;fi

cp Script.m Script.sf
sed "s/ShotNo = 0/ShotNo = $ShotNo/g;s/vacuum_shot = 0/vacuum_shot = $Vacuum_shot/g" Script.sf > Script.m 
matlab -nosplash -nodesktop -r Script_publish

for fig in offset.png MHD_activity.png Spectrogram.png 	q.png offset.png animation.gif[0] pictures.png;do
    convert -resize 200 $fig ${fig%%.*}-icon.png  
done    

cp pictures-icon.png graph.png
#html workarround
cp offset.png Script_01.png
cp q.png Script_02.png
cp Spectrogram.png Script_03.png
convert 'animation.gif[0]' Script_04.png
cp MHD_activity.png Script_05.png
cp pictures.png Script_06.png
cp pictures.png icon-fig.png
}



















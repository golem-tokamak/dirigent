# -*- coding: utf-8 -*-
import csv
import numpy as np
import time
import subprocess, sys

iteration='test'

"""Funkce čtení dat z tokamaku"""
def get_GOLEM_time():
    #time.sleep(60) #sekundy
    # setup url
    url = "http://golem.fjfi.cvut.cz/shots/0/Diagnostics/PlasmaDetection/Results/t_plasma_duration"
    # Try to load if webpage/parameter value does not exists 0 is assumed
    
    number = np.loadtxt(url)
    return number
   
  
def tokamak_controller(x1,x2,x3,x4):
    tokamak_command = f"""./Dirigent.sh --discharge --operation.discharge "style='standard',vacuum_shot='-1'" --infrastructure.bt_ecd "U_Bt={x2},t_Bt=0,O_Bt='CW',U_cd={x1},t_cd={x3},O_cd='CW'" --infrastructure.workinggas "S_mode='U_v',S_gas='H',p_H=10,U_v={x4}" --diagnostics.fastcameras "fr_ux100a=1200,height_ux100a=56,recrate_ux100a=40000,shotdur_ux100a=30,width_ux100a=1280,recrate_ux50a=2000" --comment 'fyzikalni_seminar_ba{iteration}'"""

    #kód pro tokamak
    #print(tokamak_command)
    subprocess.run(tokamak_command, shell = True, executable="/bin/bash")
    time = get_GOLEM_time()
    return time


res=tokamak_controller(400,800,1000,17)
print(res)

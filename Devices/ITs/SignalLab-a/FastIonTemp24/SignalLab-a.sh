
#!/bin/bash

BasePath=../../../;source $BasePath/Commons.sh



function GetReadyTheDischarge ()
{
    mkdir -p Parameters
    # GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh

    python3 - <<'END_SCRIPT' > GetReadyTheDischarge.log 2> GetReadyTheDischarge.err
import redpitaya_scpi as scpi

#? Possible waveforms:

IP = 'signallab-a.golem'
rp_s = scpi.scpi(IP)

# Reset Generation and Acquisition
rp_s.tx_txt('GEN:RST')
rp_s.tx_txt('ACQ:RST')
rp_s.check_error()


## size in samples 16Bit
DATA_SIZE = int((1024 * 1024 * .7) / 2)          # ((1024 * 1024 * 128) / 2)        ## for 128 MB ##

start_address = int(rp_s.txrx_txt('ACQ:AXI:START?'))
size = int(rp_s.txrx_txt('ACQ:AXI:SIZE?'))
start_address2 = round(start_address + size/2)
samples = (size // 2) # 1 sample = 16 Bit
rp_s.check_error()


if DATA_SIZE > size//2:
    print(f"{DATA_SIZE=} {size=}")
    print('DATA_SIZE too big')
    exit(1)
    
# Set units
rp_s.tx_txt('ACQ:AXI:DATA:UNITS VOLTS')
rp_s.tx_txt('ACQ:DATA:FORMAT BIN')
print('ACQ:AXI:DATA:UNITS?: ',rp_s.txrx_txt('ACQ:AXI:DATA:UNITS?'))
print('ACQ:DATA:FORMAT BIN?: ',rp_s.txrx_txt('ACQ:DATA:FORMAT?'))
rp_s.check_error()

# Set trigger delay for both channels
rp_s.tx_txt(f"ACQ:AXI:SOUR1:Trig:Dly {DATA_SIZE}")
rp_s.check_error()

rp_s.tx_txt(f"ACQ:AXI:SOUR2:Trig:Dly {DATA_SIZE}")
rp_s.check_error()

# Set-up the Channel 1 and channel 2 buffers to each work with half the available memory space.
rp_s.tx_txt(f"ACQ:AXI:SOUR1:SET:Buffer {start_address},{size/2}")
rp_s.tx_txt(f"ACQ:AXI:SOUR2:SET:Buffer {start_address2},{size/2}")

# Enable DMA
rp_s.tx_txt('ACQ:AXI:SOUR1:ENable ON')
rp_s.tx_txt('ACQ:AXI:SOUR2:ENable ON')
rp_s.check_error()
print('Enable CHA and CHB\n')


##### Acqusition #####

# Set coupling
rp_s.tx_txt('ACQ:SOUR1:COUP DC')
rp_s.tx_txt('ACQ:SOUR2:COUP DC')

# Set gain
rp_s.tx_txt('ACQ:SOUR1:GAIN LV')
rp_s.tx_txt('ACQ:SOUR2:GAIN LV')
rp_s.check_error()

rp_s.tx_txt('ACQ:AXI:DEC 16')
print('ACQ:AXI:DEC?: ',rp_s.txrx_txt('ACQ:AXI:DEC?'))
rp_s.check_error()

rp_s.tx_txt("ACQ:AVG ON")

rp_s.tx_txt('TRig:EXT:LEV 3')
rp_s.tx_txt('ACQ:TRig:DLY 0')
rp_s.check_error()

rp_s.close()
END_SCRIPT
}

function Arming()
{

    python3 - <<'END_SCRIPT'
import time
import redpitaya_scpi as scpi

IP = 'signallab-a.golem'
rp_s = scpi.scpi(IP)


rp_s.tx_txt('ACQ:START')
rp_s.tx_txt('ACQ:TRig EXT_PE')
rp_s.check_error()

time.sleep(1)


rp_s.close()
END_SCRIPT

}


function RawDataAcquiring()
{ 
   
   python3 - <<'END_SCRIPT' > RawDataAcquiring.log 2> RawDataAcquiring.err


import redpitaya_scpi as scpi
import numpy as np
import pandas as pd
from scipy.signal import decimate
import matplotlib.pyplot as plt
import time

IP = 'signallab-a.golem'
rp_s = scpi.scpi(IP)

## size in samples 16Bit
DATA_SIZE = int((1024 * 1024 * .7) / 2)          # ((1024 * 1024 * 128) / 2)        ## for 128 MB ##
READ_DATA_SIZE =  1024 * 10


# Wait for trigger
while 1:
    rp_s.tx_txt("ACQ:TRig:STAT?")
    if rp_s.rx_txt() == 'TD':
        print("Triggered")
        time.sleep(1)
        break

# wait for fill adc buffer
while 1:
    rp_s.tx_txt('ACQ:AXI:SOUR1:TRIG:FILL?')
    if rp_s.rx_txt() == '1':
        print('DMA buffer full\n')
        break

### PostDischarge Analysis

# Stop Acquisition
rp_s.tx_txt('ACQ:STOP')
rp_s.check_error()

# Get Decimaiton factor
decimation_factor = int(rp_s.txrx_txt('ACQ:AXI:DEC?'))
print(f"{decimation_factor=}")


# Set units
rp_s.tx_txt('ACQ:AXI:DATA:UNITS VOLTS')
rp_s.tx_txt('ACQ:DATA:FORMAT BIN')
#print('ACQ:AXI:DATA:UNITS?: ',rp_s.txrx_txt('ACQ:AXI:DATA:UNITS?'))
#print('ACQ:DATA:FORMAT BIN?: ',rp_s.txrx_txt('ACQ:DATA:FORMAT?'))
rp_s.check_error()

## Get write pointer at trigger location
posChA = int(rp_s.txrx_txt('ACQ:AXI:SOUR1:Trig:Pos?'))
posChB = int(rp_s.txrx_txt('ACQ:AXI:SOUR2:Trig:Pos?'))
rp_s.check_error()

start_address = int(rp_s.txrx_txt('ACQ:AXI:START?'))
size = int(rp_s.txrx_txt('ACQ:AXI:SIZE?'))
start_address2 = round(start_address + size/2)
samples = (size // 2) # 1 sample = 16 Bit
rp_s.check_error()

## Read & plot

dtype = np.dtype("float32")
dtype = dtype.newbyteorder('>')

received_size = 0
data_ch1 = []
data_ch2 = []
while received_size < DATA_SIZE:
    if (received_size + READ_DATA_SIZE) > DATA_SIZE:
        block_size = samples - received_size

    rp_s.tx_txt('ACQ:AXI:SOUR1:DATA:Start:N? ' + str(posChA)+',' + str(READ_DATA_SIZE))
    # try three times 
    # it may take some time before data is loaded? 
    while True:
        buff_byte = rp_s.rx_arb()
        if not isinstance(buff_byte, bool):
            break
    
    
    buff = np.frombuffer(buff_byte, dtype=dtype)
    data_ch1 = np.append(data_ch1, buff)
    
    rp_s.tx_txt('ACQ:AXI:SOUR2:DATA:Start:N? ' + str(posChA)+',' + str(READ_DATA_SIZE))
    for i in range(3):
        buff_byte = rp_s.rx_arb()
        if not isinstance(buff_byte, bool):
            break
    buff = np.frombuffer(buff_byte, dtype=dtype)
    data_ch2 = np.append(data_ch2, buff)
    
    posChA += READ_DATA_SIZE
    posChA = posChA % samples
    received_size += READ_DATA_SIZE
        

## Close connection with Red Pitaya
rp_s.tx_txt('ACQ:AXI:SOUR1:ENable OFF')
rp_s.tx_txt('ACQ:AXI:SOUR2:ENable OFF')
#rp_s.check_error()


#print('resetting device')
# Reset Generation and Acquisition
rp_s.tx_txt('ACQ:STOP')
rp_s.tx_txt('OUTPUT:STATE OFF')
#rp_s.check_error()

# Set gain to HV just to be sure nothing is fried
rp_s.tx_txt('ACQ:SOUR1:GAIN HV')
rp_s.tx_txt('ACQ:SOUR2:GAIN HV')

rp_s.close()


t_end = (1/250e6) * len(data_ch1) * decimation_factor
t_vec = np.linspace(0,t_end, len(data_ch1))


# Combine decimated data into a Pandas DataFrame
df_data = pd.DataFrame({
    'time': t_vec,
    'ch1': data_ch1,
    'ch2': data_ch2
})

# Save the decimated data to a CSV file
csv_file = "data.csv"
df_data.to_csv(csv_file, index=False)

# Plot ch0 and ch1
plt.figure(figsize=(10, 6))
plt.plot(t_vec*1e3, data_ch1, label="Channel 1", alpha = .75)
plt.plot(t_vec*1e3, data_ch2, label="Channel 2", alpha = .75)
plt.xlabel("Time (ms)")
plt.ylabel("Amplitude [V]")
plt.legend()
plt.grid(True)
plt.savefig('ScreenShotAll.png')
END_SCRIPT

convert -resize $icon_size ScreenShotAll.png rawdata.jpg
 
}


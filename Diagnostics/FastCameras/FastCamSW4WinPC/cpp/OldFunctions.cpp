/** @file OldFunctions.cpp
    @brief unused functions
    @note not tested properly
*/

#include "FastCamGOLEM.h"


//------------------- Overall Settings ------------------------
void SetAll_old(vector<camera>& cams) {
    unsigned long nRet, nErrorCode, Rate, Width, Height, ShutterValue;

    for (int i = 0; i < cams.size(); i++) {
        //TODO: u vsech by mozna bylo lepsi vypisovat nastavenou hodnotu mimo if else
        //---Frame rate---
        nRet = PDC_SetRecordRate(cams[i].dev, cams[i].child, cams[i].recordRate, &nErrorCode);
        checkStatus("PDC_SetRecordRate", nRet, nErrorCode);
        //Check it:
        nRet = PDC_GetRecordRate(cams[i].dev, cams[i].child, &Rate, &nErrorCode);
        checkStatus("PDC_GetRecordRate", nRet, nErrorCode);
        if (Rate != cams[i].recordRate) { cout << "Record rate was set wrong! Rate = " << Rate << endl; exit(0); }
        else { cout << "Record rate was set to: " << cams[i].recordRate << endl; }//TODO: exit by mohl zpusobovat chybu

        //---Resolution---
        nRet = PDC_SetResolution(cams[i].dev, cams[i].child, cams[i].width, cams[i].height, &nErrorCode);
        checkStatus("PDC_SetResolution", nRet, nErrorCode);
        //Check it:
        nRet = PDC_GetResolution(cams[i].dev, cams[i].child, &Width, &Height, &nErrorCode);
        checkStatus("PDC_GetResolution", nRet, nErrorCode);
        if ((Width != cams[i].width) && (Height != cams[i].height)) { cout << "Resolution was set wrong! "; exit(0); }
        else { cout << "Resolution was set to: Height = " << Height << " and Width = " << Width << endl; }//TODO: exit by mohl zpusobovat chybu

        //---Shutter speed---
        //in fractions of a second, i.e. 1/(shutterValue)
        nRet = PDC_SetShutterSpeedFps(cams[i].dev, cams[i].child, cams[i].shutterSpeed, &nErrorCode);
        checkStatus("PDC_SetShutterSpeedFps", nRet, nErrorCode);
        //Check it:
        //TODO: jednotky...
        nRet = PDC_GetShutterSpeedFps(cams[i].dev, cams[i].child, &ShutterValue, &nErrorCode);
        checkStatus("PDC_GetShutterSpeedFps", nRet, nErrorCode);
        if (ShutterValue != cams[i].shutterSpeed) { cout << "Shutter speed was set wrong! "; exit(0); }
        else { cout << "Resolution was set to: 1/ShutteValue = " << 1 / ShutterValue << endl; }//TODO: exit by mohl zpusobovat chybu


    }
}


void SetAll(vector<camera>& cams) {
    unsigned long nRet, nErrorCode;
    unsigned long Rate, Width, Height, ShutterValue;
    unsigned long List[PDC_MAX_LIST_NUMBER], size;
    unsigned long list;
    for (int i = 0; i < cams.size(); i++) {

        //FrameRate       
        nRet = PDC_GetRecordRateList(cams[i].dev, cams[i].child, &size, List, &nErrorCode);
        checkStatus("PDC_GetRecordRateList", nRet, nErrorCode);
        cout << "List in FrameRate" << List[5] << endl;
        cams[i].recordRate = getClosestValue(cams[i].recordRate, List);
        nRet = PDC_SetRecordRate(cams[i].dev, cams[i].child, cams[i].recordRate, &nErrorCode);
        checkStatus("PDC_SetRecordRate", nRet, nErrorCode);

        nRet = PDC_GetRecordRate(cams[i].dev, cams[i].child, &Rate, &nErrorCode);
        checkStatus("PDC_GetRecordRate", nRet, nErrorCode);
        cout << "Record rate is set to: " << Rate << endl;

        //ShutterSpeed (in fps)
        nRet = PDC_GetShutterSpeedFpsList(cams[i].dev, cams[i].child, &size, List, &nErrorCode);
        checkStatus("PDC_GetShutterSpeedFPSList", nRet, nErrorCode);

        nRet = PDC_SetShutterSpeedFps(cams[i].dev, cams[i].child, cams[i].shutterSpeed, & nErrorCode);
        checkStatus("PDC_SetShutterSpeedFPS", nRet, nErrorCode);

        nRet = PDC_GetShutterSpeedFps(cams[i].dev, cams[i].child, &ShutterValue, &nErrorCode);
        checkStatus("PDC_GetShutterSpeedFps", nRet, nErrorCode);
        cout << "Resolution is set to: 1/ShutterValue = " << "1 / " << ShutterValue << endl;


        //Resolution
        nRet = PDC_GetResolutionList(cams[i].dev, cams[i].child, &size, List, &nErrorCode);
        checkStatus("PDC_GetResolutionList", nRet, nErrorCode);

        cout << "size:" << size << endl;
        //for (int i = 0; i < 20; i++) { cout << List[i] << endl; }


        /** @todo Dodelat a otestovat!!! */
        unsigned long widthList[16];
        unsigned long heightList[16];

 /*       for (int i = 0; i < 16; i++) { widthList[i] = List[i]; cout << "width from list" << widthList[i] << endl; }
        for (int i = 16; i < 32; i++) { heightList[i] = List[i]; cout << "height from list" << heightList[i] << endl; }
        cams[i].width = getClosestValue(cams[i].width, widthList);
        cams[i].height = getClosestValue(cams[i].height, heightList);*/

        nRet = PDC_SetResolution(cams[i].dev, cams[i].child, cams[i].width, cams[i].height, &nErrorCode);
        checkStatus("PDC_SetResolution", nRet, nErrorCode);

        nRet = PDC_GetResolution(cams[i].dev, cams[i].child, &Width, &Height, &nErrorCode);
        checkStatus("PDC_GetResolution", nRet, nErrorCode);
        cout << "Resolution is set to: Height = " << Height << " and Width = " << Width << endl;


        //to be sure
        //ShutterSpeed (in fps)
        nRet = PDC_GetShutterSpeedFpsList(cams[i].dev, cams[i].child, &size, List, &nErrorCode);
        checkStatus("PDC_GetShutterSpeedFPSList", nRet, nErrorCode);

        nRet = PDC_SetShutterSpeedFps(cams[i].dev, cams[i].child, cams[i].shutterSpeed, &nErrorCode);
        checkStatus("PDC_SetShutterSpeedFPS", nRet, nErrorCode);

        nRet = PDC_GetShutterSpeedFps(cams[i].dev, cams[i].child, &ShutterValue, &nErrorCode);
        checkStatus("PDC_GetShutterSpeedFps", nRet, nErrorCode);
        cout << "Resolution is set to: 1/ShutterValue = " << "1 / " << ShutterValue << endl;

    }
}
/** @file FastCamGOLEM.h 
*/

#ifndef FASTCAMGOLEM_H_INCLUDED
#define FASTCAMGOLEM_H_INCLUDED

#include <iostream>
#include <Windows.h>
#include <sstream>
#include <string>
#include <climits>
#include <fstream>
#include <iomanip>
#include <vector>


#include "PDCLIB.h"
#include "nlohmann/json.hpp"


using namespace std;
using json = nlohmann::json;

struct camera {
    //for operation
    unsigned long dev;
    unsigned long child;
    unsigned long deviceCode; //typ kamery
    unsigned long status;
    bool isOpened;
    bool detected;


    //configuration (JSON) related
    string name;
    unsigned long ipAddr;
    unsigned long width;
    unsigned long height;
    unsigned long framesToSave;
    unsigned long recordRate; //v Hz (fps)
    unsigned long shutterSpeed; //v Hz (fps)
    unsigned long shotDurToSave;
    unsigned long variableChannel;

    string BMPdataFolderPath;
    string videoFileName;

    std::pair<std::string, unsigned long> TriggerMode;
    std::pair<std::string, unsigned long> input1;
    std::pair<std::string, unsigned long> input2;
    std::pair<std::string, unsigned long> SettingsMode;


};

//JSON
/** The function is typically used once and the created file is then edited "by hand".
*   @brief Creates new JSON file (Camsconfig.json) with parameters strictly defined inside this function.  
*   @attention If the json file already exists, it is overwritten without warning.*/
void createJSON(void);

/** Overwrites parameters (such as "framesToSave", "shotDurToSave", "width", "height", "recordRate", "shutterSpeed") that may have been changed during processing.
*   @brief Writes into existing JSON file.
*/
void outputJSON(vector<camera>& cams);


//Additional
string translate_error(unsigned long nErrorCode);
void checkStatus(string functionName, unsigned long nRet, unsigned long nErrorCode, bool addException = false);
unsigned long IPtoLong(string str);
string longToIP(unsigned long num);
int getClosestValue(int desiredValue, unsigned long* List);
void printCamsPar(vector<camera>& cams);

//init
/** PDCLIB is the official external camera library that is necessary for their operation.
* @brief Initializes PDCLIB.
*/
void init(void);


//OpenDevice
/** @brief Opens the target devices.
*   @param cams selected cameras
*/
void OpenDevices(vector<camera>& cams);


//Settings
/** @brief Returns the current status of the camera.
*   @param cam selected camera 
*   @return status
*/
unsigned long GetStatus(camera& cam);

/** @brief Sets the desired camera status.
*   @param cam selected camera 
*/
void SetStatus(vector<camera>& cams, unsigned long DesiredStatus);
/** Variable channel is ....
*   @brief Sets the required variable channel.
*   @param cams selected cameras 
*/
void SetVariableChannel(vector<camera>& cams);

/** 
*   It uses parameters from CamsConfig.json and store it in variable channel 3. (Channel 3 was chosen because channels 1 and 2 were normally used when using PFV4.)
*   @brief Creates and sets new variable channel. 
*   @param cams selected cameras 
*/
void CreateSetVariableChannel(vector<camera>& cams);

/** I/O can be eg. 
*   @brief Sets the required external input/output.
*   @param cams selected cameras
*/
void SetExternal_IO_signal(vector<camera>& cams);

/** 
*   Unlike setAll_old, input value correction is/will be implemented here. 
*   @brief Sets frame rate, shutter speed, resolution, ...
*   @param cam selected camera
*   @todo Zajistit, aby se nastavene hodnoty nevylucovaly
*   @warning still under construction
*/
void SetAll(vector<camera>& cams);
/** 
*   @brief Sets frame rate, shutter speed, resolution, ...
*   @param cam selected camera
*/

void SetAll_old(vector<camera>& cams);
// void checkParam(vector<camera>& cams);


//Recording
/** The function is composed of several parts: 
* \n    Trigger setting = 
* \n    Start recording = 
*   @brief Captures images.
*   @param cams selected cameras
*/
void recording(vector<camera>& cams);

//getData
//wchar_t* convertCharArrayToLPCWSTR(const char* charArray);
//LPCWSTR convertCharArrayToLPCWSTR(const char* charArray);
wstring convertCharArrayToLPCWSTR(const char* charArray);

void SetTransfer(vector<camera>& cams);
void getRawData(camera& cam);
void SaveBMP(vector<camera>& cams);
void SaveAVI(vector<camera>& cams);

void checkNumFrameToSave(vector<camera>& cams);


#endif // FASTCAMGOLEM_H_INCLUDED
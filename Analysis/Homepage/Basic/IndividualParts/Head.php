<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">    
    <!--<title>Golem No: <?php echo ($shot_no);?> - the only fully opensource tokamak </title>-->
    <title>Golem #<?php echo ($shot_no);?></title>
    <link rel="shortcut icon" href="http://golem.fjfi.cvut.cz/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/_static/default.css" type="text/css">
    <link rel="stylesheet" href="/_static/pygments.css" type="text/css">
    <link rel="stylesheet" href="/_static/lightbox.css" type="text/css" media="screen">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" async
  src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/latest.js?config=TeX-MML-AM_CHTML">
</script>
<script type="text/javascript">
	function getURL(val){
// 	location = /shots/ + val + '/' +  "/";
	location = /shots/ + val;
	return false;
	}
</script>
<script>
if(typeof(EventSource) !== "undefined") {
  var source = new EventSource("checker.php");
  source.onmessage = function(event) {
    document.getElementById("main").innerHTML = event.data + "<br>";
    if (document.getElementById("main").innerHTML.indexOf("file changed") != -1) {
        window.location.reload();
     }
    if (document.getElementById("main").innerHTML.indexOf("new shot available") != -1 && document.getElementById("myCheck").checked == true) {
        // window.location.replace("../0/index.html");
        window.location.replace("http://golem.fjfi.cvut.cz/shots/0");
    }
  };
} else {
  document.getElementById("main").innerHTML = "Sorry, your browser does not support server-sent events...";
}

function myFunction() {
  var checkBox = document.getElementById("myCheck");
  var text = document.getElementById("text");
  if (checkBox.checked == true){
    text.style.display = "block";
  } else {
    text.style.display = "none";
  }
}

function copyToClipboard() {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      var text = xhr.responseText.trim();
      var textarea = document.createElement("textarea");
      document.body.appendChild(textarea);
      textarea.value = text;
      textarea.select();
      document.execCommand("copy");
      document.body.removeChild(textarea);
    }
  };
  xhr.open("GET", "Production/Parameters/CommandLine", true);
  xhr.send();
}
</script>    
    

<style>
    tr.data-flow th { text-align: right; font-style: italic; }
</style>
  </head>

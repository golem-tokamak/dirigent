#/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

PC=Nb-Master

notebook=RawDataGraph

HotFix=svoboda@Nb-Master

#function Arming #Nemuze byt, nahozeni trva moc dlouho ...
function GetReadyTheDischarge
{
    scp Setting.json golem@$PC:/home/golem/MiniSpectrometers/
    ssh golem@$PC "cd /home/golem/MiniSpectrometers;rm *.h5;./SpectrometerControlApp.ex -i "Setting.json" -o "SettingOut.json" -s 0 -a > DumpedCommunication.txt" &
}


function RawDataAcquiring
{
    #scp golem@$PC:/home/golem/MiniSpectrometers/VIS_TG_0.h5 Spectrometer_vis_0.h5
    scp golem@$PC:/home/golem/MiniSpectrometers/*.h5 .
    scp golem@$PC:/home/golem/MiniSpectrometers/SettingOut.json .
    scp golem@$PC:/home/golem/MiniSpectrometers/DumpedCommunication.txt .
    cat /golem/shm_golem/shot_no > ShotNo

}

function PostDischargeAnalysis
{
:
}

function GraphZMB()
{

    jupyter-nbconvert --execute $notebook.ipynb --to html --output $notebook.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    
}

# Version SW trigger ssh golem@$PC "cd /home/golem/MiniSpectrometers;./SpectrometerControlApp.ex -i "Setting.json" -o "SettingOut.json" -v -s 0 -t sw -a > DumpedCommunication.txt"

#!/bin/bash
shot_no=0

/home/golem/venvs/tomography/bin/jupyter-nbconvert --ExecutePreprocessor.timeout=3600 --to html --execute minfisher_golem_Abacus.ipynb --output tomography_analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

ffmpeg -i TomographyResult.avi TomographyResult.mp4

scp tomography_analysis.html golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/
scp TomographyResult.mp4 golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/
scp tomo_iconfig.png golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/
scp tomo_ScreenShotAll.png golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/tomo_ScreenShotAll.png
scp jup-nb_stderr.log golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/
scp jup-nb_stdout.log golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/
scp -r results golem@golem.fjfi.cvut.cz:/golem/shots/$shot_no/Diagnostics/FastCameras/

rm -rf /home/golem/FastCameras/$shot_no/
	#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
DAS="ITs/SignalLab-a/FastIonTemp24"
source Universals.sh

#With FG:
DeviceList="$DAS Tools/ElectricalManipulator-a/Basic AVs/D-Linkcamera-a/ManipulatorVert AVs/D-Linkcamera-b/ManipulatorAng Interfaces/Motor_Controller-a/ElectricalManipulator-a Interfaces/Motor_Controller-b/ElectricalManipulator-a FunctionGenerators/RigolDG1062Z-a/SweepingElstatProbes"


columns="2 3 4 7 10"
diags=('U_bias' 'U_current' 'U_loop-d' 'U_probe' 'I_probe')

nb_id='simple_analysis'
notebook=simple_analysis.ipynb

function GetReadyTheDischarge
{
    GeneralTableUpdateAtDischargeBeginning
    Call $SHM0/Devices/Tools/ElectricalManipulator-a SetPosition $(<Parameters/r_lp_tip)
}


function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/ITs/SignalLab-a/ DAS_raw_data_dir

    mkdir -p Results
    tail -n +2 DAS_raw_data_dir/data.csv | awk -F ',' '{print $1, $2}' OFS=',' > Results/U_sweep.csv
    tail -n +2 DAS_raw_data_dir/data.csv | awk -F ',' '{print $1, $3}' OFS=',' > Results/I_probe.csv

    Analysis
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox

    Call $SHM0/Devices/Tools/ElectricalManipulator-a Homing
}


function Analysis
{

    #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb

    
    jupyter-nbconvert --ExecutePreprocessor.timeout=300 --to html --execute $nb_id.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    

    if [[ -f icon-fig.png ]]; then
        convert -resize $icon_size icon-fig.png graph.png
        convert -resize $icon_size icon-fig.png analysis.jpg
    fi

}




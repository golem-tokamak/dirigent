import socket
import re
import time

resp_prs = re.compile('PRS=(\d.\d\dE[-+]\d\d)Pa')
resp_pri = re.compile('PRI=(\d.\d\dE[-+]\d\d)Pa')
resp_ras = re.compile('RAS=(\d\d)')

## only for Entry EVR 116
resp_flo = re.compile('FLO=(\d.\d\dE[-+]\d\d)Pal/s')
msg_close = 'FLO=4.99E-04'
resp_close = 'FLO=4.99E-04Pal/s'
##

msg_open = 'FLO=1.00E+05'
resp_open = 'FLO=1.00E+05Pal/s'

mod_pres = 'MOD=PRESS'
mod_flow = 'MOD=FLOW'



class RVC300GNOME232(object):

    def __init__(self, ipaddress='192.168.2.239', port=10001,  retries=5, delay=.1):
        for attempt in range(retries):
            try:
                self.sock = socket.create_connection((ipaddress, port))
                break
            except ConnectionRefusedError:
                if attempt < retries:
                    time.sleep(delay)
                else:
                    raise

    def recv(self, bufsize=64):
        buf = b''
        while not buf.endswith(b'\r\n'):
            buf += self.sock.recv(bufsize)
        return buf

    def transmit(self, msg):
        if isinstance(msg,str):
            msg = msg.encode()
        req = msg + b'\r\n'
        self.sock.sendall(req)
    
    def query(self, msg) -> str:
        self.transmit(msg)
        ret = self.recv()
        return ret.decode("ascii")

    def get_pressure(self):
        msg = 'PRI?'
        ret = self.query(msg)
        ret_match = resp_pri.search(ret)
        if not ret_match:
            raise ValueError('Cannot parse return message %s' % ret)
        value = float(ret_match.group(1))
        return value

    def setpoint_mPa(self, press_mPa):
        if self.get_mode() != mod_pres:
            self.query(mod_pres)
        
        press_Pa = press_mPa/1e3
        msg = f'PRS={press_Pa:.2E}Pa'
        ret = self.query(msg)
        
        ret_match = resp_prs.search(ret)
        if not ret_match:
            raise ValueError('Cannot parse return message %s' % ret)
        value = float(ret_match.group(1))
        #print(f'{value=}')
        
    
    def open(self):
        if self.get_mode() != mod_flow:
            self.query(mod_flow)
            
        ret = self.query(msg_open)
        if str(resp_open) not in str(ret):
            raise ValueError('Invalid response for open valve: %s' % ret)
        
    def close(self):
        if self.get_mode() != mod_flow:
            self.query(mod_flow)
            
        ret = self.query(msg_close)
        if str(resp_close) not in str(ret):
            raise ValueError('Invalid response for closed valve: %s' % ret)
        
    def get_mode(self):
        msg = 'MOD?'
        return self.query(msg)
    
    
    def set_controller_speed(self,val):
        if val < 0 or val > 100:
            raise ValueError('Invalid parameter %s. Must be in range 1-99')
        
        ret = self.query(f'RAS={val:02d}')
        ret_match = resp_ras.match(ret)
        if not ret_match:
            raise ValueError('Cannot parse return message %s' % ret)
        value = float(ret_match.group(1))
        #print(f'{value=}')
<?php
require_once('../db_header.php');

$title='Title';
$url='https://golem.fjfi.cvut.cz/shots/';
$SHOTS='/golem/shots';

$FROM=$_GET['from'];
$TO=$_GET['to'];


echo "<html><body><h1>$title</h1><h2>From #$FROM to #$TO</h2><table border=1>";
echo "<tr><th>ShotNo</th><th>Basic diagnostics<br>Tags</br></th></tr>";

for ($i = $FROM; $i <= $TO; $i++) {
    echo "<tr>
    <td><a href=$url/$i>$i</a></td>";
    echo "<td><img src='http://golem.fjfi.cvut.cz/shots/$i/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg'/><br>";
    EchoQuerry('SELECT  \'D:<a href=/DataBase/Utils/InvertTag.php?shot_no='.$i.'&table=diagnostics.basicdiagnostics&tag=tag_dbl_bd>\'||tag_dbl_bd||\'</a>\' FROM diagnostics.basicdiagnostics WHERE shot_no='.$i);
    EchoQuerry('SELECT  \'T:<a href=/DataBase/Utils/InvertTag.php?shot_no='.$i.'&table=diagnostics.basicdiagnostics&tag=tag_trp_bd>\'||tag_trp_bd||\'</a>\' FROM diagnostics.basicdiagnostics WHERE shot_no='.$i);
    EchoQuerry('SELECT  \'M:<a href=/DataBase/Utils/InvertTag.php?shot_no='.$i.'&table=diagnostics.basicdiagnostics&tag=tag_mlt_bd>\'||tag_mlt_bd||\'</a>\' FROM diagnostics.basicdiagnostics WHERE shot_no='.$i);
    echo "</td></tr>";
};

echo "</table></body></html>";

// e.g. https://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?from=44860&to=44871 (smazat s v https)
?>

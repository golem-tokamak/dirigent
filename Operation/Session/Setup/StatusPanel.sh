xterm -fg yellow -bg black -fa 'Monospace' -fs 50 -title "Operational parameters" -e "watch -t \"
echo -n Time:;date '+%H:%M:%S';
echo -n Shot No:; cat /golem/shm_golem/shot_no| tr -d '\n';
echo -n ' (';cat /golem/shm_golem/ActualSession/SessionLogBook/tokamak_state| cut -d'@' -f1| tr -d '\n';echo ') ';
echo -n p_ch:; cat /golem/shm_golem/ActualSession/SessionLogBook/ActualChamberPressuremPa;
if [ -f /golem/shm_golem/ActualShot/Infrastructure/WorkingGas/Parameters/p_h ]; then
echo -n '/'; cat /golem/shm_golem/ActualShot/Infrastructure/WorkingGas/Parameters/p_h| tr -d '\n';fi
echo -n ' mPa;';
echo  -n U_Bt:; cat /golem/shm_golem/ActualSession/SessionLogBook/U_Bt_now;
if [ -f /golem/shm_golem/ActualShot/Infrastructure/Bt_Ecd/Parameters/u_bt ]; then
echo -n '/'; cat /golem/shm_golem/ActualShot/Infrastructure/Bt_Ecd/Parameters/u_bt| tr -d '\n';fi
echo -n ' V;';
echo  -n U_cd:; cat /golem/shm_golem/ActualSession/SessionLogBook/U_CD_now;
if [ -f /golem/shm_golem/ActualShot/Infrastructure/Bt_Ecd/Parameters/u_cd ]; then
echo -n '/'; cat /golem/shm_golem/ActualShot/Infrastructure/Bt_Ecd/Parameters/u_cd| tr -d '\n';fi
echo ' V;';
if [ -f \"/golem/shm_golem/ActualShot/comment\" ];then
if [ -e /golem/shm_golem/ActualShot/index.html ];then
echo -n Discharge completed:;cat /golem/shm_golem/ActualShot/comment;
else
echo -n On air:;cat /golem/shm_golem/ActualShot/comment;
fi;fi;
if [ -f /golem/shm_golem/ActualShot/Diagnostics/PlasmaDetection/Results/b_plasma ] && grep -q \"1.000\" /golem/shm_golem/ActualShot/Diagnostics/PlasmaDetection/Results/b_plasma; then echo -n Plasma YES:;echo -n ' ';cat /golem/shm_golem/ActualShot/Diagnostics/PlasmaDetection/Results/t_plasma_duration;echo ' ms';fi
if [ -f /golem/shm_golem/ActualShot/Diagnostics/PlasmaDetection/Results/b_plasma ] && grep -q \"0.000\" /golem/shm_golem/ActualShot/Diagnostics/PlasmaDetection/Results/b_plasma; then echo -n Plasma: NO;fi
\""

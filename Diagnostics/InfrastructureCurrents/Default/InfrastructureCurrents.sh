BasePath=../..;source $BasePath/Commons.sh

DAS="Oscilloscopes/RigolDS1104Z-a/Bt_Ecd_currents"
DeviceList="$DAS"



function PostDischargeAnalysis
{

    WaitForFileReady -file ../../Devices/`dirname $DAS/`ScreenShotAll.png -timeout 10 -step 1

    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir
    cp DAS_raw_data_dir/* .

echo "
<a href="Devices/`dirname $DAS`/ScreenShotAll.png"><img src="Devices/`dirname $DAS`/rawdata.jpg"></a>
<a href="Devices/`dirname $DAS`/" title="Data directory">$diricon</a><br></br>
" >> include.html

    
}




<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

$time = date("Y-m-d h:i:sa");

function recursive_directory($dirname,$maxdepth=10, $depth=0){
 if ($depth >= $maxdepth) {
  return false;
 }
 $subdirectories = array();
 $files = array();
 if (is_dir($dirname) && is_readable($dirname)) {
  $d = dir($dirname);
  while (false !== ($f = $d->read())) {
   $file = $d->path.'/'.$f;
   // skip . and ..
   if (('.'==$f) || ('..'==$f) || ('index.php'==$f) || ('status.php'==$f) || ('files.php'==$f)) {
    continue;
   };
   if (is_dir($dirname.'/'.$f)) {
    array_push($subdirectories,$dirname.'/'.$f);
   } else {
    array_push($files,$dirname.'/'.$f);
   };
  };
  $d->close();
  foreach ($subdirectories as $subdirectory) {
    $files = array_merge($files, recursive_directory($subdirectory, $maxdepth, $depth+1));
  };
 }
 return $files;
}

while(True){

  try{
    $string = file_get_contents("https://golem.fjfi.cvut.cz/current_status.json");
    $json = json_decode($string, true);
    $pom1 = $json["tokamak_state"];#date("h:i:sa");
    sleep(2);
    $string = file_get_contents("https://golem.fjfi.cvut.cz/current_status.json");
    $json = json_decode($string, true);
    $pom2 = $json["tokamak_state"];#date("h:i:sa");
    if($pom1 != $pom2){

      echo "data: <font size=5vw;> Index Of: <br> </font>\n";
      echo "data: <ul>\n";
      $files = recursive_directory(".");
      foreach ($files as $value) {
        $value = str_replace('./', '', $value);
        $div = explode('/', ltrim($value, '/'));
        /*for($i=1; $i<count($div); $i++){

          echo "data: <li> <a href=\"$div[$i]\"> $div[$i] <br> </a> </li> \n";
        }*/
        echo "data: <li> <a href=\"$value\"> $value <br> </a> </li> \n";
      }
      echo "data: </ul>\n\n";
    }
    ob_flush();
    flush();
  }
  catch (\Error $e){
      echo "data: error, json not available \n\n";
  }
}

#echo "data: $time\n\n";

?>

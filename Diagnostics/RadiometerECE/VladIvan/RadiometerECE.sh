#!/bin/bash
BasePath=../..;source $BasePath/Commons.sh

source Universals.sh
diag_id=DoubleRakeProbe


DAS="DASs/Papouch-Ji/Radiometer"

#DeviceList="$DAS LabInstruments/RadiometerECE/VladiIvan PowerSupplies/RigolDP831-a/Radiometer"
DeviceList="$DAS LabInstruments/RadiometerECE/VladiIvan"


LastChannelToAcq=12
diags=('Uloop' 'rm1' 'rm2' 'rm3' 'rm4'  'rm5' 'rm6' 'rm7' 'rm8'  'rm9' 'rm10' 'rm11')





function PostDischargeAnalysis() 
{

    #GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s $BasePath/Devices/$(dirname $DAS) DAS_raw_data_dir
    #ln -s ../../../Devices/$DAS DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    
     Analysis
     notebook='null'
     GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox
}


function Analysis
{
local ShotNo=$(<$BasePath/shot_no)

if [ -e $BasePath/Production/Tags/b_plasma-NO ]; then return;fi

cp Script.m Script.sf
sed "s/ShotNo = 0/ShotNo = $ShotNo/g" Script.sf > Script.m 
matlab -nosplash -nodesktop -r Script
convert -resize $icon_size icon-fig.png graph.png
}


function NoAnalysis
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}

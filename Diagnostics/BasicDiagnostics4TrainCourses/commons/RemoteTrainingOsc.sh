# -b,-d,-e
# for address in 11:22 19:45 14:7b;do sudo arp-scan --interface=global0 147.32.4.0/24|grep $address;sudo arp-scan --interface=global0 147.32.5.0/24|grep $address; done


function CommonOpenSession()
{
    SCALE=10
    OFFSET=0
    echo "
    :CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe $SCALE;CHANnel1:OFFSet $OFFSET;
    :CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe $SCALE;CHANnel2:OFFSet $OFFSET;
    CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe $SCALE;CHANnel3:OFFSet $OFFSET;
    CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe $SCALE;CHANnel4:OFFSet $OFFSET;
    :TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 0.001;TIMebase:MAIN:OFFSet 0;
    :TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 0;
    :STOP;:CLEAR;
    :SYSTem:KEY:PRESs MOFF"|$COMMAND
    echo OK
}

function CommonSampleSetting()
{
    echo "
    :CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 3;CHANnel1:OFFSet -10;
    :CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 0.02;CHANnel2:OFFSet -0.06;
    :CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 0.1;CHANnel3:OFFSet 0;
    :CHANnel4:DISPlay ON;CHANnel4:PROBe 1;CHANnel4:SCALe 0.1;CHANnel4:OFFSet 0.0;
    :TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 0.002;TIMebase:MAIN:OFFSet 0.008;
    :TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 6;
    :STOP;:CLEAR;
    :SYSTem:KEY:PRESs MOFF"|$COMMAND
    echo OK
}


function CommonGetData()
{
#SETUP
    mkdir -p ScopeSetup;quer='XINC XOR XREF START STOP'; echo `for i in $quer; do echo :WAV:$i?";";done`'*IDN?'|nc -w 1 $ThisDev 5555|sed 's/;/\n/g'|sed \$d > ScopeSetup/answers;for i in $quer; do echo _WAV_$i;done > ScopeSetup/querries;paste ScopeSetup/querries ScopeSetup/answers > ScopeSetup/ScopeSetup; 
    #cat ScopeSetup/ScopeSetup;
    awk '{print "echo " $2 " >ScopeSetup/"$1}' ScopeSetup/ScopeSetup |bash;

#TIME
    _WAV_XINC=`cat ScopeSetup/_WAV_XINC`;_WAV_START=`cat ScopeSetup/_WAV_START`;_WAV_STOP=`cat ScopeSetup/_WAV_STOP`; perl -e '$Time=0;for ( $i='$_WAV_START' ; $i <= '$_WAV_STOP'; $i++ ){printf "%.5e\n", $Time;$Time=$Time+'$_WAV_XINC'}' > Time
    
#DATA
    for CHANNEL in `seq 1 4`; do 
	echo "$ThisDev oscilloscope:  Downloading data for channel $CHANNEL";echo ":WAV:SOURCE CHAN$CHANNEL;:WAV:FORM ASCii;:WAV:MODE NORM"|$scope_address;sleep 0.5s;echo ":WAV:DATA?" |$scope_address|cut -c 12-|sed 's/,/\n/g'|tee data$CHANNEL|gnuplot -e 'set terminal jpeg;plot "<cat" w l' > ${diags[$CHANNEL]}.jpg;paste -d, Time data$CHANNEL > ${diags[$CHANNEL]}.csv
    sleep 0.2s
    done
    wait
    rm Time data
#    paste data* > data_all
#    Analysis # Pro ucitele
}



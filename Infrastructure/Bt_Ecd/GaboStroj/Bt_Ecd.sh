 BasePath=../../..;source $BasePath/Commons.sh

function Relay {
RelayBoardName=$1
RelayNo=$2
SwitchTo=$3
    LogIt "#$RelayNo@$RelayBoardName  (${FUNCNAME[1]}) goes $SwitchTo .. "; 
    case "$SwitchTo" in
    'ON')  
    echo "*B1OS"$RelayNo"H"|telnet Quido-$RelayBoardName 10001  1>/dev/null 2>/dev/null;
    ;;
    'OFF')
    echo "*B1OS"$RelayNo"L"|telnet Quido-$RelayBoardName 10001 1>/dev/null 2>/dev/null;
    ;;
    esac
}

function Relay_ON-OFF
{
RelayBoardName=$1
RelayNo=$2

    Relay $RelayBoardName $RelayNo ON
    Relax
    Relay $RelayBoardName $RelayNo OFF
}


function TestBtLinky
{
    Trig-act@Bt_Ecd ON;sleep 5
    Bt-CW@Bt_Ecd;Relax # Prepinac na CW
    Shortcuts@Bt_Ecd ON;Relax
    LV-Engage@Bt_Ecd ON;Relax
    LV_Bt-Engage@Bt_Ecd ON;sleep 5 # Nabijeni
    LV_Bt-Engage@Bt_Ecd OFF;Relax
    LV-Engage@Bt_Ecd OFF;Relax
    sleep 5 #Trigger na Dg@GM instalovano arduino
    #@Dg: cd /home/golem/Drivers/Arduino
    #./arduino-ide_2.3.2_Linux_64bit.AppImage
    # file:///cloud/Dirigent/Infrastructure/Bt_Ecd/GaboStroj/Trigry
    Shortcuts@Bt_Ecd OFF
    Trig-act@Bt_Ecd OFF
}


# ============ functions@Gabo New energetics ============
# Bt-Ecd 16-c .251 :7B
# Under GABO construction (horizon)
#function Rel1@Bt_Ecd { Relay Bt_Ecd 1 $1; }
function Bt-Disc@Bt_Ecd { Relay_ON-OFF Bt_Ecd 1; }
function Bt-ACW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 2; }
function Bt-CW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 3; }

#Budouci reseni az bude Velke rele
#function CBt-Disc@Bt_Ecd { Relay_ON-OFF Bt_Ecd 4; }
#function CBt-Pl@Bt_Ecd { Relay_ON-OFF Bt_Ecd 5; } #Plus
#function CBt-Mn@Bt_Ecd { Relay_ON-OFF Bt_Ecd 6; } #Minus

#Provizorni reseni nez bude Velke rele
function CBt-Disc@Bt_Ecd { Relay Bt_Ecd 4 $1; }

function Ecd-Disc@Bt_Ecd { Relay_ON-OFF Bt_Ecd 7; }
function Ecd-ACW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 8; }
function Ecd-CW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 9; }

#10 a 11 volne

function Trig-act@Bt_Ecd { Relay Bt_Ecd 12 $1; }
function HV_Bt-Commut@Bt_Ecd { Relay Bt_Ecd 13 $1; }
function HV_Bt-Engage@Bt_Ecd { Relay Bt_Ecd 14 $1; }
function LV_Bt-Engage@Bt_Ecd { Relay Bt_Ecd 15 $1; }
function HV_Ecd-Engage@Bt_Ecd { Relay Bt_Ecd 16 $1; }
function LV_Ecd-Engage@Bt_Ecd { Relay Bt_Ecd 17 $1; }
function HV_Ebd-Engage@Bt_Ecd { Relay Bt_Ecd 18 $1; }
function LV_Ebd-Engage@Bt_Ecd { Relay Bt_Ecd 19 $1; }
function LV-Engage@Bt_Ecd { Relay Bt_Ecd 20 $1; }
function Bt-AdditCapac@Bt_Ecd { Relay Bt_Ecd 21 $1; }
function Shortcuts@Bt_Ecd { Relay Bt_Ecd 22 $1; }

# LEDstrips 16-d .239 :35
#

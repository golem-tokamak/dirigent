#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source Universals.sh



DeviceList="Interfaces/Gnome232-b/PreionizationPowSup PowerSupplies/GWInstekPSW-b/Preionization PowerSupplies/AC250Kxxx-a/Preionization"

Gnome232=Gnome232-b


function GetReadyTheDischarge()
{
    #LogTheDeviceAction 
    GeneralTableUpdateAtDischargeBeginning
    
    powsup_heater=$(<$SHM0/Infrastructure/Preionization/Parameters/u_heater)
    #powsup_heater=60
    num=$powsup_heater
    powsup_heater_format=$(printf "%03d\n" $powsup_heater)
    sum=0
    while [ $num -gt 0 ]
do
    mod=$(($num % 10))    #It will split each digits
    sum=$((sum + mod))   #Add each digit to sum
    num=$(($num / 10))    #divide num by 10.
done



#echo Sum@Preionization: $sum
chksum=$(printf "%x\n" $((10#$sum)))
#echo  CheckSum@Preionization:  $chksum

    VerboseMode "sum chksum"

echo -ne "@0ANAP"$powsup_heater_format"E"$chksum"\n"|telnet $Gnome232 10001 1>/dev/null 2>/dev/null
sleep 3
echo -ne "@0AOUT19A\n"|telnet $Gnome232 10001 1>/dev/null 2>/dev/null


Call $RelayBoards Preionization-Heater@Miscellaneous ON #PreionHeaterON
powsup_accel=$(<$SHM0/Infrastructure/Preionization/Parameters/u_accel)
#powsup_accel=100

echo "APPL $powsup_accel,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-b 2268
Call $RelayBoards Preionization-Engage@Miscellaneous ON

sleep 10
Call $RelayBoards Preionization-Shortcut@Miscellaneous ON #protect Am meter
}

function ArmingWithCurrentMeas
{

Current=`echo "MEASure:SCALar:CURRent:DC?"|netcat -w 1 GWInstekPSW-b 2268|sed 's/+//g'`
    echo $Current >Results/I_PI^arm
    UpdateDataBaseQuantAtCurrentShot infrastructure.preionization I_PI^arm $Current
    if (( $(echo $Current " < 0.002" |bc -l) )); then
        Speaker Problems/Infrastructure-fail-Preionization-under-expected-value;
    fi

}


function Arming
{
:
}


function SecurePostDischargeState()
{
#echo "*B1OS1L"|telnet 192.168.2.240 10001 #PreionHeaterOFF
Call $RelayBoards Preionization-Engage@Miscellaneous OFF
Call $RelayBoards Preionization-Heater@Miscellaneous OFF #PreionHeater OFF
mRelax
Call $RelayBoards Preionization-Shortcut@Miscellaneous OFF #protect Am meter
echo -ne "@0AOUT099\n"|telnet $Gnome232 10001  1>/dev/null 2>/dev/null
echo "APPL 0,2;OUTPut:IMMediate OFF"|netcat -w 1 GWInstekPSW-b 2268

if [[ $(bc <<< "2.1 < 2.3") -eq 1 ]]; then echo ahoj;fi


}


function PostDischargeAnalysis
{
    GenerateDiagWWWs
}

function GenerateDiagWWWs
{
local cesta="Infrastructure/$instrument"

echo "<h3>Preionization</h3><table><tr>
Parameters <a href=$cesta/Parameters/ title="Parameters">$parametersicon</a>:`cat $SHM0/Production/Parameters/infrastructure.preionization`" >include.html;

echo "<table><tr>
<td><a href=$cesta/port_$port.jpg title="Infrastructure placement"><img src=$cesta/port_$port.jpg width='$iconsize'/></a></td>
<td><img src="$cesta/setup.png" width="$iconsize"></td>

<td><a href=https://www.dropbox.com/$DropBox title="Photogallery@DropBox"><img src=$cesta/Overview-thumb.jpg /></a></td></tr></table>

" >> include.html


}



# ============ Test ============
# 520  Call $RelayBoards Preionization-Heater@Miscellaneous ON
#  521  echo -ne "@0AOUT19A\n"|telnet Gnome232-b 10001
#  522  Call $RelayBoards Preionization-Engage@Miscellaneous ON
#  523  echo "APPL 80,2;OUTPut:IMMediate ON"|netcat -w 1 GWInstekPSW-b 2268



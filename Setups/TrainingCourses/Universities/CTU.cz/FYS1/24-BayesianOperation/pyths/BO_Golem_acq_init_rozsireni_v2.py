from bayes_opt import BayesianOptimization
import numpy as np
import subprocess, sys

#from bayes_opt import acquisition
from unicodedata import ucd_3_2_0

iteration = 0
"""Funkce čtení dat z tokamaku"""
def get_GOLEM_time():
    # setup url
    url = '/golem/shots/0/Diagnostics/PlasmaDetection/Results/t_plasma_duration'
    #url = "http://golem.fjfi.cvut.cz/shots/0/Diagnostics/PlasmaDetection/Results/t_plasma_duration"
    # Try to load if webpage/parameter value does not exists 0 is assumed
    try:
        number = np.loadtxt(url)
        return number
    except:
        return 0

"""Funkce ovládání tokamaku"""
def tokamak_controler(x1,x2,x3,x4):
    tokamak_command = f"""./Dirigent.sh --discharge --operation.discharge "style='standard',vacuum_shot='-1'" --infrastructure.bt_ecd "U_Bt={x2:.1f},t_Bt=0,O_Bt='CW',U_cd={x1:.1f},t_cd={x3:.1f},O_cd='CW'" --infrastructure.workinggas "S_mode='U_v',S_gas='H',p_H=10,U_v={x4:.2f}" --infrastructure.positionstabilization "main_switch='on',radial_switch='on',radial_waveform='1000,0;11000,-20;20000,-20;40000,0',vertical_switch='on',vertical_waveform='1000,0;10000,-20;19000,-20;40000,0'" --comment 'fyzikalni_seminar_ba{iteration}'"""

    #kód pro tokamak
    subprocess.run(tokamak_command, shell = True, executable="/bin/bash")
    time = get_GOLEM_time()
    print('Duration ',time)
    return float(time)

"""Blackbox funkce"""
def tokamak_function(U_cd,U_bt,delta_t,p):
    global iteration
    iteration += 1

    time = tokamak_controler(U_cd,U_bt,delta_t,p)

    return time

"""nastavení definičního oboru parametrů"""
pbounds = {'U_cd': (380, 450),'U_bt': (1000, 1300),'delta_t': (500, 1500),'p': (16, 17)}

"""deklarace optimizéru"""
optimizer = BayesianOptimization(
    f=tokamak_function,
    pbounds=pbounds,
#   acquisition_function= acquisition.ExpectedImprovement(xi = 0.1),
    verbose=1,
    random_state=1,
    allow_duplicate_points=True
)
"""Nahrání dat jako inicializační body"""
with open("vstupni_data.txt", "r") as file:
    points = file.readlines()

for point in points:

    ucd = float(point.split(" ")[0])
    ubt = float(point.split(" ")[1])
    delta_t = float(point.split(" ")[2])
    p = float(point.split(" ")[3])
    target = float(point.split(" ")[4])

    optimizer.register(params={'U_cd': ucd,'U_bt': ubt,'delta_t': delta_t,'p': p-1}, target=target)

"""Spuštění samotné optimalizace"""
optimizer.maximize(init_points=0, n_iter=10)

"""Zpracování výsledného maxima"""
target = optimizer.max["target"]
parameters_for_target = [float(optimizer.max["params"][i]) for i in optimizer.max["params"]]

print(f" Maximum found as: {target} ms, for parameters: {parameters_for_target}")

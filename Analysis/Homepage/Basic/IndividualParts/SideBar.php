      <div class="sphinxsidebar">
	<div class="sphinxsidebarwrapper" style="position:absolute;z-index:0;float: left; margin-right: 0px; width: 172px;">


	    <a href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>"><img src="/_static/logos/golem.svg" alt="" width="80"></a>

    <h3>Navigation</h3>
		<ul class="this-page-menu">

 <li>

    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo ($shot_no+1);?>">Next</a>
    </li>

 <li>

    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/<?php echo ($shot_no-1);?>">Previous</a>
    </li>
<li>

    <a class="reference internal" href="http://golem.fjfi.cvut.cz/shots/0">Current</a>
    </li>

		</ul>


	    <div id="searchbox" style="">
		<h3>Go to shot</h3>
		    <form style="display:inline;" method="post" action="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/" onsubmit="return getURL(this.shot.value)">
		    <input size="5" type="text" name="shot" value="<?php echo ($shot_no);?>">
		    <input size="5" type="submit" name="Go" value="Go">
		    </form>
	    </div>
	    <script type="text/javascript">$('#searchbox').show(0);</script>




		<h3>Other</h3>
		<ul class="this-page-menu">
        
    
 <li><a class="reference internal" href="http://golem.fjfi.cvut.cz/wiki/">Wiki</a></li>
 <li><a class="reference internal" href="/bookmarks/bookmarks.html">Operator's bookmarks</a>
  </li>
 <!--<li><a class="reference internal" href="http://golem.fjfi.cvut.cz/utils/">Utilities</a></li>-->

		</ul>

        <div id="searchbox">
            <br>
            <h3>Database operations</h3>
            <ul class="this-page-menu">
                <li> <a class="reference internal" href="https://golem.fjfi.cvut.cz/tools/Chart/chart.php">General DB querry</a></li>
            </ul>
        </div>
	</div>

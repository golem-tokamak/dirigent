#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

COMMAND="netcat -w 1 $ThisDev 5555"

function Wake-upCall() {  
DelayCall $RelayBoards $ThisDev@Galvanics ON; }   

function Sleep-downCall() {  
DelayCall $RelayBoards $ThisDev@Galvanics OFF; } 

function PostDischargeAnalysis()
{
    GetOscScreenShot
}

function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$COMMAND
	mRelax
    echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND|tail -c +12 > ScreenShot.png
    #echo ":DISPLAY:DATA?  ON,OFF,PNG"|$COMMAND
    convert -morphology Dilate Octagon -resize 200x200 ScreenShot.png rawdata.jpg
}

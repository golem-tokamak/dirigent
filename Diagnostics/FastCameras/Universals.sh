#!/bin/bash


port=S_cam
DropBox='scl/fo/qm9vp3u6g7fixgy04ivbd/h?rlkey=ofc0ikw37qgkgfx1zepr4unt0&dl=0'
name="Fast cameras"
alias="fastcams"


#additionals
# DROP TABLE diagnostics.fastcameras;
# CREATE TABLE diagnostics.fastcameras (
#     shot_no integer,
#     changesetup boolean,
#     fr_ux100a integer,
#     width_ux100a integer,
#     height_ux100a integer,
#     recrate_ux100a integer,
#     shotdur_ux100a integer,
#     fr_ux100b integer,
#     width_ux100b integer,
#     height_ux100b integer,
#     recrate_ux100b integer,
#     shotdur_ux100b integer,
#     fr_ux50a integer,
#     width_ux50a integer,
#     height_ux50a integer,
#     recrate_ux50a integer,
#     shotdur_ux50a integer,
#     fr_ux50b integer,
#     width_ux50b integer,
#     height_ux50b integer,
#     recrate_ux50b integer,
#     shotdur_ux50b integer
# );
# ALTER TABLE diagnostics.fastcameras OWNER TO golem;
# ALTER TABLE ONLY diagnostics.fastcameras
#     ADD CONSTRAINT fastcameras_shot_no_key UNIQUE (shot_no);
#

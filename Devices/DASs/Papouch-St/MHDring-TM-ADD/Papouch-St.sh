#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh
source Universals.sh

Drivers="DASs/Drivers/PapouchDAS1210/driver"
source $SW/Devices/$Drivers.sh

papouch_ip=$ThisDev

shot_no=$(CurrentShotDataBaseQuerry shot_no)
data_dir=$PWD

diags=('U_{loop}' 'U_{rogowski}' 'U_{Bt-HFS}' 'U_{Bt-LFS}' 'U_{diam-inner}' 'U_{diam-outer}')

function Arming()
{
    arm_papouch_das $papouch_ip
}


function RawDataAcquiring()
{
local LastChannelToAcq=6

    LogIt "$ThisDev: Start of acquiring"
    for i in $(seq 1 $LastChannelToAcq) ; do
            echo -ne ACQ:ch$i@$papouch_ip";";read_channel_papouch_das $papouch_ip $i $data_dir/ch$i.csv 40e-3; 
    done
    PlotIt
    LogIt "$ThisDev: End of acquiring"
}




function PlotIt()
{
	rm tmp
    echo  "set datafile separator ',';"\
          "set terminal png size 1600, 1000;"\
          "set output 'ScreenShotAll.png';"\
          "set bmargin 0;"\
          "set tmargin 0;"\
          "set lmargin 10;"\
          "set rmargin 3;"\
          "unset xtics;"\
          "unset xlabel;"\
          "set multiplot layout 3,2 columnsfirst title 'GOLEM Shot $shot_no';"\
          "set xrange [*:*];"\
          "set yrange [*:*];"\
          "set style data dots;"\
          "set ytics;"\
        > tmp

    for i in $(seq 1 3); do
        echo "set ylabel '${diags[$i-1]}';plot 'ch$i.csv' u 1:2 t '' w l lc 1;" \
        >> tmp
    done
    
    for i in $(seq 4 6); do
        echo "set ylabel '${diags[$i-1]}';plot 'ch$i.csv' u 1:2 t '' w l lc 1;" \
        >> tmp
    done

    cat tmp|gnuplot 1>/dev/null 2>/dev/null
	convert -resize 200x200 ScreenShotAll.png rawdata.jpg

}


function PlotIt-12()
{
	rm tmp
    echo  "set datafile separator ',';"\
          "set terminal png size 1600, 1000;"\
          "set output 'ScreenShotAll.png';"\
          "set bmargin 0;"\
          "set tmargin 0;"\
          "set lmargin 10;"\
          "set rmargin 3;"\
          "unset xtics;"\
          "unset xlabel;"\
          "set multiplot layout 6,2 columnsfirst title 'GOLEM Shot $shot_no';"\
          "set xrange [*:*];"\
          "set yrange [*:*];"\
          "set style data dots;"\
          "unset ytics;"\
        > tmp

    for i in $(seq 1 6); do
        echo "set ylabel 'R$((i-0))';plot 'ch$i.csv' u 1:2 t '' w l lc 1;" \
        >> tmp
    done

    for i in $(seq 7 12); do
        echo "set ylabel 'L$((i-6))';plot 'ch$i.csv' u 1:2 t '' w l lc 1;" \
        >> tmp
    done

    cat tmp|gnuplot 1>/dev/null 2>/dev/null
	convert -resize 200x200 ScreenShotAll.png rawdata.jpg

}

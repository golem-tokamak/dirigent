from=41655
#to=43571
to=42630
soubor=index.html; 
url=https://golem.fjfi.cvut.cz/shots/
t_plasma_duration_limit=7
interval=10
title='MHD - advanced ring - vacuum and plasma shots '
#url=/shots

echo "<html><body>
<h1>$title</h1>
<h2>From #$from to #$to with t_plasma_duration_limit=$t_plasma_duration_limit with plasma within +/- $interval from vacuum shot</h2>
<table>" > $soubor;
echo "<tr><th colspan=3>VACUUM SHOT</th><th colspan=3>PLASMA SHOT</th></tr>" >> $soubor;
echo "<tr><th>ShotNo</th><th>Basic diagnostics</th><th>MHD diagnostics</th><th>ShotNo</th><th>Basic diagnostics</th><th>MHD diagnostics</th></tr>" >> $soubor;
Counter=0
for i in `seq $from $to|grep '41906\|41794\|41807\|41814\|42052\|42063\|42082|'`; do echo -ne '*'$i:; 
if [[ $(< /golem/shots/$i/Diagnostics/BasicDiagnostics/DetectPlasma/Results/b_plasma) == "0.000" ]]; then
    echo  Vacuum shot '*'$i yes
    for j in `seq $((i-interval)) $((i+interval))`; 
    do echo -ne tested '*'$j:;
        if [[ `echo $(</golem/shots/$j/Diagnostics/BasicDiagnostics/DetectPlasma/Results/t_plasma_duration)|bc|tr -d -` > $t_plasma_duration_limit ]] \
        && [[ $(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/u_bt) == $(< /golem/shots/$j/Infrastructure/Bt_Ecd/Basic/Parameters/u_bt) ]] \
        && [[ $(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/u_cd) == $(< /golem/shots/$j/Infrastructure/Bt_Ecd/Basic/Parameters/u_cd) ]] \
        && [[ $(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/t_bt) == $(< /golem/shots/$j/Infrastructure/Bt_Ecd/Basic/Parameters/t_bt) ]] \
        && [[ $(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/t_cd) == $(< /golem/shots/$j/Infrastructure/Bt_Ecd/Basic/Parameters/t_cd) ]] \
        && [[ $(< /golem/shots/$i/shot_date) == $(< /golem/shots/$j/shot_date) ]] \
        && [[ -e "/golem/shots/$i/Devices/DASs/2NI_PC-StepMal/LimiterMirnovCoils/rawdata.jpg" ]] \
        && [[ -e "/golem/shots/$j/Devices/DASs/2NI_PC-StepMal/LimiterMirnovCoils/rawdata.jpg" ]];then
            let Counter++ #increment
            echo YES! $i AND $j FOUND
            echo "<tr><td colspan=6>
            Date: <b>`cat /golem/shots/$i/shot_date`</b>, Parameters:
            U_bt=$(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/u_bt)V,
            U_cd=$(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/u_cd)V,
            t_bt=$(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/t_bt)us,
            t_cd=$(< /golem/shots/$i/Infrastructure/Bt_Ecd/Basic/Parameters/t_cd)us
            </td></tr><tr>
            <td><a href=$url/$i>$i</a><br>`cat /golem/shots/$i/shot_time`</td>
            <td><img src=$url/$i/Diagnostics/BasicDiagnostics/Basic/graph.png></td>
            <td><img src=$url/$i/Devices/DASs/2NI_PC-StepMal/LimiterMirnovCoils/rawdata.jpg></td>
            <td><a href=$url/$j>$j</a><br>`cat /golem/shots/$j/shot_time`</td>
            <td><img src=$url/$j/Diagnostics/BasicDiagnostics/Basic/graph.png></td>
            <td><img src=$url/$j/Devices/DASs/2NI_PC-StepMal/LimiterMirnovCoils/rawdata.jpg></td>
            </tr>" >> $soubor;
        fi
        done 
else echo -ne no';';fi;done
echo "</table>Alltogether $Counter combinations<hr/>
<h2>For generation bash script used</h2>
<pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;




import argparse
import niscope
import numpy as np
import socket
import time
import threading
import pandas as pd
import traceback


TRIGGER_SOURCE = "PXI-5114/Trig" 
SCOPE_NAME = "PXI-5114" 
SAMPLE_FREQ = 250_000_000
NUM_OF_SAMPLES = int(30e-3 * SAMPLE_FREQ)
TRIGGER_LEVEL=4 #V
VOLTAGE_RANGE=10
OUTFILE='dataHiRes.npz'


ARMING_MAX_WAIT=60
PORT=45123
armed = False


def read_scope(scope_name=SCOPE_NAME, 
               trigger_source=TRIGGER_SOURCE, 
               trigger_level=TRIGGER_LEVEL,
               samplerate=SAMPLE_FREQ,
               n_samples=NUM_OF_SAMPLES,
               ch0_range=VOLTAGE_RANGE,
               ch1_range=VOLTAGE_RANGE):
    global armed
    with niscope.Session(resource_name=scope_name) as session:
        
        ch0_array = np.empty(n_samples, dtype=np.float64)
        ch1_array = np.empty(n_samples, dtype=np.float64)
        
        session.channels[0].configure_vertical(range=ch0_range, coupling=niscope.VerticalCoupling.DC)
        session.channels[1].configure_vertical(range=ch1_range, coupling=niscope.VerticalCoupling.DC)
        session.configure_horizontal_timing(min_sample_rate=samplerate, min_num_pts=n_samples, ref_position=0, num_records=1, enforce_realtime=True)
        session.configure_trigger_edge(trigger_source = trigger_source, level = trigger_level, trigger_coupling=niscope.TriggerCoupling.DC,
                                       slope=niscope.TriggerSlope.POSITIVE,
                                       )   
        
        t0 = time.monotonic()
        while not armed:
            time_elapsed = time.monotonic() - t0
            if time_elapsed > ARMING_MAX_WAIT:
                break
            else:
                time.sleep(1)
            
        with session.initiate():
            ch0_waveform_info = session.channels[0].fetch_into(waveform=ch0_array, num_records=1, timeout=30)
            ch1_waveform_info = session.channels[1].fetch_into(waveform=ch1_array, num_records=1, timeout=10)

        t_stop = n_samples/samplerate
        t_vec = np.linspace(0,t_stop,n_samples)
        
        # TD check if works or change ref_position of configure_horizontal_timing to 0.0
        t_vec += ch0_waveform_info[0].relative_initial_x

        return t_vec, ch0_array, ch1_array


def arm_scope():
    client = socket.socket()
    client.connect(('localhost', PORT))
    client.sendall(b"arming")
    client.close()
    print('calling to arms')


def handle_client(server):
    global armed
    conn, _ = server.accept()
    data = conn.recv(1024).decode()
    if data == "arming":
        armed = True
    conn.close()

def main():    
    parser = argparse.ArgumentParser()
    parser.add_argument('--trigger',help = 'Name of NI trigger source', 
                        type = str, default = TRIGGER_SOURCE)
    parser.add_argument('--trigger_level', type=float, default=TRIGGER_LEVEL)

    parser.add_argument('--card', help = 'Name of single NI card to read from', 
                        type = str, default=SCOPE_NAME)
    
    parser.add_argument('--ch0_range', type= float, default=VOLTAGE_RANGE)
    parser.add_argument('--ch1_range', type= float, default=VOLTAGE_RANGE)
    parser.add_argument('--samplerate', type = int, default=SAMPLE_FREQ)
    parser.add_argument('--nsamples', type = int, default=NUM_OF_SAMPLES)
    parser.add_argument('--ms_samples', type = float)
    parser.add_argument('--arming', action='store_true')
   
    
    parser.add_argument('-o', '--output', help = 'Output file name', type = str, default = OUTFILE)
    args = parser.parse_args()
    
    if args.arming:
        arm_scope()
        return

    # Setup the server socket for delayed arming
    server = socket.socket()
    server.bind(('localhost', PORT))
    server.listen(1)


    # Start the thread to handle the client
    thread = threading.Thread(target=handle_client, args=(server,))
    thread.start()
    

    nsamples = args.nsamples
    if args.ms_samples:
        nsamples = int(args.ms_samples * args.samplerate / 1e3)

    try:
        t_vec, ch0, ch1 = read_scope(scope_name=args.card,
                          trigger_source=args.trigger,
                          trigger_level=args.trigger_level,
                          samplerate=args.samplerate,
                          n_samples=nsamples,
                          ch0_range=args.ch0_range,
                          ch1_range=args.ch1_range)
    except Exception as e:
        print(e)
        traceback.print_exc()

    else:
    #np.savez_compressed(args.output, time=t_vec, ch0=ch0, ch1 = ch1)           
    
        # Combine data into a Pandas DataFrame
        df_data = pd.DataFrame({
            'time': t_vec,
            'ch1': ch0,
            'ch2': ch1
        })

        # Save CSV file
        csv_file = "data.csv"
        df_data.to_csv(csv_file, index=False)

    finally:
        thread.join()
        server.close()

if __name__ == "__main__":  
    main()
    

#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh
source Universals.sh



function SetAcqMode() {
    echo ":ACQUIRE:MODE HIRes;
    :HORizontal:MODE MANual;
    :HORizontal:DELay:MODe OFF;
    :HORizontal:POSition 10;
    :HORIZONTAL:MODE:SAMPLERATE 250e6;
    :HORIZONTAL:MODE:SCALE 5.0e-3;
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT INTERNAL;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    :SAVEON:WAVEform:SOURCE ALL;
    FPANEL:PRESS SINGLESEQ;
    TRIGGER:A:MODE NORMAL;
    TRIGGER:A:TYPE EDGE ;
    TRIGGER:A:LEVEL:CH8 4;
    TRIGGER:A:EDGE:SOURCE CH8"|$COMMAND 1>/dev/null 2>/dev/null

}

function SetChannels() {


    # echo "
    # :CH1:SCALE 4;:CH1:OFFSET 0;:DISplay:GLObal:CH1:STATE ON;:CH1:TERMINATION 1.0E+6;
    # :CH2:SCALE 0.05;:CH2:OFFSET -0.2;:DISplay:GLObal:CH2:STATE ON;:CH2:TERMINATION 50;
    # :CH3:SCALE 0.05;:CH3:OFFSET -0.05;:DISplay:GLObal:CH3:STATE ON;:CH3:TERMINATION 50;
    # :CH4:SCALE 0.05;:CH2:OFFSET -0.2;:DISplay:GLObal:CH4:STATE ON;:CH4:TERMINATION 50;
    # :CH5:SCALE 0.05;:CH5:OFFSET -0.2;:DISplay:GLObal:CH5:STATE ON;:CH5:TERMINATION 50;
    # :CH6:SCALE 0.2;:CH6:OFFSET 0.2;:DISplay:GLObal:CH6:STATE ON;:CH6:TERMINATION 50;
    # :CH7:SCALE 0.2;:CH7:OFFSET 0.2;:DISplay:GLObal:CH7:STATE ON;:CH7:TERMINATION 50;
    # :CH8:SCALE 2;:CH8:OFFSET 0;:DISplay:GLObal:CH8:STATE ON;:CH8:TERMINATION 1.0E+6;
    # :CH1:LABel:NAME 'U_loop-b';
    # :CH2:LABel:NAME 'YaP';
    # :CH3:LABel:NAME 'CeBr-a';
    # :CH4:LABel:NAME 'CebR-b';
    # :CH5:LABel:NAME 'NaITl-a';
    # :CH6:LABel:NAME 'LYSO-1';
    # :CH7:LABel:NAME 'GAGG';
    # :CH8:LABel:NAME 'Trigger'"|$COMMAND 1>/dev/null 2>/dev/null


        echo "
    :CH1:SCALE 0.2;:CH6:OFFSET 0.2;:DISplay:GLObal:CH6:STATE ON;:CH6:TERMINATION 50;
    :CH2:SCALE 0.2;:CH6:OFFSET 0.2;:DISplay:GLObal:CH6:STATE ON;:CH6:TERMINATION 50;
    :CH3:SCALE 0.05;:CH3:OFFSET -0.05;:DISplay:GLObal:CH3:STATE ON;:CH3:TERMINATION 50;
    :CH4:SCALE 0.05;:CH2:OFFSET -0.2;:DISplay:GLObal:CH4:STATE ON;:CH4:TERMINATION 50;
    :CH5:SCALE 0.2;:CH6:OFFSET 0.2;:DISplay:GLObal:CH6:STATE ON;:CH6:TERMINATION 50;
    :CH6:SCALE 0.2;:CH6:OFFSET 0.2;:DISplay:GLObal:CH6:STATE ON;:CH6:TERMINATION 50;
    :CH7:SCALE 0.2;:CH7:OFFSET 0.2;:DISplay:GLObal:CH7:STATE ON;:CH7:TERMINATION 50;
    :CH8:SCALE 2;:CH8:OFFSET 0;:DISplay:GLObal:CH8:STATE ON;:CH8:TERMINATION 1.0E+6;
    :CH1:LABel:NAME 'LYSO 6';
    :CH2:LABel:NAME 'LYSO 7';
    :CH3:LABel:NAME 'CeBr-a';
    :CH4:LABel:NAME 'CebR-b';
    :CH5:LABel:NAME 'LYSO-3';
    :CH6:LABel:NAME 'LYSO-1';
    :CH7:LABel:NAME 'LYSO-5';
    :CH8:LABel:NAME 'Trigger'"|$COMMAND 1>/dev/null 2>/dev/null   

}

function OpenSession()
{
    SetAcqMode

    SetChannels

    #ExternDataAvailabilityTest
    
    #PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`

    
}


function Arming()
{
    LogTheDeviceAction

#    SetAcqMode

#    SetChannels
    echo ":ACQUIRE:MODE HIRes;
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT INTERNAL;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    :SAVEON:WAVEform:SOURCE ALL;
    FPANEL:PRESS SINGLESEQ;
    TRIGGER:A:MODE NORMAL;
    TRIGGER:A:TYPE EDGE ;
    TRIGGER:A:LEVEL:CH8 4;
    TRIGGER:A:EDGE:SOURCE CH8"|$COMMAND 1>/dev/null 2>/dev/null

    echo "
    `for i in 1 2 3 4 5 6 7 8; do echo :DISplay:GLObal:CH$i:STATE ON;done`
    :SAVEON:WAVEform:SOURCE ALL;"|$COMMAND 1>/dev/null 2>/dev/null

    SingleSeq

}
   



function RawDataAcquiring()
{
    
    echo ":DISplay:GLObal:CH6:STATE ON;
    :DISplay:GLObal:CH7:STATE ON;
    :DISplay:GLObal:CH8:STATE OFF"|$COMMAND  1>/dev/null 2>/dev/null

    sleep 30
    for i in 1 2 3 4 5 6 7; do 
    cp `ls -d /home/golem/tektronix_drop/*|grep wfm|grep TektrMSO58_ch$i`  ch$i.wfm
    done
    cp `ls -d /home/golem/tektronix_drop/*|grep png|grep TektrMSO58` ScreenShotAll.png

    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    
    rm -f /home/golem/tektronix_drop/TektrMSO58*.wfm
    rm -f /home/golem/tektronix_drop/TektrMSO58*.png
   
}
    

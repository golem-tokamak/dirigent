#!/bin/bash

echo -n "Time: "; date '+%H:%M:%S'
echo -n "Shot No: "; cat /golem/shm_golem/shot_no | tr -d '\n'
echo

if [[ "$(cat  /golem/shm_golem/ActualSession/SessionLogBook/tokamak_state)" == "idle" ]]; then
    sort -k2,2nr /golem/shots/$(cat /golem/shm_golem/shot_no)/Production/FunctionTimes | head -n 20
else
pstree -p -a $(cat /dev/shm/golem/ActualShot/Operation/Discharge/Discharge.pid) -l \
    | sed -E 's/(bash,[0-9]+).*source/\1 source/' \
    | sed -E 's/\/dev\/shm\/golem\/ActualShot//g' \
    | sed -E 's/\\012//g' \
    | sed -E 's/\/opt\/anaconda3\/bin\/jupyter-nbconvert//g' \
        | grep -v 'ipykernel_launcher' \
        | grep -v 'jup-nb_stderr' \
        | grep -v 'jup-nb_stdout' \
        | grep -v tail \
        | grep -v 'SELECT latex' \
        | grep -v '{[^}]*}' \
        |cut -c 1-100
fi

exit

chmod +x monitor.sh
watch -t -n 1 ./monitor.sh

sed -E 's/\/opt\/anaconda3\/bin//g' \

watch -t -n 1 "
echo -n Time:;date '+%H:%M:%S';
echo -n Shot No:; cat /golem/shots/47588/shot_no|tr -d '\n';
if [[ $(cat /golem/shots/47588/tokamak_state) = 'idle' ]]; then
    sort -k2,2nr /golem/shots/$(cat /golem/shots/47588/shot_no)/Production/FunctionTimes|head -n 20
else
    pstree -a $(cat /golem/shots/47588/Operation/Discharge/Discharge.pid)|sed 's/bash.*source//;s/\\012//g;s/\/dev\/shm\/golem\/ActualShot//g'|grep -v 'ipykernel_launcher'|grep -v 'jup-nb_stderr'|grep -v 'jup-nb_stdout'|grep -v tail|grep -v 'SELECT latex';
fi"

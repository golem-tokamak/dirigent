#!/bin/bash

basedir="../..";source $basedir/Commons.sh
source Universals.sh

Mission=BiasingElectrode


DAS="Oscilloscopes/RigolMSO5204-d/$Mission"

DeviceList="Amplifiers/Kepco100-4D-a/$Mission FunctionGenerators/RigolDG1032Z-a/$Mission $DAS"



LastChannelToAcq=4

function GetReadyTheDischarge () 
{
    GeneralTableUpdateAtDischargeBeginning
}


function PostDischargeAnalysis
{
#    CallFunction Devices/RelayBoards/Quido8-a/TokamakSouth CurrentClampOFF
    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s $basedir/Devices/`dirname $DAS` DAS_raw_data_dir

    cp DAS_raw_data_dir/*.csv .
    
    issue="I_viaR"
#    paste -d, U_R-BiasEl.csv U_R-PowSup.csv |awk -F "," '{print $1","($4-$2)/22}' > $issue.csv
    cat U_R.csv |awk -F "," '{print $1","$2/22}' > $issue.csv
    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'$issue.csv'" w l t "'${issue/_/\\\\_}'"' > $issue.jpg;

    issue="I_CurrPrb"
    cat U_CurrPrb.csv |awk -F "," '{print $1","$2/(50e-3)}' > $issue.csv
    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'$issue.csv'" w l t "'${issue/_/\\\\_}'"' > $issue.jpg;

    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "I_viaR.csv" w l t "I\\_viaR","I_CurrPrb.csv" w l t "I\\_CurrPrb"' > Result.jpg;
    
    homepage_row
      
}




function homepage_row
{
local cesta="Infrastructure/$instrument"


echo "<h3>Biasing electrode    <a href=$cesta/ title="Directory">$diricon</a></h3>
Parameters <a href=$diagpath/Parameters/ title="Parameters">$parametersicon</a>:`cat $SHM0/Production/Parameters/infrastructure.biasingelectrode`" >include.html;

echo "<table><tr>
<td><a href=$cesta/port_$port.jpg title="Infrastructure placement"><img src=$cesta/port_$port.jpg width='$iconsize'/></a></td>
<td><a href=$cesta/expsetup.svg><img src=$cesta/setup.png width='$iconsize'/></a></td>
<td><a href=https://www.dropbox.com/$DropBox title="Photogallery@DropBox"><img src=$cesta/Overview-thumb.jpg /></a></td>
<td><a href=Devices/`dirname $DAS`/ScreenShotAll.png><img src='Devices/`dirname $DAS`/ScreenShotAll.png'  width='$iconsize'/></a></td>
<td><a href=$cesta/Result.jpg><img src=$cesta/Result.jpg width='$iconsize'/></a></td>
      </tr></table>" >> include.html
}

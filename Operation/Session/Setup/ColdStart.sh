#!/bin/bash

xterm -sb -bg black -fg white -hold -geometry 200x50+0+0 -title "tG: Cold start " -e "
ssh -Y golem@golem 'export TERM=vt100;cd /golem/Dirigent/;./Dirigent.sh --session SetupSetup /golem/Dirigent/Setups/ChamberConditioning.setup'

bash -c /golem/Dirigent/Operation/Session/Setup/OpenCurrentSessionColdStart.sh
"

#!/bin/bash

source ../../../Commons.sh
IP=192.168.2.127


function Wake-upCall { :; }

function GetReadyTheDischarge { :; }

function SecurePostDischargeState { :; }

function Sleep-downCall { :; }


function Arming()
{
  ssh golem@$IP -f "python read_daq.py --trigger '/PXIe-6358/ai0' --card '/PXIe-6358' --channels 1,2,3  --nsamples 5000 --samplerate 100000 -o data.csv" >StandardLog 2>ErrorLog
}


function RawDataAcquiring()
{
    scp amos@TimepixPC:data.csv .
    ssh amos@TimepixPC "rm data.csv"

}

from=47701
to=47789
soubor=index.html; 
system=Devices/Oscilloscopes/TektrMSO64-a
url=https://golem.fjfi.cvut.cz/shots/
#url=/shots

echo "<html><body><table>" > $soubor;
echo '' >yes-shots.txt

for i in `seq $from 47704` `seq 47767 $to`; do echo -ne '*'$i;
if
[[ -e "/golem/database/operation/shots/$i/Diagnostics/BasicDiagnostics/graph.png" ]] &&
[[ -e "/golem/database/operation/shots/$i/Devices/DASs/NI-squid/ScreenShotAll.png" ]] &&
[[ $(< /golem/shots/$i/Diagnostics/PlasmaDetection/Results/b_plasma) != "0.000" ]]; then
echo -ne yes';'
echo $i>>yes-shots.txt
echo "<tr>
<td><a href=$url/$i>$i</a><br>`cat /golem/database/operation/shots/$i/shot_date`<br>`cat /golem/database/operation/shots/$i/shot_time`</td>
<td><img src=$url/$i/Diagnostics/BasicDiagnostics/graph.png></td>
<td><a href=$url/$i/Devices/DASs/NI-squid/ScreenShotAll.png><img src=$url/$i/Devices/DASs/NI-squid/rawdata.jpg></a></td>
</tr>" >> $soubor; else echo -ne no';';fi;done

echo "</table><hr/><h2>For generation bash script used</h2><pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;
echo **** yes-shots ***
cat yes-shots.txt

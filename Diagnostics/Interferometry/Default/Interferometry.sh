#!/bin/bash

 
BasePath=../..;source $BasePath/Commons.sh
source Universals.sh

DAS="Oscilloscopes/TektrDPO3014-a/Interferometry"

DeviceList=$DAS

notebook=phase_detection_density.ipynb

 
function GetReadyTheDischarge
{
    GeneralTableUpdateAtDischargeBeginning
}

# function PostDischargeAnalysis() is @Commons


function Analysis
{
local nb_id="phase_detection_density"


    IfNoPlasmaThenReturn
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat $SHMS/shot_no`/g" $nb_id.ipynb
    jupyter-nbconvert --execute $nb_id.ipynb --to html --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    
    UpdateDataBaseQuantAtCurrentShot diagnostics.interferometry ne_lav_max $(awk 'BEGIN {print ARGV[1]/1e18}' $(<ne_lav_max))
    UpdateDataBaseQuantAtCurrentShot diagnostics.interferometry ne_lav_mean $(awk 'BEGIN {print ARGV[1]/1e18}' $(<ne_lav_mean))

    convert -resize $icon_size icon-fig.png graph.png

    mkdir -p Results
    ln -s ../ne_lav.csv Results/ne_lav.csv
    ln -s ../ne_lav_max Results/ne_lav_max
    ln -s ../ne_lav_mean Results/ne_lav_mean
    ln -s ../ne_lav_unrepaired.csv Results/ne_lav_unrepaired.csv
}

function NoAnalysis
{

#Workarround
    echo "<HTML><HEAD><TITLE></TITLE><BODY>
    <H1>Diagnostics analysis is not ready yet</H1>
    <!-- <img src="icon-fig.png"></img>-->
    <br></br></BODY></HTML>" > analysis.html
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
#Finals      
    convert -resize $icon_size icon-fig.png graph.png
}



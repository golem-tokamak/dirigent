from=40000
to=40100
soubor=index.html; 
diag=Diagnostics/LimiterMirnovCoils
url=https://golem.fjfi.cvut.cz/shots/
#url=/shots

echo "<html><body><table>" > $soubor;

for i in `seq $from $to`; do echo -ne '*'$i:; 
if [[ -d "/golem/database/operation/shots/$i/$diag" ]] && [[ $(< /golem/shots/$i/Diagnostics/BasicDiagnostics/Results/is_plasma) != "0.000" ]]; then 
echo -ne yes';'
echo "<tr>
<td><a href=$url/$i>$i</a><br>`cat /golem/database/operation/shots/$i/shot_date`<br>`cat /golem/database/operation/shots/$i/shot_time`</td>
<td><img src=$url/$i/Diagnostics/BasicDiagnostics/graph.png></td>
<td><img src=$url/$i/$diag/graph.png></td>
</tr>" >> $soubor; else echo -ne no';';fi;done

echo "</table><hr/><pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;

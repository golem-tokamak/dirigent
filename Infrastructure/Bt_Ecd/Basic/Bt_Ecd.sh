#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source Universals.sh

DAS="Oscilloscopes/RigolMSO5204-c/Bt_Ecd_Monitor"

DeviceList="ITs/RasPi3-c/Bt_Ecd Infrastructure/Racks/Bt/Basic RelayBoards/Quido8-a/PowerSupplies Infrastructure/Racks/Ecd/Basic $DAS"

#casem: RelayBoards/Quido16-c/Bt-Ecd

function GetReadyTheDischarge
{
        GeneralTableUpdateAtDischargeBeginning
}


function SecurePostDischargeState
{
        SwitchRelay PowerSupplyHVcontactor@PowerSupplies OFF
        mRelax
        SwitchRelay ShortCircuits@PowerSupplies OFF
    #due to backwards compatibility:
    cp $SHM0/Infrastructure/Bt_Ecd/Parameters/t_bt $SHM0/Production/Parameters/TBt
    cp $SHM0/Infrastructure/Bt_Ecd/Parameters/t_cd $SHM0/Production/Parameters/Tcd
    cp $SHM0/Infrastructure/Bt_Ecd/Parameters/o_bt $SHM0/Production/Parameters/Bt_orientation
    cp $SHM0/Infrastructure/Bt_Ecd/Parameters/o_cd $SHM0/Production/Parameters/CD_orientation
}


function PostDischargeAnalysis()
{

Call $SHM0/Devices/`dirname $DAS` PostDischargeAnalysis
GenerateDiagWWWs
}

function GenerateDiagWWWs
{

local cesta="Infrastructure/$instrument"
echo "`cat $SHM0/$cesta/Parameters/WholeParametersDefinition`" >$cesta/command.html

echo "<h3>Bt and Ecd <a href=$cesta/ title="Data directory">$diricon</a>,<a href=Diagnostics/InfrastructureCurrents/ title="Data directory">$diricon</a></h3>
Parameters <a href=$cesta/Parameters/ title="Parameters">$parametersicon</a>:`cat $SHM0/Production/Parameters/infrastructure.bt_ecd`" >include.html;

echo "<table><tr>
<td><a href=$cesta/port_$port.jpg title="Infrastructure placement"><img src=$cesta/port_$port.jpg width='$iconsize'/></a></td>
<td><img src="$cesta/setup.png" width="$iconsize"></td>

<td><a href=https://www.dropbox.com/$DropBox title="Photogallery@DropBox"><img src=$cesta/Overview-thumb.jpg /></a></td>

<td><a href="Devices/`dirname $DAS`/ScreenShot.png"><img src="Devices/`dirname $DAS`/rawdata.jpg"></a></td>
<td>" >> include.html

echo "<a href="Diagnostics/InfrastructureCurrents/DAS_raw_data_dir/ScreenShotAll.png"><img src="Diagnostics/InfrastructureCurrents/DAS_raw_data_dir/rawdata.jpg"></a>" >> include.html

echo "</td></tr></table>" >> include.html

}














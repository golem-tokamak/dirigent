#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh
PUMP_id=a

function OpenValves
{
    LogIt "Opening Valves-a"
    Call $RelayBoards MajorVent-"$PUMP_id"@Vacuum  ON
    Call $RelayBoards MinorVent-"$PUMP_id"@Vacuum  ON
}

function CloseValves
{
LogIt "Closing Valves-a"

    Call $RelayBoards MajorVent-"$PUMP_id"@Vacuum  OFF
    Call $RelayBoards MinorVent-"$PUMP_id"@Vacuum  OFF

}




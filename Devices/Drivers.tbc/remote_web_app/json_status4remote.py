#!/usr/bin/env python3
import json
import os
from os.path import join
import subprocess
from time import strftime, sleep


def run_shell(shell_command, executable='/bin/bash'):
    """Run given string of commands in the shell and return output

    by default uses bash

    can be used as

    .. highlight:: python

       shmc = run_shell('source Commons.sh; echo $SHM0')

    """
    out = subprocess.check_output(shell_command, shell=True,
                                  executable=executable)
    return out.decode('utf-8')


def write2file(fname, value, mode='w'):
    """Write str(value) into file fname

    By default overwrites,
    change mode to 'a' for append
    """
    with open(fname, mode) as fout:
        fout.write(str(value))


def catfile(fname, default=None):
    """Reads file fname and tries to return contents as int, float or str

    the types are tried in that order until it can be parsed

    if the file cannot be opened, the default is returned
    """
    try:
        with open(fname) as fin:
            content = fin.read()
    except IOError:
        return default
    try:
        return int(content)
    except ValueError:
        try:
            return float(content)
        except ValueError:
            return content.strip()


def main_loop(sleep_seconds):
    output = run_shell('source Commons.sh; echo $SHM0; echo $SHML')
    current_shot_path, session_logbook_path = output.strip().splitlines()

    # updates only while the information is available
    # TODO perhaps leave it running forever or end with some reasonable values
    while True:
        create_json_status(current_shot_path, session_logbook_path)
        sleep(sleep_seconds)

def create_json_status(current_shot_path, session_logbook_path):
    current_data = dict(
        time=strftime("%H:%M:%S"),
        U_Bt=catfile(join(session_logbook_path,
                          'U_Bt_now'), default=0),
        U_CD=catfile(join(session_logbook_path,
                          'U_CD_now'), default=0),
        p_ch=catfile(join(session_logbook_path,
                          'ActualChamberPressuremPa'), default=0),
        tokamak_state=catfile(join(session_logbook_path,
                                   'tokamak_state'), default='idle'),
    )
    if current_data['tokamak_state'] != 'idle':
        current_data['discharge_params'] = dict(
            UBt=catfile(join(current_shot_path, 'RASPs/Charger/U_Bt_target')),
            Ucd=catfile(join(current_shot_path, 'RASPs/Charger/U_CD_target')),
            pressure=catfile(join(current_shot_path,
                                  'RASPs/Chamber/pressure')),
            comment=catfile(join(current_shot_path, 'comment')),
            gas=catfile(join(current_shot_path, 'RASPs/Chamber/gas')),
            preionization=catfile(join(current_shot_path,
                                       'RASPs/Discharge/preionization')),
            shot_no=catfile(join(current_shot_path, 'shot_no')), )
    else:                       # idle
        current_data['discharge_params'] = {}
    json_str = json.dumps(current_data)
    write2file(os.path.join(session_logbook_path, 'golem_status.json'), json_str)


if __name__ == '__main__':
    main_loop(sleep_seconds=1)

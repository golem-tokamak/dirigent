<html><body>
<h1></h1>
<h2>From #44421 to #44430</h2>
<table>
<tr><th>ShotNo</th><th>Basic diagnostics</th><th>Tags</th></tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44421>44421</a><br>24-04-10<br>11:04:57</td><td>
    
   44421 |        8.28100002

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44421/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44422>44422</a><br>24-04-10<br>11:10:02</td><td>
    
   44422 |        8.92999998

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44422/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44423>44423</a><br>24-04-10<br>11:13:31</td><td>
    
   44423 |        8.66500002

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44423/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44424>44424</a><br>24-04-10<br>11:17:00</td><td>
    
   44424 |        8.15700002

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44424/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44425>44425</a><br>24-04-10<br>11:20:33</td><td>
    
   44425 | 8.057000039999998

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44425/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44426>44426</a><br>24-04-10<br>11:24:05</td><td>
    
   44426 | 7.873999960000001

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44426/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44427>44427</a><br>24-04-10<br>11:27:43</td><td>
    
   44427 | 7.790000020000001

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44427/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44429>44429</a><br>24-04-10<br>15:25:52</td><td>
    
   44429 | 5.398999999999999

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44429/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
<tr>
    <td><a href=https://golem.fjfi.cvut.cz/shots//44430>44430</a><br>24-04-10<br>16:09:28</td><td>
    
   44430 |             5.827

</td><td><a href=DataBase/InvertTag.php>php</a></td><td><img src=https://golem.fjfi.cvut.cz/shots//44430/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>
</table>Alltogether 9 discharges<hr/>
<h2>For generation bash script used</h2>
<pre>
FROM=44421
TO=44430
soubor=index.html;
soubor=index.php;
url=https://golem.fjfi.cvut.cz/shots/
t_plasma_duration_limit=7
title=''
SHOTS='/golem/shots'
export PGPASSWORD=`cat /golem/production/psql_password`
PSQL='psql -d golem_database -U golem -t -c'

echo "&lt;html&gt;&lt;body&gt;
&lt;h1&gt;$title&lt;/h1&gt;
&lt;h2&gt;From #$FROM to #$TO&lt;/h2&gt;
&lt;table&gt;" &gt; $soubor;
echo "&lt;tr&gt;&lt;th&gt;ShotNo&lt;/th&gt;&lt;th&gt;Basic diagnostics&lt;/th&gt;&lt;th&gt;Tags&lt;/th&gt;&lt;/tr&gt;" &gt;&gt; $soubor;
Counter=0
for i in `seq $FROM $TO`; do echo -ne '*'$i:;
if [[ -e "$SHOTS/$i/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg" ]] \
; then
    let Counter++ #increment
    echo -ne yes';'
    echo "&lt;tr&gt;
    &lt;td&gt;&lt;a href=$url/$i&gt;$i&lt;/a&gt;&lt;br&gt;$(&lt;$SHOTS/$i/shot_date)&lt;br&gt;$(&lt;$SHOTS/$i/shot_time)&lt;/td&gt;&lt;td&gt;
    "&gt;&gt; $soubor;
    $PSQL "SELECT  shot_no, t_plasma_duration FROM diagnostics.basicdiagnostics WHERE shot_no=$i"&gt;&gt; $soubor;
    echo "&lt;/td&gt;&lt;td&gt;&lt;a href=DataBase/InvertTag.php&gt;php&lt;/a&gt;&lt;/td&gt;&lt;td&gt;&lt;img src=$url/$i/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg&gt;&lt;/td&gt;
    &lt;/tr&gt;" &gt;&gt; $soubor;
else echo -ne no';';fi;done
echo "&lt;/table&gt;Alltogether $Counter discharges&lt;hr/&gt;
&lt;h2&gt;For generation bash script used&lt;/h2&gt;
&lt;pre&gt;
`cat script.bash|sed 's/&lt;/\&lt;/g'|sed 's/&gt;/\&gt;/g'`
&lt;/pre&gt;&lt;/body&gt;&lt;/html&gt;" &gt;&gt; $soubor;

#UNDER CONSTRUCTION, COMMAND LINE SNB:
#psql -c "SELECT  dsch.shot_no, bdg.t_plasma_duration FROM diagnostics.basicdiagnostics bdg,operation.discharges dsch WHERE dsch.shot_no=44200" -qAt -U  golem golem_database
#UPDATE table SET boolean_field = NOT boolean_field WHERE id = :id
</pre></body></html>

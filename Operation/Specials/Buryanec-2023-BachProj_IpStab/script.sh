#!/bin/bash

BasePath=".";source $BasePath/Commons.sh


function Prepare
{
    Call $SW/Devices/Oscilloscopes/TektrMSO56-a/BasicDiagnostics OpenSession
}

function Engage
{
    Call $SW/Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_ON
    Call $SW/Devices/PowerSupplies/BEZ-HV/Basic ShortCircuitOFF;
    ssh Charger 'source Rasp-Bt_Ecd.sh;LV_PS_test_ON;CdHVrelayON'
    sleep 8
    ssh Charger 'source Rasp-Bt_Ecd.sh;CdHVrelayOFF'
    sleep 1
    echo ":OUTP1 ON"|netcat -w 1 192.168.2.180 5555
}


function Trigger
{
    echo "FPANEL:PRESS SINGLESEQ"|netcat -q 1 BasicDiagnostics 4000
    #Speaker CountDown   
    sleep 1
    Call $SW/Infrastructure/Triggering/Basic TriggerTest1x
    sleep 1
}

function Disengage
{
    ssh Charger 'source Rasp-Bt_Ecd.sh;CdHVrelayOFF;sleep 5;LV_PS_test_OFF;'
    Call $SW/Devices/PowerSupplies/BEZ-HV/Basic ShortCircuitON;
    Call $SW/Devices/RelayBoards/Quido4-a/Racks_Misc 12V_24V_intosystem_OFF 
    echo ":OUTP1 OFF"|netcat -w 1 192.168.2.180 5555
}   

function Action
{
    Engage
    Trigger
    Disengage
}    

#@Dg
#source /golem/Dirigent/Operation/Specials/Buryanec-2023-BachProj_IpStab/script.sh;Prepare

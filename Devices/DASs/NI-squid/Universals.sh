#!/bin/bash


function Wake-upCall()
{
    LogIt $ThisDev
    shuf -i 0-5|head -1|xargs sleep # To Protect electrical grid
    wakeonlan -i 192.168.2.255 6C:F0:49:E0:C0:81
}

function Sleep-downCall()
{
    LogIt $ThisDev
    ssh golem@$ThisDev.golem 'sudo /sbin/shutdown -h now'
}

function ShutDown()
{
    Sleep-downCall

}

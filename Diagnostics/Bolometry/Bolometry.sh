SHM="/dev/shm/golem"
source $SHM/Commons.sh
source $SHM/Tools.sh

function PostDischargeFinals 
{
    #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" notebook.ipynb
    jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --execute notebook.ipynb --output analysis.html 2>ErrorLog

    convert -resize $icon_size icon-fig.png icon.png

}


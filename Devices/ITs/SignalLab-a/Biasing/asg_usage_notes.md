# **A**rbitrary **S**ignal **G**nerator on **R**ed**P**itaya

## Why RP
 I have decided to use RP as a arbitrary signal generator because I have
 spend some time with that recently and I have found that it can be easily
 used as ASG, with the advantage of python interface. It is a matter of fact
 that it acually uses SCPI controll under the hood, but one may easily create
 the waveforms, which is much more convenient than the way Daniela had to
 invent to contol Kepcos for plasma position stabilization.
 One may even play with the waveform a bit more.
 As I can not find the script that controlled the original
 ASG Rigol GZ 1032 Z at the time (18/09/22), I can not be sure but I think that
 the values had to be rounded before sending to the ASG Rigol. It is not the case
 of using RP as ASG. However I am affraid that there might be a problem with
 insufficient voltage output range of RP. It is limited to +/- 5 V.

# TODO - check the documentation to Kepcos, what voltage will it give for what input.
# TODO - after RP reset do a proper settings of all parameters

Anyway. The ASG, at least when controlled via SCPI, has a 16383 sample long buffer.
There is a SOUR:FREQ:FIX <frequency> command that sets the frequency with which
the buffer is outputed i.e. how many times per second will the buffer start.
-- Note: It is funny to give it a buffer that does not end at the same value it starts. --
The way I plan to use it is following.
I will set the frequency to about 1/(plasma duration) so that the buffer will be long as
a pulse. It gives about 800 samples/ms for a 20 ms long pulse. As I expect to use
biasing frequencies of about 20 kHz I will have 40 samples for one period of the signal.

The resulting waveform will start with zeros, at given time, a harmonic ~ 15 kHz signal
will start and before the end of the dicharge (or shortly after, it depends on what we
want to have) it will drop back to zero.

20.09.22 - I will stick to frequency SOUR:FREQ:FIX <frequency> of 50 Hz, so that I can 
rely on 20 ms long buffer, that will make control much easier. I do not expect longer 
pulses.


from bayes_opt import BayesianOptimization
import csv
import numpy as np
import time
import subprocess, sys

iteration = 0
"""Funkce čtení dat z tokamaku"""
def get_GOLEM_time():
    #time.sleep(60) #sekundy
    # setup url
    url = '/golem/shots/0/Diagnostics/PlasmaDetection/Results/t_plasma_duration'
    #url = "http://golem.fjfi.cvut.cz/shots/0/Diagnostics/PlasmaDetection/Results/t_plasma_duration"
    # Try to load if webpage/parameter value does not exists 0 is assumed
    try:
        number = np.loadtxt(url)
        return number
    except:
        return 0

"""Funkce ovládání tokamaku"""
def tokamak_controler(x1,x2,x3,x4):
    tokamak_command = f"""./Dirigent.sh --discharge --operation.discharge "style='standard',vacuum_shot='-1'" --infrastructure.bt_ecd "U_Bt={x2:.1f},t_Bt=0,O_Bt='CW',U_cd={x1:.1f},t_cd={x3:.1f},O_cd='CW'" --infrastructure.workinggas "S_mode='U_v',S_gas='H',p_H=10,U_v={x4:.2f}" --diagnostics.fastcameras "fr_ux100a=1200,height_ux100a=56,recrate_ux100a=40000,shotdur_ux100a=30,width_ux100a=1280,recrate_ux50a=2000" --comment 'fyzikalni_seminar_ba{iteration}'"""

    #kód pro tokamak
    subprocess.run(tokamak_command, shell = True, executable="/bin/bash")
    time = get_GOLEM_time()
    print('Duration ',time)
    return float(time)

"""Blackbox funkce"""
def tokamak_function(U_cd,U_bt,delta_t,p):
    global iteration
    iteration += 1

    time = tokamak_controler(U_cd,U_bt,delta_t,p)

    """CSV zápis výsledku do souboru"""
    #with open('results.csv', 'a+', newline='') as csvfile:
    #    writer = csv.writer(csvfile, delimiter=' ',
    #                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    #    writer.writerow([iteration, U_cd, time])
        
    return time

"""nastavení definičního oboru parametrů"""
pbounds = {'U_cd': (300, 700),'U_bt': (700, 1300),'delta_t': (200, 3000),'p': (16.5, 20)}

"""deklarace optimizéru"""
optimizer = BayesianOptimization(
    f=tokamak_function,
    pbounds=pbounds,
    verbose=0,
    random_state=1,
)

"""Spuštění samotné optimalizace"""
optimizer.maximize(init_points=2, n_iter=10)

"""Zpracování výsledného maxima"""
target = optimizer.max["target"]
parameters_for_target = [float(optimizer.max["params"][i]) for i in optimizer.max["params"]]

print(f" Maximum found as: {target} ms, for parameters: {parameters_for_target}")

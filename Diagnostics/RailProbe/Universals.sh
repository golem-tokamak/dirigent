#!/bin/bash

function DefineDiagTable()
{
    #CreateDiagnosticsTable $diag_id
    #AddColumnToDiagnosticsTable $diag_id r_lp_tip integer
    psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.r_lp_tip IS 'The distance between the Lang probe tip and plasma center [mm]'" -q -U golem golem_database
    #AddColumnToDiagnosticsTable $diag_id f_lp integer
    psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.f_lp IS 'The sweeping frequency on the LP tip [kHz]'" -q -U golem golem_database
    #AddColumnToDiagnosticsTable $diag_id scan text
    #psql -c "COMMENT ON COLUMN  diagnostics.$diag_id.scan IS 'Arbitrary scan definition for scripts [-]'" -q -U golem golem_database
}


function Gnuplot_Analysis
{
# workarround
    echo -n "set terminal png;unset xtics;set size 1,1;set origin 0,0;set multiplot layout $LastChannelToAcq,1 columnsfirst scale 1.1,1;set datafile separator ',';" >/tmp/foo; for i in `seq 1 $LastChannelToAcq`; do echo -n plot \'${diags[$i-1]}'.csv'\' u 1:2';';done >>/tmp/foo;echo " unset multiplot" >>/tmp/foo; cat /tmp/foo|gnuplot > icon-fig.png

convert -resize $icon_size icon-fig.png graph.png
}

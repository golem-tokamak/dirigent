#!/bin/bash

# create command file
control_file="/tmp/golemRemoteCommand"
echo "run" > "$control_file"

tmux kill-session -t golem_remote_session
tmux new-session -s golem_remote_session -n GOLEM -d 'watch -t "bash /golem/Dirigent/Operation/Session/Setup/PsTree.sh"'
tmux split-window -v -t golem_remote_session 'tail -n -0 -F /dev/shm/golem/ActualSession/SessionLogBook/GlobalLogbook'
tmux split-window -v -t golem_remote_session 'Operation/Remote/RunRemote.sh'
tmux split-window -v -t golem_remote_session 'bash Operation/Remote/displayMenu.sh'
tmux attach-session -t golem_remote_session






rm $control_file
exit 0

#xterm -fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb  -fg yellow -bg blue -title "Session log@GOLEM tokamak" -geometry 200x200+0+0 -e 'bash /golem/Dirigent/Operation/Session/Setup/Tmux.sh' #@Dg

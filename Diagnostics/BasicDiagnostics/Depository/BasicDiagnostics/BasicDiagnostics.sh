SHM="/dev/shm/golem"
source $SHM/Commons.sh
source $SHM/Tools.sh
OUT=onstage.html
namesize=100
iconsize=200

DAS=TektrMSO64-a


function Arming()
{
    echo "OK"
}



function PostDischargeFinals 
{
    export SHOT_NO=`cat ../../shot_no` # for linux
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" notebook.ipynb
    jupyter-nbconvert --execute notebook.ipynb --output analysis.html 2>ErrorLog

    cp /$SHM/Management/imgs/DAS.jpg .
    convert -resize $icon_size icon-fig.png graph.png
    convert -resize $icon_size icon-fig.png $SHM/ActualShot/Diagnostics/BasicDiagnostics/analysis.jpg
    convert /$SHM/Management/imgs/DAS_icon.jpg graph.png +append icon_.png
    convert -bordercolor Black -border 2x2 icon_.png icon.png
    
#    convert -resize $icon_size icon-fig.png icon.png


    echo '
     <table>
<tbody>
    <tr class="data-flow"><th>Data flow</th><th >measurement &rarr;</th><th >digitization &rarr;</th><th >saving &rarr;</th><th></th></tr>
<tr><th>Name</th><th>Experiment setup</th><th>Data acquistition system</th><th>Raw data</th><th>All files</th></tr>
    ' > $OUT 
    dir=Diagnostics;
    DiagnosticsID=BasicDiagnostics;
    echo "<tr>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/$dir/$DiagnosticsID/><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/$dir/$DiagnosticsID/name.png  width='$namesize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/setup.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/setup.png  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/das.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/das.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/rawdata.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/rawdata.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/analysis.html><img src=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/analysis.jpg  width='$iconsize'/></a></td>
    <td valign=bottom><a href=http://golem.fjfi.cvut.cz/shots/$SHOT_NO/Diagnostics/$DiagnosticsID/><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='100px'/></a></td></tr></tbody></table>" >> $OUT;


}


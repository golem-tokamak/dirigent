
#### Toroidal Field Capacitor
Voltage on the capacitors $U_\mathrm{B_t}$ to be discharged into the toroidal field coils.  
The higher the voltage, the larger the magnetic field confining the plasma.

#### Current Drive Capacitor
Voltage on the capacitors $U_\mathrm{cd}$ to be discharged into the primary transformer winding.  
The higher the voltage, the larger the electric field creating and heating the plasma. The electric field capacitors are discharged after a configurable delay with respect to the magnetic field capacitors.


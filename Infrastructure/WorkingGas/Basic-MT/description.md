Set the pressure and type of the working gas from which the plasma is formed. Pressure must be high enough for plasma to form, but low enough for gas breakdown to occur.

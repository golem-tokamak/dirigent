#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source Universals.sh

shot_no=`CurrentShotDataBaseQuerry shot_no`

setup_id="TimepixDetector"

#DeviceList="ITs/TimepixPC/Timepix Radiation/AdvaPix_CdTe-a  Radiation/AdvaPix_CdTe-b"
DeviceList="ITs/TimepixPC/Timepix Radiation/Timepix/AdvaPix_CdTe-F10 Tools/Chillers/CW-5200-a"

DAS="Radiation/Timepix/AdvaPix_Si-H03"

#account="pi@RasPi4-c"
#Rasps="RasPi4-c RasPi4-b"
Rasps="TimepixPC"

function GetReadyTheDischarge
{
    GeneralTableUpdateAtDischargeBeginning
}


function Arming()
{
ssh amos@TimepixPC "rm /home/amos/measGolem/F10-W0049/*"
#ssh amos@TimepixPC "rm /home/amos/measGolem/H03-W0051/*"
#ssh amos@TimepixPC "rm /home/amos/measGolem/E05-W0037/*"
#ssh amos@TimepixPC "python3 arming2903_23.py $shot_no > /dev/null 2>/dev/null &"
}



function PostDischargeAnalysis()
{
    if [[ ! -d DAS_raw_data_dir ]]; then ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir; fi


	sleep 5
	echo "PostDischargeAnalysis @ F10"
		mkdir -p F10
		#mkdir -p calib_matrix_H03
		scp amos@TimepixPC:measGolem/F10-W0049/* F10/
		#scp amos@TimepixPC:measGolem/calib_matrix_H03-W0051/* calib_matrix_H03/ 
		sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" basicCdTe.ipynb
		jupyter-nbconvert  --ExecutePreprocessor.timeout=40 --to html --execute basicCdTe.ipynb --output basicCdTe.html         > >(tee -a jup-nb_stdout-basicCdTe.log) 2> >(tee -a jup-nb_stderr-basicCdTe.log >&2)
		convert -resize $icon_size hit-map.png rawF10.png
		convert -resize $icon_size icon-fig.png graph.png
	#done
	cp rawF10.png DAS_raw_data_dir/rawdata.jpg

	#echo "PostDischargeAnalysis @ F10"
	#	mkdir -p F10
	#	scp amos@TimepixPC:measGolem/F10-W0049/* F10/
	#	sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" RasPi4-b_0604_23.ipynb
	#	jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute RasPi4-b_0604_23.ipynb --output RasPi4-b_0604_23.html         > >(tee -a jup-nb_stdout-RasPi4-b_0604_23.log) 2> >(tee -a jup-nb_stderr-RasPi4-b_0604_23.log >&2)
	#	convert -resize $icon_size icon-fig_F10.png RasPi4-b.png
	#done
	#cp RasPi4-b.png graph.png
    notebook=basicCdTe.ipynb
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox

	#HtmlGenerat

}

function HtmlGenerat()
{
SHOT_NO=`cat $SHM/shot_no`
echo "" > diagrow.html
#local cesta="Diagnostics/TimepixDetector/MalecSFP23"
echo "<tr>
    <td valign=bottom><img src=Diagnostics/$instrument/name.png  width='100'/>&nbsp;</td>
	<td valign=bottom><a href=Diagnostics/$instrument/setup.png title='Diagnostics placement'><img src=Diagnostics/$instrument/setup.png  width='200'/>&nbsp;</a></td>
    <td valign=bottom><img src=Diagnostics/$instrument/expsetup_pixeldet.svg  width='200'/>&nbsp;</td>
    <td valign=bottom><a href=https://www.dropbox.com/scl/fo/ru1be2nygsjxgit643ezh/h?rlkey=bp0uevugs9eqg6lir4jra590h&dl=0 title=Photogallery@DropBox><img src=Diagnostics/TimepixDetector/Overview-thumb.jpg /></a>
    </td>
    <td valign=bottom><img src=Diagnostics/$instrument/device.png  width='200'/>&nbsp;</td>
    <td valign=bottom><img src=Diagnostics/$instrument/rawF10.png  width='200'/>&nbsp;</td>
    <td valign=bottom><img src=Diagnostics/$instrument/analysis.png  width='200'/>&nbsp;</td>
    #<td valign=bottom><img src=Diagnostics/$instrument/RasPi4-b.png  width='200'/>&nbsp;</td> 
    <td></td>
    <td valign=bottom><a href=Diagnostics/$instrument/ title=Directory><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='30px'/></a></td>
    <td></td>
    <td></td>
    </tr>" >> diagrow.html
}

#function HtmlGenerat_zmb()
#{
#	<td valign=bottom><img src=$whereami/RasPi4-b.png  width='200'/>&nbsp;</td>
#	#<td valign=bottom><img src=$whereami/RasPi4-b.png  width='200'/>&nbsp;</td>
#}


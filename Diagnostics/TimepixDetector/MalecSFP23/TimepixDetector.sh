#!/bin/bash

SHM="/dev/shm/golem"
source $SHM/Commons.sh
#rPi=192.168.2.92 ## rPI IP adress 
shot_no=`CurrentShotDataBaseQuerry shot_no`

setup_id="TimepixDetector"

DeviceList="ITs/TimepixPC/Timepix Radiation/AdvaPix_CdTe-a  Radiation/AdvaPix_CdTe-b"

#account="pi@RasPi4-c"
#Rasps="RasPi4-c RasPi4-b"
Rasps="TimepixPC"

function Arming()
{
ssh amos@TimepixPC "rm /home/amos/measGolem/F10-W0049/*"
#ssh amos@TimepixPC "rm /home/amos/measGolem/H03-W0051/*"
ssh amos@TimepixPC "rm /home/amos/measGolem/E05-W0037/*"
ssh amos@TimepixPC "python3 arming2903_23.py $shot_no > /dev/null 2>/dev/null &"
}



function PostDischargeAnalysis()
{
	sleep 5
	echo "PostDischargeAnalysis @ F10"
		mkdir -p F10
		scp amos@TimepixPC:measGolem/F10-W0049/* F10/
		sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" RasPi4-b_0604_23.ipynb
		jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute RasPi4-b_0604_23.ipynb --output RasPi4-b_0604_23.html         > >(tee -a jup-nb_stdout-RasPi4-b_0604_23.log) 2> >(tee -a jup-nb_stderr-RasPi4-b_0604_23.log >&2)
		convert -resize $icon_size icon-fig_F10.png RasPi4-b.png
	#done
	cp RasPi4-b.png graph.png
	
	#cp H03.png graphH03.png
	echo "PostDischargeAnalysis @ E05"
		mkdir -p E05
		scp amos@TimepixPC:measGolem/E05-W0037/* E05/
		sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" RasPi4-c_0604_23.ipynb
		jupyter-nbconvert  --ExecutePreprocessor.timeout=20 --to html --execute RasPi4-c_0604_23.ipynb --output RasPi4-c_0604_23.html         > >(tee -a jup-nb_stdout-RasPi4-c_0604_23.log) 2> >(tee -a jup-nb_stderr-RasPi4-c_0604_23.log >&2)
		convert -resize $icon_size icon-fig_E05.png RasPi4-c.png
	#done
	cp RasPi4-c.png graph.png
	#cp H03.png graphH03.png


	HtmlGenerat

}

function HtmlGenerat()
{
SHOT_NO=`cat $SHM/shot_no`
echo "" > diagrow_$setup_id.html
#local cesta="Diagnostics/TimepixDetector/MalecSFP23"
echo "<tr>
    <td valign=bottom><img src=$whereami/name.png  width='100'/>&nbsp;</td>
    <td valign=bottom><img src=$whereami/setup.png  width='200'/>&nbsp;</td>
    <td></td>
    <td valign=bottom><img src=$whereami/device.png  width='200'/>&nbsp;</td>
    <td></td>
    <td valign=bottom><img src=$whereami/RasPi4-b.png  width='200'/>&nbsp;</td>
    <td valign=bottom><img src=$whereami/RasPi4-c.png  width='200'/>&nbsp;</td> 
    <td></td>
    <td valign=bottom><a href=$whereami/ title=Directory><img src=http://golem.fjfi.cvut.cz/_static/direct.png  width='30px'/></a></td>
    <td></td>
    <td></td>
    </tr>" >> diagrow_$setup_id.html
}

#function HtmlGenerat_zmb()
#{
#	<td valign=bottom><img src=$whereami/RasPi4-b.png  width='200'/>&nbsp;</td>
#	#<td valign=bottom><img src=$whereami/RasPi4-b.png  width='200'/>&nbsp;</td>
#}


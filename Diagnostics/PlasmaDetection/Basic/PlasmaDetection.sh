#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh


DAS="Oscilloscopes/TektrMSO56-a"
#DeviceList="Oscilloscopes/TektrMSO56-a/PlasmaDetection"
DeviceList="Oscilloscopes/TektrMSO56-a/BasicDiagnostics"






# Scope file set-up: TIME,CH1,CH2,CH3,CH4,,TIME,MATH1,,TIME,MATH2
#columns="2 3 4 5 8 11" # Tek MSO64
columns="2 3 4 5 10 13" # Tek MSO56
diags=('U_Loop' 'U_BtCoil' 'U_RogCoil' 'U_LeybPhot' 'U_IntBtCoil' 'U_IntRogCoil')

function GetReadyTheDischarge ()
{
    GeneralTableUpdateAtDischargeBeginning
}


function PostDischargeAnalysis()
{
    #GeneralDAScommunication $DAS RawDataAcquiring
    Call $SHM0/Devices/$DAS BasicDiagnosticsRawDataAcquiring
    ln -s ../../Devices/$DAS DAS_raw_data_dir
    
    n=0;for i in $columns; do tail -q -n +13  DAS_raw_data_dir/TektrMSO56_ALL.csv |awk -F "," '{print $1","'\$$i'}'> ${diags[$n]}.csv;((n+=1)); done;
    Analysis
}  

function Analysis ()
{
    ln -s ../../../Production/Parameters/ Parameters
    #mkdir Results
    export SHOT_NO=`cat ../../../shot_no` # for linux
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" DetectPlasma.ipynb
    jupyter-nbconvert --execute DetectPlasma.ipynb --to html --output DetectPlasma.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    
    if [[ $(< Results/b_plasma) != "0.000" ]]; 
    then 
        touch $BasePath/Production/Tags/b_plasma-YES;
    else    
        touch $BasePath/Production/Tags/b_plasma-NO;
    fi
    

    
}


# Tuning:

#cd "/golem/shm_golem/ActualShot/Diagnostics/BasicDiagnostics/"; cp /golem/svoboda/Dirigent/Diagnostics/BasicDiagnostics/DetectPlasma.* .;source DetectPlasma.sh ;PostDischargeAnalysis

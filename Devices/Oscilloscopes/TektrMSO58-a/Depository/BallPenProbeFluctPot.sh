#!/bin/bash

source /dev/shm/golem/Commons.sh

whoami="Devices/Oscilloscopes/TektrMSO58-a/BallPenProbeFluctPot"

source Universals.sh


function OpenSession()
{
    echo ":ACQUIRE:MODE HIRes;
    :HORIZONTAL:MODE:SAMPLERATE 2.5E6;
    :HORIZONTAL:MODE:SCALE 2.4e-3;
    FPANEL:PRESS SINGLESEQ;
    TRIGGER:A:MODE NORMAL;
    TRIGGER:A:TYPE EDGE ;
    TRIGGER:A:LEVEL:CH8 4;
    TRIGGER:A:EDGE:SOURCE CH8;
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null

    ExternDataAvailabilityTest
    
    echo "
    :CH1:LABel:NAME 'U_loop-b';:CH1:SCALE 4;:CH1:OFFSET 0;:DISplay:GLObal:CH1:STATE ON;:CH1:TERMINATION 1.0E+6;
    :CH2:LABel:NAME 'null';:CH2:SCALE 0.05;:CH2:OFFSET 0;:DISplay:GLObal:CH2:STATE OFF;:CH2:TERMINATION 1.0E+6;
    :CH3:LABel:NAME 'null';:CH3:SCALE 0.1;:CH3:OFFSET 0;:DISplay:GLObal:CH3:STATE OFF;:CH3:TERMINATION 1.0E+6;
    :CH4:LABel:NAME 'null';:CH4:SCALE 0.05;:CH2:OFFSET 0;:DISplay:GLObal:CH4:STATE OFF;:CH4:TERMINATION 1.0E+6;
    :CH5:LABel:NAME 'null';:CH5:SCALE 0.05;:CH3:OFFSET 0;:DISplay:GLObal:CH5:STATE OFF;:CH5:TERMINATION 1.0E+6;
    :CH6:LABel:NAME 'null';:CH6:SCALE 1;:CH6:OFFSET 0;:DISplay:GLObal:CH6:STATE OFF;:CH6:TERMINATION 1.0E+6;
    :CH7:LABel:NAME 'U_BPP_fl';:CH7:SCALE 5;:CH7:OFFSET 0;:DISplay:GLObal:CH7:STATE ON;:CH7:TERMINATION 1.0E+6;
    :CH8:LABel:NAME 'Trigger':CH8:SCALE 2;:CH8:OFFSET 0;:DISplay:GLObal:CH8:STATE OFF;:CH8:TERMINATION 1.0E+6;"|$COMMAND 1>/dev/null 2>/dev/null
    
    PrepareFilesToSHMs $SHMS Devices/`dirname $Drivers`

}


function Arming()
{

    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO58*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO58*.png
    echo "
    :SAVEon:TRIG ON;
    :SAVEON:WAVEform ON;
    :SAVEON:WAVEFORM:FILEFORMAT SPREADSheet;
    :SAVEon:FILE:DEST 'L:/';
    :SAVEON:IMAGE ON;
    :SAVEON:FILE:NAME 'TektrMSO58';
    `for i in 1 7 8; do echo :DISplay:GLObal:CH$i:STATE ON;done`
    `for i in 2 3 4 5 6; do echo :DISplay:GLObal:CH$i:STATE OFF;done`
    :SAVEON:WAVEform:SOURCE ALL"|$COMMAND 1>/dev/null 2>/dev/null

	SingleSeq
}
   
   

function RawDataAcquiring()
{
    # for i in `seq 1 5`; do echo Call from Petiprobe $i;sleep 1; done
    getdata "1 7"

    echo "
    :DISplay:GLObal:CH1:STATE ON;
    :DISplay:GLObal:CH2:STATE OFF;
    :DISplay:GLObal:CH3:STATE OFF;
    :DISplay:GLObal:CH4:STATE OFF;
    :DISplay:GLObal:CH5:STATE OFF;
    :DISplay:GLObal:CH6:STATE OFF;
    :DISplay:GLObal:CH7:STATE ON;
    :DISplay:GLObal:CH8:STATE OFF"|$COMMAND  1>/dev/null 2>/dev/null


    #GetOscScreenShot
}
    

The neutral working gas must be first ionized in order to break down into a plasma.  
Using the **electron gun** will locally ionize the gas. Without any ionization, no plasma can form.
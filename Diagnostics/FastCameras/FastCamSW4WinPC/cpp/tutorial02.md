@page tutorial02 Tutorial 02 - Creating the CamsConfig.json
@tableofcontents
### How to create new configuration file
In this tutorial we will describe how to create an initial configuration file, in our case CamsConfig.json. A configuration file is a file where you can specify the required parameters to be set on the cameras. All parameters with their possible values can be found in...


Specify required values via createJSON():

@code 
void createJSON(void) {
    json w;

    for (int i = 0; i < 2; i++) {
        w[i]["framesToSave"] = 1200;
        w[i]["shotDurToSave"] = 30;
        w[i]["TriggerMode"] = "TrigMan"; 
        w[i]["input1"] = "TrigPos"; 
        w[i]["input2"] = "SyncPos";
        w[i]["variableChannel"] = 1;
        w[i]["SettingsMode"] = "SetAll";

        w[i]["width"] = 1280;
        w[i]["height"] = 56;
        w[i]["recordRate"] = 40000;
        w[i]["shutterSpeed"] = 40000;
    }

    w[0]["ipAddr"] = "192.168.2.10";
    w[1]["ipAddr"] = "192.168.2.11";
    w[0]["name"] = "Camera Vertical";
    w[1]["name"] = "Camera Radial";
    w[0]["videoFileName"] = "AVI\\CamVert.avi";
    w[1]["videoFileName"] = "AVI\\CamRad.avi";

    //write to .json file
    ofstream WriteFile("CamsConfig.json");
    WriteFile << w.dump(2);
    WriteFile.close();
    cout << "The JSON file CamsConfig.json has been overwritten.\n" << endl;
    return;
}

 @endcode

Call createJSON() in the main: 
@code 
int main()
{
    createJSON();

    return 0;
}

 @endcode
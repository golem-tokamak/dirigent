	    <h1>Tokamak GOLEM - Shot Database - <a href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/" title="Permalink to this headline">#<?php echo $shot_no;?></a><a href="http://golem.fjfi.cvut.cz/shotdir/<?php echo $shot_no;?>/" title="Permalink to this headline"><?php echo "  ".$diricon; ?></a><a href="http://golem.fjfi.cvut.cz/shotdir/<?php echo $shot_no;?>/Production/qrcode.jpg" title="QR code to the discharge"><?php echo "  ".$qricon; ?></a></h1>
<!--	    <a class="viewcode-back" href="analysis_wave_0/Homepage/ErrorLog"> <tt class="file docutils literal"><span class="pre">[Web log]</span></tt></a>-->

<!--		    <a class="viewcode-back" href="http://golem.fjfi.cvut.cz/shots/<?php echo $shot_no;?>/home.php.template"><tt class="file docutils literal"><span class="pre">[Template source]</span></tt></a><br>-->


<div style=" display: inline-block;float:right;position:relative; top:100px; right:10px;  ">
<div align="right" style="width:10px;">

<!--Zde jsem vyhodil style "scoped" -->

<form name="tags">
<ul class="labels">
</ul>
</form>
</div>
</div>
<table class="docutils field-list" frame="void" rules="none">
<colgroup>
<col class="field-name" style="width: 16%;">
<col class="field-body" style="width: 83%;">
</colgroup>
<tbody valign="top">
<?php
    echo  "<tr class='field'><th class='field-name'>Time stamp</th><td class='field-body'>";
    quantity_item ('start_timestamp');
    echo "</td></tr>";
    //echo '<a href="Infrastructure/Homepage/psql/ShotsOfTheDay.php" title="Shots of the day">  '.$psqlicon.'</a>';
    echo '<a class="viewcode-back" href="ShotLogbook"> <tt class="file docutils literal"><span class="pre">[Shot logbook]</span></tt></a>';
    echo  "<tr class='field'><th class='field-name'>The session mission</th><td class='field-body'>";
    echo "<a href='whole.setup'>";
    psql_shots_request ('session_setup');
    echo "</a>";
    psql_shots_request ('session_mission');
//    echo '<a href="Infrastructure/Homepage/psql/ShotsOfTheMission.php" title="Shots of the mission">  '.$psqlicon.'</a>';
    echo "</td></tr>";
//     quantity_item ('session_id');
    echo  "<tr class='field'><th class='field-name' style='white-space: nowrap;'>";
    psql_physical_quantities_request ('description','session_id');
    echo '<a href="https://golem.fjfi.cvut.cz/tools/LastSessions/" title="session\'s history 1 year back">  '.$historyicon_small.'</a>';
    echo "</th><td class='field-body'>";
    psql_shots_request ('session_id').'</td></tr>';
    echo '<a href="Infrastructure/Homepage/psql/ShotsOfTheSession.php" title="Shots of the session">  '.$psqlicon.'</a>';

    quantity_item ('pre_comment');
//     href_quantity_item ('X_discharge_command','Production/Parameters/FullCommandLine');
    echo  "<tr class='field'><th class='field-name' style='white-space: nowrap;'>";
    echo "<a href='Production/Parameters/FullCommandLine'>";
    psql_physical_quantities_request ('description','X_discharge_command');
    echo "</a>";
    echo "<a href='Operation/Discharge/Styles/index.html' title='Command line help'>$helpicon_small</a>";
    echo "<a href='Production/Parameters' title='Command line structure'>$diricon_small</a>";
    echo "</th><td class='field-body' style='word-break: break-all; white-space: normal;'>";
    echo substr(bare_psql_shots_request ('X_discharge_command'),1,180);
    echo " ... ";
    echo "<i class='fa fa-copy' onclick='copyToClipboard()' style='font-size:20px'></i>".'</td></tr>';


?>
</tbody>
</table>

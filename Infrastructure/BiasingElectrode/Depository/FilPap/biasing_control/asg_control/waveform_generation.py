"""
This script provides the functionality to generate waveform for
an SCPI controlled arbitrary signal generator.
"""

import numpy as np
from utils import get_buff_duration, time_to_index


def check_voltage_range(waveform: np.ndarray):
    """ This function controls that the waveform send to
    the ASG (RedPitaya) does not exceed the ± 10 V range.
    When a value in waveform is above 1 it is replaced
    with 1.
    When a value in waveform is below -1 it is replaced
    with -1."""

    # the upper limit of 1 is checked here
    waveform_upper = np.where(waveform < 1, waveform, 1)
    # the lower limit of -1 is checked here
    waveform_checked = np.where(waveform_upper > -1, waveform_upper, -1)

    return waveform_checked


def return_harmonic_signal(t, amplitude=1, frequency=20):
    """
    Creates a harmonic signal i.e. evaluates function sin for
    given time range.

    :param np.ndarray t: time range
    :param float amplitude: amplitude of the harmonic signal
    :param float frequency: frequency of the harmonic signal, in kHz
    :return:
    """
    return amplitude * np.sin(2*np.pi*frequency*t)


def generate_waveform(harmonic_start,
                      harmonic_stop,
                      harmonic_amplitude,
                      harmonic_offset,
                      harmonic_freq,
                      buff_len=16384,
                      buff_freq=50):

    waveform = np.zeros(buff_len)

    start_index = time_to_index(harmonic_start, buff_freq=buff_freq)
    stop_index = time_to_index(harmonic_stop, buff_freq=buff_freq)

    index_diff = stop_index - start_index

    t = create_time_range(harmonic_start,
                          harmonic_stop,
                          index_diff,
                          buff_freq=buff_freq)

    harmonic_part = return_harmonic_signal(t,
                                           frequency=harmonic_freq,
                                           amplitude=harmonic_amplitude)

    waveform[start_index:stop_index] = harmonic_part + harmonic_offset

    waveform = check_voltage_range(waveform=waveform)

    waveform = np.round(waveform, decimals=1)

    return waveform


def create_time_range(start: float = None,
                      stop: float = None,
                      sample_count: int = None,
                      buff_freq: int = 50):
    """
    Creates the time axis for the given buff_frequency.

    :param float start:
    :param float stop:
    :param int sample_count:
    :param int buff_freq:
    :return:
    """
    if start is None:
        start = 0
    if stop is None:
        stop = get_buff_duration(buff_freq=buff_freq)
    if sample_count is None:
        sample_count = 16384

    return np.linspace(start, stop, sample_count)

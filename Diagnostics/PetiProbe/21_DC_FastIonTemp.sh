#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh


diag_id=PetiProbe
setup_id="21_DC_FastIonTemp"
whoami="Diagnostics/$diag_id/$setup_id"

DAS="Oscilloscopes/TektrMSO64-a/21_DC_FastIonTemp"



#pwd
source Universals.sh

#With FG:
DeviceList="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs FunctionGenerators/RigolDG1032Z-a/FG4ElstatProbes $DAS"



LastChannelToAcq=2
diags=('U_bias' 'U_current')


function GetReadyTheDischarge ()
{

   GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh
}


function PostDischargeAnalysis() 
{

    GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    
    
    tail -n +10 `ls -d /home/golem/tektronix_drop/*|grep TektrMSO58_ch7` > U_LP_fl.csv
    tail -n +10 `ls -d /home/golem/tektronix_drop/*|grep TektrMSO58_ch6` > U_BPP_fl.csv

    GenerateDiagWWWs $diag_id $setup_id `dirname $DAS` # @Commons.sh
    Analysis
}

function Analysis()
{

local nb_id="Golem_analysis-CUTOFF"
    #sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" $nb_id.ipynb
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat ../../shot_no`/g" $nb_id.ipynb
    jupyter-nbconvert --allow-errors --execute $nb_id.ipynb --to html --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    convert -resize $icon_size icon-fig.png graph.png
}

function Kick-off_Analysis
{
    gnuplot  -e  "set terminal png size 1600, 1000;unset xtics;set datafile separator ',';set bmargin 0;set tmargin 1;set lmargin 20;set rmargin 3;unset xlabel;set multiplot layout 5, 1 title 'GOLEM Shot #$SHOT_NO, f_fg=`cat $SHM0/Diagnostics/PetiProbe/Parameters/f_fg` Hz, u_fg=`cat $SHM0/Diagnostics/PetiProbe/Parameters/u_fg` V';set xrange [*:*];set yrange [-5:20];set style data dots;set ylabel 'U_loop [V]';plot 'U_loop-b.csv' u 1:2 t '' w l lc 1 ;set bmargin 0;set tmargin 0;unset title;set xtics;set yrange [*:*];set ylabel 'U_bias [V]';plot 'U_bias.csv'  u 1:2  w l lc 2 title '';unset xtics;set ylabel 'U_res [V]';plot 'U_current.csv'  u 1:2  w l lc 3 title '';set xtics; set ylabel 'U_current/R_i [I]';set xlabel 'U_bias [V]';plot '< paste -d "," U_bias.csv U_current.csv' u 2:(\$4)/47  w l lc 4 title 'R_i=47 Ohm'" > icon-fig.png
    
    echo "<h1>Kick-off analysis for fast ion temp meas.</h1>
    <img src="icon-fig.png" width="80%"></img><br></br>"> analysis.html

    convert -resize $icon_size icon-fig.png graph.png
}




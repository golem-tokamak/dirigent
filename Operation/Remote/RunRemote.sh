#!/bin/bash

# Define the temporary files for communication
control_file="/tmp/golemRemoteCommand"

SESSION_NAME="golem_remote_session"

exit_on_tmux_detach() {
    if [[ $(tmux list-sessions | grep  "$SESSION_NAME" | grep -c attached) -eq 0 ]]; then
        echo "Tmux is detached exiting"
        tmux kill-session -t "$SESSION_NAME"
        exit
    fi
}

while [ 1 ]; do

    if [[ -f "$control_file" ]]; then
        action=$(cat "$control_file")
    else
        echo "Missing Command File -- Pause"
        action="pause"
    fi

    if [ "$action" == "exit" ];then
        echo "Exiting"
        sleep 2;
        break
    fi

    if [ "$action" == "pause" ]; then
        echo "Execution paused"; sleep 2s; continue;
    fi

    export PGPASSWORD=`cat /golem/production/psql_password`; 
    if [ `psql -c "SELECT status FROM remote.shots AS shots WHERE status = '0' ORDER BY status ASC LIMIT 1" -qAt -U golem golem_database|wc -l` != 0 ]; 
    then

        if [ "$action" == "pause" ]; then
            echo "Execution paused"; sleep 2s; continue;
        elif [ "$action" == "runOne" ]; then
            echo "pause" > "$control_file"
            action="run"
            echo "Execute one shot"
        fi

        if [ "$action" == "run" ]; then  
            echo "########################################"
            echo "###  / ____|| |  | | / __ \|__   __| ###"
            echo "### | (___  | |__| || |  | |  | |    ###"
            echo "###  \___ \ |  __  || |  | |  | |    ###"
            echo "###  ____) || |  | || |__| |  | |    ###"
            echo "### |_____/ |_|  |_| \____/   |_|    ###"
            echo "########################################"
            #DB management
            export PGPASSWORD=`cat /golem/production/psql_password`; vshotno=`psql -c "SELECT vshotno FROM remote.shots AS shots WHERE status = '0' ORDER BY date ASC LIMIT 1" -qAt -U golem golem_database`
            export PGPASSWORD=`cat /golem/production/psql_password`;psql -c "UPDATE remote.shots SET status = '1' WHERE vshotno = '"$vshotno"';" -qAt -U golem golem_database
            #SHOT command
            export PGPASSWORD=`cat /golem/production/psql_password`;psql -c "SELECT extra_params FROM remote.shots AS shots WHERE status = '1' ORDER BY status ASC LIMIT 1;" -qAt -U golem golem_database |awk -F "|" '{printf "./Dirigent.sh \
--discharge \
--operation.discharge \"style='\''remote'\'',voice='\''on'\'',analysis='\''on'\''\" \
%s \n",  $1}' |xargs bash
            #DB management
            export PGPASSWORD=`cat /golem/production/psql_password`;psql -c "UPDATE remote.shots SET shotno = `cat /dev/shm/golem/shot_no` WHERE vshotno = '"$vshotno"' " -qAt -U golem golem_database
            export PGPASSWORD=`cat /golem/production/psql_password`;psql -c "UPDATE remote.shots SET status = '2' WHERE vshotno = '"$vshotno"' " -qAt -U golem golem_database
            echo "Chance to stop it, sleeping";sleep 2s;\
            echo "Chance to stop it, sleeping";sleep 2s;\
    fi
else 
		echo "Waiting for discharge in the queue";sleep 2s;
fi;

## check if tmux is atteched if not exit
exit_on_tmux_detach
done

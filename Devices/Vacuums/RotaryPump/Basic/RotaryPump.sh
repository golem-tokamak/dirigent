#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh


function PumpingON
{
    LogIt "engaging rotary pump"
    Call $RelayBoards RotPump@Vacuum  ON
}

function PumpingOFF
{
    LogIt "disengaging rotary pump"
    Call $RelayBoards RotPump@Vacuum  OFF
}

#Call /golem/Dirigent/Devices/Vacuums/RotaryPump/Basic PumpingON


source /golem/Dirigent/Commons.sh

#WaitForFileReady -file /golem/shm_golem/ActualSession/SessionLogBook/GlobalLogbook -timeout 120 -timestep 10

#return
xterm -sb -bg black -fg white  -geometry 200x50+0+0 -title "tG: Cold start " -e "
ssh -Y golem@golem 'export TERM=vt100;cd /golem/Dirigent/;./Dirigent.sh --session SetupSetup /golem/Dirigent/Setups/ChamberConditioning.setup'"



ssh -Y golem@golem 'xterm -hold -geometry 200x50+0+0 -fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb  -fg yellow -bg blue -title "Open Session@GOLEM tokamak" -e "cd /golem/Dirigent/;./Dirigent.sh --session open  2>&1 |tee >(ansi2txt  > /dev/shm/golem/SessionStdErrout.log)"' &
sleep 5
#> >(tee >(ansi2txt  > /dev/shm/golem/SessionStdout.log)) 2> >(tee -a /dev/shm/golem/SessionStderr.log >&2)

WaitForFileReady -file /golem/shm_golem/ActualSession/SessionLogBook/GlobalLogbook -timeout 240 -timestep 10

#ssh -Y golem@golem 'xterm -hold -geometry 200x50+0+0 -fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb  -fg yellow -bg blue -title "Session log@GOLEM tokamak" -e "tail -n -0 -F /dev/shm/golem/ActualSession/SessionLogBook/GlobalLogbook"' &

ssh -Y golem@golem 'xterm -hold -geometry 200x50+0+0 -fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb  -fg yellow -bg blue -title "Session log@GOLEM tokamak" -e "bash /golem/Dirigent/Operation/Session/Setup/Tmux.sh"' &


if ! ping -c 1 -n -w 1 Chamber.golem &> /dev/null; 
then 
    echo "Waiting for chamber RASP\n"
    while ! ping -c 1 -n -w 1 Chamber.golem &> /dev/null
    do
        printf "%c" "."
    done
fi

WaitForFileReady -file /golem/shm_golem/ChamberLog -timeout 30 -timestep 1

#ChamberLog (feed gnuplot)
bash /golem/Dirigent/Operation/Session/Setup/ChamberLog.sh
sleep 5

ssh -Y golem@golem 'cd /golem/Dirigent/;./Dirigent.sh -ch pon'

read
#Do the windows stuff:


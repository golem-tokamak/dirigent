/** @file Settings.cpp 
*   @brief Functions used to get desired settings of a camera.
*   @note The required values are specified in the CamsConfig.json file.
*/

#include "FastCamGOLEM.h"

//------------ LIVE/MEMORY PLAYBACK mode -------------
unsigned long GetStatus(camera& cam) { 
    /** @todo this function should also work for another status; zkontrolovat zda je spravne predavani skrz referenci */
    unsigned long nRet;
    unsigned long nErrorCode;
    unsigned long status;

    nRet = PDC_GetStatus(cam.dev, &status, &nErrorCode);
    checkStatus("PDC_GetStatus", nRet, nErrorCode);
    return status;

}
void SetStatus(vector<camera>& cams, unsigned long DesiredStatus) {
    unsigned long nRet;
    unsigned long nErrorCode;
    unsigned long ActualStatus;

    for (int i = 0; i < cams.size(); i++) {
        cout << "Getting current status..." << endl;
        ActualStatus = GetStatus(cams[i]);
        if (ActualStatus != DesiredStatus) {
            nRet = PDC_SetStatus(cams[i].dev, DesiredStatus, &nErrorCode);
            checkStatus("PDC_SetStatus", nRet, nErrorCode);
            cout << "On " << cams[i].name << " was set new status." << endl;
        }
    }
    return;
}


//---------------- Variable Channel ----------------------------
void SetVariableChannel(vector<camera>& cams) {
    unsigned long nRet;
    unsigned long nErrorCode;
    unsigned long varChan, currVarChan, Rate, Width, Height, Xpos, Ypos;

    for (int i = 0; i < cams.size(); i++) {
        varChan = cams[i].variableChannel;
        nRet = PDC_GetVariableChannel(cams[i].dev, cams[i].child, &currVarChan, &nErrorCode);
        checkStatus("PDC_GetVariableChannel", nRet, nErrorCode);
        cout << "Variable channel " << currVarChan << " is currently set on " << cams[i].name << endl;

        //Get some information about the current channel:
        nRet = PDC_GetVariableChannelInfo(cams[i].dev, varChan, &Rate, &Width, &Height, &Xpos, &Ypos, &nErrorCode);
        checkStatus("PDC_GetVariableChannelInfo", nRet, nErrorCode);
        cout << "Information about the current channel (" << varChan << "):\n\t" << "Recording speed (Rate): " << Rate << "\n\tWidth and Height: " << Width << " and " << Height << endl;

        if (currVarChan != varChan) {
            nRet = PDC_SetVariableChannel(cams[i].dev, cams[i].child, varChan, &nErrorCode);
            checkStatus("PDC_SetVariableChannel", nRet, nErrorCode);
            cout << "Variable channel " << varChan << " was set on " << cams[i].name << endl;

            //Get some information about the currently set channel:
            nRet = PDC_GetVariableChannelInfo(cams[i].dev, varChan, &Rate, &Width, &Height, &Xpos, &Ypos, &nErrorCode);
            checkStatus("PDC_GetVariableChannelInfo", nRet, nErrorCode);
            cout << "Information about channel " << varChan << ":\n\t" << "Recording speed (Rate): " << Rate << "\n\tWidth and Height: " << Width << " and " << Height << endl;

        }

    }
    return;
}

void CreateSetVariableChannel(vector<camera>& cams) {
    /** @todo still not tested for enough values*/
    unsigned long nRet;
    unsigned long nErrorCode;
    unsigned long varChan, currVarChan, Rate, Width, Height, Xpos, Ypos, shutterSpeed, Size;
    unsigned long WidthMax, HeightMax, WidthMin, HeightMin, WidthStep, HeightStep, XPosStep, YPosStep, FreePos, MaxRate;

    varChan = 3;
    for (int i = 0; i < cams.size(); i++) {
        cams[i].variableChannel = varChan;
        nRet = PDC_EraseVariableChannel(cams[i].dev, cams[i].variableChannel, &nErrorCode);
        checkStatus("PDC_EraseVariableChannel", nRet, nErrorCode);
        
        //Get all restrictions and if nececessary change required values
        nRet = PDC_GetMaxResolution(cams[i].dev, cams[i].child, &WidthMax, &HeightMax, &nErrorCode);
		checkStatus("PDC_GetMaxResolution", nRet, nErrorCode);
        if (cams[i].height > HeightMax) { cams[i].height = HeightMax; cout << "Height set to max possible height: " << HeightMax << endl; }
        
        nRet = PDC_GetVariableRestriction(cams[i].dev, &WidthStep, &HeightStep, &XPosStep, &YPosStep, &WidthMin, &HeightMin, &FreePos, &nErrorCode);
        checkStatus("PDC_GetVariableRestriction", nRet, nErrorCode);
        if (cams[i].height % HeightStep != 0) { cams[i].height = cams[i].height - (cams[i].height % HeightStep); cout << "Height set to: " << cams[i].height << endl; }
        if (cams[i].height < HeightMin) { cams[i].height = HeightMin; cout << "Height was too small, so it was increased to a minimum: " << HeightMin << endl; }
        if (cams[i].width % WidthStep != 0) { cams[i].width = cams[i].width - (cams[i].width % WidthStep); cout << "Width set to: " << cams[i].width << endl; }
        if (cams[i].width < WidthMin) { cams[i].width = WidthMin; cout << "Width was too small, so it was increased to a minimum: " << WidthMin << endl; }
        
        nRet = PDC_GetVariableMaxRate(cams[i].dev, cams[i].width, cams[i].height, &MaxRate, &nErrorCode);
		checkStatus("PDC_GetVariableMaxRate", nRet, nErrorCode);
        if (cams[i].recordRate > MaxRate) { cams[i].recordRate = MaxRate; }

        //Create new channel 
        Xpos = 0; Ypos = 0; /** @todo check what it does*/
        nRet = PDC_SetVariableChannelInfo(cams[i].dev, varChan, cams[i].recordRate, cams[i].width, cams[i].height, Xpos, Ypos, &nErrorCode);
        checkStatus("PDC_SetVariableChannelInfo", nRet, nErrorCode);

        //Chcek if required VarChan was created
        nRet = PDC_GetVariableChannelInfo(cams[i].dev, varChan, &Rate, &Width, &Height, &Xpos, &Ypos, &nErrorCode);
        checkStatus("PDC_GetVariableChannelInfo", nRet, nErrorCode);
        cout << "Information about the current channel (" << varChan << "):\n\t" << "Recording speed (Rate): " << Rate << "\n\tWidth and Height: " << Width << " and " << Height << endl;


        //Set the new variable channel 
        nRet = PDC_SetVariableChannelInfo(cams[i].dev, 1, cams[i].recordRate, cams[i].width, cams[i].height, Xpos, Ypos, &nErrorCode);
		checkStatus("PDC_SetVariableChannelInfo", nRet, nErrorCode);
		nRet = PDC_SetVariableChannel(cams[i].dev, cams[i].child, 1, &nErrorCode);
		checkStatus("PDC_SetVariableChannel", nRet, nErrorCode);

        //Get some information about the currently set channel:
        nRet = PDC_GetVariableChannelInfo(cams[i].dev, varChan, &Rate, &Width, &Height, &Xpos, &Ypos, &nErrorCode);
        checkStatus("PDC_GetVariableChannelInfo", nRet, nErrorCode);
        cout << "Information about channel " << varChan << ":\n\t" << "Recording speed (Rate): " << Rate << "\n\tWidth and Height: " << Width << " and " << Height << endl;

        
        // set Shutter speed
        /** @todo is it in Hz, what is the proper required value*/
        /** @todo test it*/
        unsigned long list[PDC_MAX_LIST_NUMBER];
        unsigned long shutterVal;
		nRet = PDC_GetShutterSpeedFpsList(cams[i].dev, cams[i].child, &Size, list, &nErrorCode);
		checkStatus("PDC_GetShutterSpeedFpsList", nRet, nErrorCode);
		shutterVal = getClosestValue(cams[i].shutterSpeed, list);
        if (shutterVal != cams[i].shutterSpeed) { cout << "Shutter speed: " << shutterVal << endl; }
		 
		nRet = PDC_SetShutterSpeedFps(cams[i].dev, cams[i].child, cams[i].shutterSpeed, &nErrorCode);
		checkStatus("PDC_SetShutterSpeedFps", nRet, nErrorCode);
		nRet = PDC_GetShutterSpeedFps(cams[i].dev, cams[i].child, &shutterSpeed, &nErrorCode);
		checkStatus("PDC_GetShutterSpeedFps", nRet, nErrorCode);
		if (shutterSpeed != cams[i].shutterSpeed) { cout << "ShutterSpeed " << shutterSpeed << " is set instead of intended " << cams[i].shutterSpeed << endl; cams[i].shutterSpeed = shutterSpeed; }

    }

    return;
}



//----------------- External I/O signal -----------------------
void SetExternal_IO_signal(vector<camera>& cams) {
    unsigned long nRet;
    unsigned long nErrorCode;
    unsigned long ExtInPortNo; //input terminal port number


    for (int i = 0; i < cams.size(); i++) {
        ExtInPortNo = 1;
        nRet = PDC_SetExternalInMode(cams[i].dev, ExtInPortNo, cams[i].input1.second, &nErrorCode);
        checkStatus("PDC_SetExternalInMode", nRet, nErrorCode);
        cout << "Input " << ExtInPortNo << "was set to " << cams[i].input1.first << " on " << cams[i].name << endl;

        ExtInPortNo = 2;
        nRet = PDC_SetExternalInMode(cams[i].dev, ExtInPortNo, cams[i].input2.second, &nErrorCode);
        checkStatus("PDC_SetExternalInMode", nRet, nErrorCode);
        cout << "Input " << ExtInPortNo << "was set to " << cams[i].input2.first << " on " << cams[i].name << endl;

        //TODO:
        //There is option to set othersync and synchronizes camera with another one (?).....
        //It was set differently in PFV program

        ExtInPortNo = 3;
        nRet = PDC_SetExternalInMode(cams[i].dev, ExtInPortNo, PDC_EXT_IN_OTHERSSYNC, &nErrorCode);
        checkStatus("PDC_SetExternalInMode", nRet, nErrorCode);
        cout << "Input " << ExtInPortNo << "was set to PDC_EXT_IN_OTHERSYNC" << " on " << cams[i].name << endl;

    }
    return;
}


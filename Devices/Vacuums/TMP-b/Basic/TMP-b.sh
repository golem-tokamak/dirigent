#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh
PUMP_id=b


function PumpingON
{
    LogIt "engaging TMP-$PUMP_id pump"
    Call $RelayBoards TMP-"$PUMP_id"@Vacuum  ON
}

function PumpingOFF
{
    LogIt "disengaging TMP-$PUMP_id pump"
    Call $RelayBoards TMP-"$PUMP_id"@Vacuum  OFF
}

function StandbyON
{
    LogIt "Stand by TMP-$PUMP_id pump"
    Call $RelayBoards TMP-"$PUMP_id"Standby@Vacuum  ON
}

function StandbyOFF
{
    LogIt "Stand by TMP-$PUMP_id pump OFF"
    Call $RelayBoards TMP-"$PUMP_id"Standby@Vacuum  OFF
}

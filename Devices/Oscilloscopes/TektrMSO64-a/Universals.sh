#!/bin/bash


COMMAND="netcat -q 1 $ThisDev.golem 4000"
QUERYCOMMAND="netcat -w 1 $ThisDev.golem 4000"
Drivers="Oscilloscopes/Drivers/TektrMSO5/driver"


function Wake-upCall(){ 
DelayCall $RelayBoards TektrMSO64-a@Galvanics ON
 } 

function Sleep-downCall(){ 
DelayCall $RelayBoards TektrMSO64-a@Galvanics OFF
}


function ExternDataAvailabilityTest()
{
    
    $LogFunctionGoingThrough
    Arming
    Relax
    ForceTrig
    Relax
#    ls $Tek_mount_path
    if ! ls $Tek_mount_path/TektrMSO64*.* > /dev/null 2>&1; then 
    critical_error "$ThisDev mount problem ... 192.168.2.116, tek_drop, golem, tokamak";fi
}


function Arming()
{
    LogTheDeviceAction

    rm -f /home/golem/tektronix_drop/TektrMSO64*.csv
    rm -f /home/golem/tektronix_drop/TektrMSO64*.png
    #mkdir -p $SHM0/$SUBDIR/$ThisDev/
    echo ":DISplay:GLObal:CH1:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH2:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH3:STATE ON"|$COMMAND
    echo ":DISplay:GLObal:CH4:STATE ON"|$COMMAND
    #echo ":DISplay:GLObal:CH5:STATE ON"|$COMMAND
    #echo ":DISplay:GLObal:CH6:STATE ON"|$COMMAND
    SingleSeq
}


function SingleSeq()
{
    echo "FPANEL:PRESS SINGLESEQ"|$COMMAND
}   


function ForceTrig()
{
    echo "FPANEL:PRESS FORCetrig"|$COMMAND
}


function AdjustHorizontalScale() 
{
    local recordMS=$1
    samplerate=`echo "HORizontal:MODE:SAMPLERate?" | $QUERYCOMMAND`
    if [[ "$samplerate" != "" ]]; then
        recordLength=`python3 -c "s=float('$samplerate');r=int('$recordMS');print(int(s*r/1000))"`
        echo "HORizontal:RECOrdlength $recordLength" | $COMMAND
        echo "Setting RECOrdlength $recordLength @ $ThisDev"
    else
        echo "AdjustHorizontalScale @ $ThisDev FAILED"
    fi   
}


# MOUNT: 192.168.2.116, tek_drop, golem, tokamak
# systemctl restart smbd
# Dat pozor, na ktery disk se mapuje .. O ci L

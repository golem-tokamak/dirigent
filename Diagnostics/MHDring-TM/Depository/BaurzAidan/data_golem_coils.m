function [ data ] = data_golem_coils(shotNums)
s = 1;

for shotNum = shotNums
    
    adress_mirnov = ['http://golem.fjfi.cvut.cz/shots/' num2str(shotNum) '/Diagnostics/MHDring_TM/'];
    adress_basic = ['http://golem.fjfi.cvut.cz/shots/' num2str(shotNum) '/Diagnostics/BasicDiagnostics/Results/'];
    adress1 = ['http://golem.fjfi.cvut.cz/shots/' num2str(shotNum) '/Devices/RASPs/Charger/'];
    
    
    %% Basic diagnostics
    x = table2array(webread([adress_basic 'Bt.csv']));
    data(s).Bt = x(:,2);
    x = table2array(webread([adress_basic 'Ich.csv']));
    data(s).Ich =  x(:,2);

    x = table2array(webread([adress_basic 'U_loop.csv']));
    data(s).U_loop =  x(:,2);
    %
    data(s).t_basic = x(:,1);
    
    b_plasma = str2double(urlread([adress_basic 'b_plasma']));
    if b_plasma > 0
        data(s).t_plasma_duration = str2double(urlread([adress_basic 't_plasma_duration']));
        data(s).t_plasma_end = str2double(urlread([adress_basic 't_plasma_end']));
        data(s).t_plasma_start = str2double(urlread([adress_basic 't_plasma_start']));
        x = table2array(webread([adress_basic 'Ip.csv']));
        data(s).Ip =  x(:,2);
    end
    
    
    %% Coils
    for i = 1:16
        x = table2array(webread([adress_mirnov 'ring_' num2str(i) '.csv']));
        data(s).coils(:,i) = x(:,2);
    end
    data(s).t_coils = x(:,1);
  %  disp(s);
    
   s = s + 1; 
end

end








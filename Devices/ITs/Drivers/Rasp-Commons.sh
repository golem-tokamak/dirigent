# RASPs issues: (to be executed at RASPs)
# **********************************************************

source Commons.sh


function MountCentralSHMEnvironment()
{

    mkdir -p $SHM
    sshfs golem@$DirigentServer:$SHM/ $SHM/ 
    df|grep golem
}   

function CallCentralFunction
{
local where=$1
local what=$2
local param1=$3
local param2=$4
local param3=$5
local param4=$6
local param5=$7
local param6=$8
local param7=$9 #lepe

#local script=`dirname $where|xargs basename`


    ssh $GM "export TERM=vt100;cd $where/;source `basename $where`.sh;$what $param1 $param2 $param3 $param4 $param5 $param6 $param7"
}



import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="vacuum-control",
    version="0.0.1",
    author="Ondrej Grover",
    author_email="ondrej.grover@gmail.com",
    description="GOLEM vacuum pumping RaspberyPi controller API",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.fjfi.cvut.cz/golem/vacuum-control",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 2",
    ],
    install_requires=['RPi.GPIO'],
)

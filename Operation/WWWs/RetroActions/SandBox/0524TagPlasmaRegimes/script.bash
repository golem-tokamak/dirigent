FROM=44860
TO=44871
soubor=index.html;
#soubor=index.php;
url=https://golem.fjfi.cvut.cz/shots/
t_plasma_duration_limit=7
title=''
SHOTS='/golem/shots'
export PGPASSWORD=`cat /golem/production/psql_password`
PSQL='psql -d golem_database -U golem -t -c'

echo "<html><body>
<h1>$title</h1>
<h2>From #$FROM to #$TO</h2>
<table>" > $soubor;
echo "<tr><th>ShotNo</th><th>Basic diagnostics</th><th>Tags</th></tr>" >> $soubor;
Counter=0
for i in `seq $TO -1 $FROM `; do echo -ne '*'$i:;
if [[ -e "$SHOTS/$i/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg" ]] \
; then
    let Counter++ #increment
    echo -ne yes';'
    echo "<tr>
    <td><a href=$url/$i>$i</a><br>$(<$SHOTS/$i/shot_date)<br>$(<$SHOTS/$i/shot_time)</td><td>
    ">> $soubor;
    $PSQL "SELECT  '<a href=/DataBase/InvertTag.php?shot_no=$i&table=diagnostics.basicdiagnostics&tag=tag_dbl_bd>'||tag_dbl_bd||'</a>' FROM diagnostics.basicdiagnostics WHERE shot_no=$i">> $soubor;
    echo "</td><td><a href=/DataBase/InvertTag.php?shot_no=$i&table=diagnostics.basicdiagnostics&tag=tag_dbl_bd>Invert dbl</a></td><td><img src=$url/$i/Devices/Oscilloscopes/TektrMSO56-a/rawdata.jpg></td>
    </tr>" >> $soubor;
else echo -ne no';';fi;done
echo "</table>Alltogether $Counter discharges<hr/>
<h2>For generation bash script used</h2>
<pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;

#UNDER CONSTRUCTION, COMMAND LINE SNB:
#psql -c "SELECT  dsch.shot_no, bdg.t_plasma_duration FROM diagnostics.basicdiagnostics bdg,operation.discharges dsch WHERE dsch.shot_no=44200" -qAt -U  golem golem_database
#UPDATE table SET boolean_field = NOT boolean_field WHERE id = :id




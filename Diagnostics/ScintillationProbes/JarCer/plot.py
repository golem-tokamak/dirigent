import matplotlib.pyplot as plt
import numpy as np 
import requests
import tekwfm

def readwfm(path):
    volts, tstart, tscale, tfrac, tdatefrac, tdate = tekwfm.read_wfm(path)
    toff = tfrac * tscale
    samples, frames = volts.shape
    tstop = samples * tscale + tstart
    volts_osc = volts.reshape(len(volts))
    time_osc = np.linspace(tstart+toff, tstop+toff, num=samples, endpoint=False)
    
    return time_osc,volts_osc


shot_no = 0


#response = requests.get(f'http://golem.fjfi.cvut.cz/shots/0/Devices/Oscilloscopes/TektrMSO58-a/ScintillationProbesHiRes2/ch4.wfm')

time, data = readwfm('/golem/shots/41913/Devices/Oscilloscopes/TektrMSO58-a/ScintillationProbesHiRes2/ch4.wfm')
plt.figure()
plt.plot(time, data)
plt.savefig('test',dpi=1000)
#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source Universals.sh

DAS="Oscilloscopes/TektrMSO56-a/BasicDiagnostics"
DeviceList="$DAS"

flukeIP=192.168.2.249
flukePORT=10001

# Scope file set-up: TIME,CH1,CH2,CH3,CH4,,TIME,MATH1,,TIME,MATH2
#columns="2 3 4 5 8 11" # Tek MSO64
columns="2 3 4 5 10 13" # Tek MSO56

diags=('U_Loop' 'U_BtCoil' 'U_RogCoil' 'U_LeybPhot' 'U_IntBtCoil' 'U_IntRogCoil')

notebook=StandardDAS.ipynb



function GetReadyTheDischarge ()
{
    
    GeneralTableUpdateAtDischargeBeginning
    StartFlukeItegralRead
}


function PostDischargeAnalysis()
{
    #ls -all
    #GeneralDAScommunication $DAS RawDataAcquiring #TOHLE SE DELA v DETECT PLASMA!
    ln -s ../../Devices/`dirname $DAS` DAS_raw_data_dir
    #echo  ../../../Devices/$DAS DAS_raw_data_dir
    
    #n=0;for i in $columns; do tail -q -n +13  DAS_raw_data_dir/TektrMSO56_ALL.csv |awk -F "," '{print $1","'\$$i'}'> ${diags[$n]}.csv;((n+=1)); done;
  
    export SHOT_NO=`cat ../../shot_no` # for linux
    mkdir -p Results
    
    Analysis

    ReadFlukeIntegralValue

    
    #GenerateDiagWWWs $diag_id $setup_id $DAS Diagnostics/Basic Sx48Muj5rcQZVGTLA
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox
# @Commons.sh
    #GenerateWWWs
}

function Analysis
{
    
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" $notebook

    jupyter-nbconvert --execute $notebook \
                      --to html \
                      --output analysis.html \
                        > >(tee -a jup-nb_stdout.log) \
                       2> >(tee -a jup-nb_stderr.log >&2)

    convert -resize $icon_size icon-fig.png graph.png
}

function StartFlukeItegralRead() {
    # comunication 1200 baud, 7 bit no parity, 1 stop bit needs to be set in GNOME232
    
    # opens pseudo file for tcp communication (bash magic)
    exec 3<>/dev/tcp/$flukeIP/$flukePORT
    cat <&3 > flukeOutput.txt &
    catPID=$!
    echo -ne "C" >&3

    TIMEOUT=5
    INTERVAL=1

    for ((i=0; i<$TIMEOUT; i+=$INTERVAL)); do
        if grep -q "CMD?" "flukeOutput.txt"; then
            echo -ne "T" >&3    
            break;
        fi
        sleep $INTERVAL
    done

    kill -9 $catPID

    # close file descriptor
    exec 3>&-
}


function ReadFlukeIntegralValue() {
    # opens pseudo file for tcp communication (bash magic)
    exec 3<>/dev/tcp/$flukeIP/$flukePORT
    cat <&3 > flukeOutput.txt &
    catPID=$!

    # break from measeurment mode
    for i in {1..20}
    do
        echo -ne " " >&3
        sleep 0.2
        if grep -q "CMD?" "flukeOutput.txt"; then
            break;
        fi
    done


    # wait for "CMD?" prompt
    TIMEOUT=5
    INTERVAL=1
    for ((i=0; i<$TIMEOUT; i+=$INTERVAL)); do
        if grep -q "CMD?" "flukeOutput.txt"; then
            break;
        fi
        sleep $INTERVAL
    done


    # get intagral value
    echo -ne "R" >&3
    sleep 3

    # reset fluke
    echo -ne "Q" >&3


    kill $catPID
    # close file descriptor
    exec 3>&-
    

    flukeVal=`awk '/[0-9]+\.[0-9]+\s+MICRO Sieverts integrated/{print $1*100}' flukeOutput.txt`
    echo $flukeVal > Results/d_fluke

    UpdateDataBaseQuantAtCurrentShot operation.discharges D_integral_dose $flukeVal

#    rm flukeOutput.txt

}

# Tuning:

#cd "/golem/shm_golem/ActualShot/Diagnostics/BasicDiagnostics/"; cp /golem/svoboda/Dirigent/Diagnostics/BasicDiagnostics/StandardDAS.* .;source StandardDAS.sh ;PostDischargeAnalysis

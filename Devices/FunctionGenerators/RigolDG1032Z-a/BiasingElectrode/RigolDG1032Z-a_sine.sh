#!/bin/bash
BasePath=../../..;source $BasePath/Commons.sh
source ../Universals.sh

COMMAND="netcat -w 1 $ThisDev 5555"
address=192.168.2.171
port=5555



function GetReadyTheDischarge (){
  mkdir Parameters
  GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh

  WaitForFileReady -file $SHM0/Infrastructure/BiasingElectrode/Parameters/waveform -timeout 10 -step 1

  raw=`cat $SHM0/Infrastructure/BiasingElectrode/Parameters/waveform`
  # creates a sequence from the string
  raw=($(echo $raw | tr -s ',' ' '))
  frequency="${raw[0]}"
  amplitude="${raw[1]}"
  offset="${raw[2]}"


  echo "Frequency: $frequency Hz"
  echo "Amplitude: $amplitude Vpp"
  echo "   Offset: $offset Vdc"

  # this sets the option for send_scpi function
  # to use dry_run set it to true
  dry_run=false

  send_scpi ":OUTP2 OFF"  # Turn off output off ch2
  sleep 0.1;
  echo "Waveform pars"
  send_scpi ":SOUR2:APPL:SIN $frequency,$amplitude,$offset,0" $dry_run
  echo "Burst mode"
  send_scpi ":SOUR2:BURS ON"

  #Set trigger to manual/external and number of bursts
  send_scpi ":SOUR2:BURS:MODE:TRIG;:SOUR2:BURS:TRIG:SOUR EXT;:SOUR2:BURS:NCYC 150 "
  sleep 0.2;
}


function send_scpi(){
  # the if the second arg is true then there is dry run
  if [ $dry_run = "true" ];then
    echo $1
  else 
    echo $1|netcat -w 1 $address $port 
  fi
}

function Arming(){
  echo "And output"
  send_scpi ":OUTP2 ON"  #  Turn on output of 
  time_ini=$(date +%s)
}
   
   
function PostDischargeAnalysis() {
  send_scpi ":OUTP2 OFF" # Turn off output off ch2
  time_end=$(date +%s)
  elapsed_time=$((time_end-time_ini))
  echo " ===================  Elapsed time was $elapsed_time ===================="
}      
      

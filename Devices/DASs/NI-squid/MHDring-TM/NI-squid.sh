#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh

source Universals.sh

function Arming()
{
    ssh golem@ni-squid.golem -f "python3.11 read_daq.py --channels '/PCI-6133-a/ai0:7' '/PCI-6133-d/ai0:7' '/PCI-6133-c/ai0:2'  --nsamples 40000 --samplerate 1000000 -o rawData.csv" >StandardLog 2>ErrorLog
}

function RawDataAcquiring()
{
    scp golem@ni-squid.golem:rawData.csv .
    ssh golem@ni-squid.golem "rm rawData.csv"

    #for i in `seq 2 20`; do tail -n +2 rawData.csv|awk -F ',' '{printf "%5.4g,%5.3f\n", $1, '\$$i'}' > ch$((i-1)).csv;done

    echo  "set terminal png size 1600, 1000;set output 'ScreenShotAll.png';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;unset xtics;unset xlabel;set multiplot layout 10,2 columnsfirst title 'GOLEM Shot $shot_no';set xrange [*:*];set yrange [*:*];set style data dots;unset ytics; set datafile separator comma"> tmp;
    # Bacha, karty jsou zrejme prehozene!
    #for i in `seq 2 17`; do echo "set ylabel '$((i-1))';plot 'rawData.csv' u 1:$((i)) t '' w l lc 1;" >> tmp;done
    # Takze nutno kreslit takhle
    for i in `seq 2 9`; do echo "set ylabel '$((i-1))';plot 'rawData.csv' u 1:$((i+8)) t '' w l lc 1;" >> tmp;done
    for i in `seq 10 17`; do echo "set ylabel '$((i-1))';plot 'rawData.csv' u 1:$((i-8)) t '' w l lc 1;" >> tmp;done
    for i in `seq 18 20`; do echo "set ylabel '$((i-1))';plot 'rawData.csv' u 1:$((i)) t '' w l lc 1;" >> tmp;done
    cat tmp|gnuplot 1>/dev/null 2>/dev/null
    rm tmp
    
    convert -resize 200x200 ScreenShotAll.png rawdata.jpg
    
    LogIt "$ThisDev: End of acquiring"

}



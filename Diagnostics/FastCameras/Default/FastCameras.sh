BasePath=../..;source $BasePath/Commons.sh
source Universals.sh

DAS="AVs/PhotronMiniUX100-a/Radial"


#DeviceList="AVs/PhotronMiniUX100-a/Radial AVs/PhotronMiniUX100-b/Vertical ITs/PhotronCamerasPC/FastCameras AVs/PhotronMiniUX50-a/South AVs/PhotronMiniUX50-b/North"
#DeviceList="AVs/PhotronMiniUX100-a/Radial AVs/PhotronMiniUX100-b/Vertical ITs/PhotronCamerasPC/FastCameras AVs/PhotronMiniUX50-a/South"
DeviceList="AVs/PhotronMiniUX100-a/Radial AVs/PhotronMiniUX100-b/Vertical ITs/PhotronCamerasPC/FastCameras"



function OpenSession()
{
    #mount /mnt/share/PhotronCamerasPC
    sleep 3

}

function CloseSession()
{
    #umount /mnt/share/PhotronCamerasPC
    sleep 3
}


function Arming()
{
    echo '[{"command":"getReady"}]' > /tmp/command.json
    sudo cp /tmp/command.json /mnt/share/PhotronCamerasPC/



}

function GetReadyTheDischarge ()
{
  GeneralTableUpdateAtDischargeBeginning
  #SetupCams
  ssh -f golem@PhotronCamerasPC.golem "cd C:\Users\golem\Documents\Photron\FastCamGOLEM & FastCamGOLEM.exe" > GetReadyTheDischarge.log
  echo OK;
}




function PostDischargeAnalysis
{

    ln -s $BasePath/Devices/AVs/PhotronMiniUX100-a/Radial/ DAS_raw_data_dir
    #convert -resize $icon_size SpeedCamera.png analysis.jpg
    timeout=20
    while ! test -s /mnt/share/PhotronCamerasPC/AVI/CamRad.avi;
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from $ThisDev"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files .. pokud čeká dlouho, je OK mount PC?
    ((timeout--))
    done

    while ! test -s /mnt/share/PhotronCamerasPC/AVI/CamVert.avi;
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from $ThisDev"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files
    ((timeout--))
    done

    # Test if files are not older than specified time limit 
    tdiff=$(( `date +%s` - `stat -c %Y /mnt/share/PhotronCamerasPC/AVI/CamRad.avi` )); 
    if  [ $tdiff -ge 300 ] ;
    then  
        LogitColor 1 'Problem with camera files ... too old'; 
        Speaker Problems/there-is-a-problem-with-the-camera-files
        return
    fi

    
    sleep 10
    mkdir Camera_Radial
    mkdir Camera_Vertical
    #mkdir Camera_South
    #mkdir Camera_North

    #cp /mnt/share/`ls -1tr /mnt/share/|grep C002|tail -1`/*.* Camera_Radial/
    #cp /mnt/share/PhotronCamerasPC/AVI/CamRad.avi Camera_Radial/Data.avi
    ffmpeg -hide_banner -loglevel error -i /mnt/share/PhotronCamerasPC/AVI/CamRad.avi Camera_Radial/Data.mp4 & >  fmpegRadial.log
    #cp /mnt/share/PhotronCamerasPC/AVI/CamVert.avi Camera_Vertical/Data.avi
    ffmpeg -hide_banner -loglevel error -i /mnt/share/PhotronCamerasPC/AVI/CamVert.avi Camera_Vertical/Data.mp4 & >  fmpegVertical.log
    wait
    cp /mnt/share/PhotronCamerasPC/OutputCamsConfig.json .
    cp /mnt/share/PhotronCamerasPC/CamsConfig.json .
    #cp /mnt/share/PhotronCamerasPC/AVI/CamSouth.avi Camera_South/Data.avi
    #cp /mnt/share/PhotronCamerasPC/AVI/CamNorth.avi Camera_North/Data.avi
    #ffmpeg -i /mnt/share/PhotronCamerasPC/AVI/CamSouth.avi Camera_South/Data.mp4
    #ffmpeg -i /mnt/share/PhotronCamerasPC/AVI/CamNorth.avi Camera_North/Data.mp4

    #ffmpeg -loglevel quiet -ss 1 -i Camera_South/Data.mp4  -t 1 -f image2 Camera_South/1s.jpeg
    #ffmpeg -loglevel quiet -ss 1 -i Camera_North/Data.mp4  -t 1 -f image2  Camera_North/1s.jpeg
    
    
    echo '[{"command":"stop"}]' > /tmp/command.json
    sudo cp /tmp/command.json /mnt/share/PhotronCamerasPC/

    
    sed -i "s/shot_no=0/shot_no\ =\ `cat $SHMS/shot_no`/g" Camera_Position.ipynb 
    jupyter-nbconvert --ExecutePreprocessor.timeout=60 --to html --execute Camera_Position.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)
    
    ln -s ../Camera_Radial/CameraRadialPosition Results/CameraRadialPosition.csv
    ln -s ../Camera_Vertical/CameraVerticalPosition Results/CameraVerticalPosition.csv

    convert -resize $icon_size icon-fig.png graph.png
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg 
    cp ScreenShotAll.png rawdata.jpg $SHM0/Devices/AVs/PhotronMiniUX100-a/
    
    notebook=Camera_Position.ipynb
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox
    #MotionDetectHtmlRow


}

function MotionDetectHtmlRow
{
local diagpath=Diagnostics/FastCameras
local devicepath=Devices/AVs/PhotronMiniUX100-a

echo "<h3><b>Motion detection</b>&nbsp;<a href=$diagpath/ title="Data directory">$diricon</a></h3>&nbsp;">>diagrow.html;

  echo "<table><tr>
    <td width='$iconsize' valign=bottom>&nbsp;
    <a href=$diagpath/port_persp_tokamak.jpg title="Diagnostics placement"><img src=$diagpath/port_persp_tokamak.jpg width='$iconsize'/></a>
    </td>
    <td width='$iconsize'>N/A</td>
    <td width='$iconsize' valign=bottom>&nbsp;
    <a href=https://www.dropbox.com/scl/fo/yvwgcf7l1cdl8ta4u7lz0/h?rlkey=h16b25zgun5saf4agck1bdb9j&dl=0 title="Photogallery@DropBox"><img src=$diagpath/MotDet-thumb.jpg /></a>
    </td>
    <td valign=bottom>&nbsp;<a href=$devicepath/das.html><img src=$devicepath/das.jpg  width='$iconsize'/></a></td>
    <td>South<br></br>
    <a href=$diagpath/Camera_South/Data.mp4><img src=$diagpath/Camera_South/1s.jpeg width='$iconsize'/></a></td>
    <td>North<br></br>
    <a href=$diagpath/Camera_North/Data.mp4><img src=$diagpath/Camera_North/1s.jpeg width='$iconsize'/></a></td></tr><table>" >>diagrow.html

}


function SetupCamsParams()
{
echo '
[
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 1600,
    "height": 56,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.12",
    "name": "Camera Vertical",
    "recordRate": 40000,
    "shotDurToSave": 40,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamVert.avi",
    "width": 1280
  },
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 1600,
    "height": 56,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.13",
    "name": "Camera Radial",
    "recordRate": 40000,
    "shotDurToSave": 40,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamRad.avi",
    "width": 1280
  },
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 200,
    "height": 1024,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.10",
    "name": "Camera South",
    "recordRate": '$(<Parameters/recrate_ux50a)',
    "shotDurToSave": 30,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamSouth.avi",
    "width": 1280
  },
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 200,
    "height": 1024,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.14",
    "name": "Camera North",
    "recordRate": 4000,
    "shotDurToSave": 30,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamNorth.avi",
    "width": 1280
  }
]' > /tmp/CamsConfig.json
sudo cp /tmp/CamsConfig.json /mnt/share/PhotronCamerasPC/

    sleep 2
    echo '[{"command":"setCamsConfig"}]' > /tmp/command.json
    sudo cp /tmp/command.json /mnt/share/PhotronCamerasPC/


}

function SetupCams()
{
echo '
[
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 1200,
    "height": 56,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.12",
    "name": "Camera Vertical",
    "recordRate": 40000,
    "shotDurToSave": 30,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamVert.avi",
    "width": 1280
  },
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 1200,
    "height": 56,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.13",
    "name": "Camera Radial",
    "recordRate": 40000,
    "shotDurToSave": 30,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamRad.avi",
    "width": 1280
  },
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 200,
    "height": 1024,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.10",
    "name": "Camera South",
    "recordRate": 4000,
    "shotDurToSave": 30,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamSouth.avi",
    "width": 1280
  },
  {
    "SettingsMode": "SetAll",
    "TriggerMode": "TrigStart",
    "framesToSave": 200,
    "height": 1024,
    "input1": "TrigPos",
    "input2": "SyncPos",
    "ipAddr": "192.168.2.14",
    "name": "Camera North",
    "recordRate": 4000,
    "shotDurToSave": 30,
    "variableChannel": 1,
    "videoFileName": "AVI\\CamNorth.avi",
    "width": 1280
  }
]' > /tmp/CamsConfig.json
sudo cp /tmp/CamsConfig.json /mnt/share/PhotronCamerasPC/

    sleep 2
    echo '[{"command":"setCamsConfig"}]' > /tmp/command.json
    sudo cp /tmp/command.json /mnt/share/PhotronCamerasPC/


}






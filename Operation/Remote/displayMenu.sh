#!/bin/bash

# Define the temporary files for communication
control_file="/tmp/golemRemoteCommand"

SESSION_NAME="golem_remote_session"

exit_on_tmux_detach() {
    tmux list-clients -t "$SESSION_NAME" > /dev/null 2>&1
    tmux_list_exit_status=$?
    echo "Returned: $tmux_list_exit_status" >> /tmp/remote_log
    if [ $tmux_list_exit_status -ne 0 ]; then
        echo "Returned: $tmux_list_exit_status"
        echo "Tmux is detached exiting"
        # send exit command to remote deamon
        echo "exit" > "$control_file"
        # wait 4 min then kill tmux
        sleep 240
        tmux kill-session -t "$SESSION_NAME"
        exit
    fi
}

# Function to display the TUI menu
display_menu() {
    # Define colors for highlighting
    green='\e[1;32m'
    nc='\e[0m' # No color

    # Initialize selected option
    selected=1

    while true; do
        clear
#        exit_on_tmux_detach
        echo "GOLEM Remote Deamon"
        if [[ -f "$control_file" ]]; then
            action=$(cat "$control_file")
	    if [ "$action" == "exit" ];then
	            echo "Exiting"
	    fi
	    if [ "$action" == "run" ];then
	            echo "Executing shot queue"
	    fi
	    if [ "$action" == "runOne" ];then
	            echo "Execute one shot, then pause"
	    fi
	    if [ "$action" == "pause" ];then
	            echo "Execution paused"
	    fi
        fi
        echo ""
        echo "Use arrow keys (← →) to navigate and press Enter to select an option:"

        # Display menu options with highlighting for the selected option
        if [ $selected -eq 1 ]; then
            echo -e "${green}>>Execute Queue<<${nc}\t  Execute One Shot  \t  Pause  \t  Exit"
        elif [ $selected -eq 2 ]; then
            echo -e "  Execute Queue  \t${green}>>Execute One Shot<<${nc}\t  Pause  \t  Exit"
        elif [ $selected -eq 3 ]; then
            echo -e "  Execute Queue  \t  Execute One Shot  \t${green}>>Pause<<${nc}\t  Exit"
        else
            echo -e "  Execute Queue  \t  Execute One Shot  \t  Pause  \t${green}>>Exit<<${nc}"
        fi

        # Read user input
        read -rsn3 input
        case "$input" in
            $'\e[C')  # Right arrow key
                ((selected++))
                [ $selected -gt 4 ] && selected=1
                ;;
            $'\e[D')  # Left arrow key
                ((selected--))
                [ $selected -lt 1 ] && selected=4
                ;;
            "")  # Enter key
                case $selected in
                    1)
                        echo "Continuing..."
                        echo "run" > "$control_file"
                        ;;
                    2)
                        echo "Running one shot from queue..."
                        echo "runOne" > "$control_file"
                        ;;
                    3)
                        echo "Pausing..."
                        echo "pause" > "$control_file"
                        ;;
                    4)
                        echo "Exiting..."
                        echo "exit" > "$control_file"
                        exit 0
                        ;;
                esac
                ;;
        esac
    done
}


# Start menu
display_menu


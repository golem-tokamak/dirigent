#!/bin/bash

BasePath=../../..;source $BasePath/Commons.sh 


DataFile=NI.lvm

source Universals.sh



function RawDataAcquiring()
{
    local shot_no=$(<$SHMS/shot_no)
    timeout=20
    while ssh golem@$ThisDev.golem "! test -s /home/golem_daq/$DataFile";
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from $ThisDev"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for $ThisDev files
    ((timeout--))
    done

    #shot_no=`cat $BASEDIR/shot_no`
    LogItColor 4 "$ThisDev: Start of acquiring"
    scp -o ConnectTimeout=1 golem@$ThisDev.golem:/home/golem_daq/$DataFile .; \
    sed 's/,/\./g' $DataFile > ToPlot.lvm;
 
    
    rm tmp
    echo  "set terminal png size 1600, 1000;set output 'ScreenShotAll.png';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;unset xtics;unset xlabel;set multiplot layout 8,2 columnsfirst title 'GOLEM Shot $shot_no';set xrange [*:*];set yrange [*:*];set style data dots;unset ytics;"> tmp;
    # Bacha, karty jsou zrejme prehozene!
    #for i in `seq 2 17`; do echo "set ylabel '$((i-1))';plot 'ToPlot.lvm' u 1:$((i)) t '' w l lc 1;" >> tmp;done
    # Takze nutno kreslit takhle
    for i in `seq 2 9`; do echo "set ylabel '$((i-1))';plot 'ToPlot.lvm' u 1:$((i+8)) t '' w l lc 1;" >> tmp;done
    for i in `seq 10 17`; do echo "set ylabel '$((i-1))';plot 'ToPlot.lvm' u 1:$((i-8)) t '' w l lc 1;" >> tmp;done
    cat tmp|gnuplot 1>/dev/null 2>/dev/null
   	convert -resize 200x200 ScreenShotAll.png rawdata.jpg

    LogIt "$ThisDev: End of acquiring"


}



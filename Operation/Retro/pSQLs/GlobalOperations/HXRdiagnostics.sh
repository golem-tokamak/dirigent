#!/bin/bash

Probes="CeBr_a CeBr_b YAP_a YAP_b YAP_c NaITl_a NaITl_b LiFZnSAg_a LYSO_a"
#(x,y,z, theta, phi, HV_PS@NIMcrate, HVchannel@NIMcrate, Voltage)
S_CeBr_a=(380 130 72 180 0 A 0 -800) #Scionix
S_ScionixCeBr_b=(380 130 72 180 0 A 0 -800) #Scionix
S_ScionixYAP_a=(380 130 72 180 0 A 1 -800) #Scionix
S_ScionixYAP_b=(380 130 72 180 0 A 1 -800) #Scionix
S_YAP_c=(380 130 72 180 0 A 1 -800) #Crytur
S_NaITl_a=(380 130 72 180 0 A 2 -700) #Envinet
S_NaITl_b=(480 130 72 180 0 A 3 -600) #Nuvia
S_LiFZnSAg_a=(480 130 72 180 0 A 3 -600) #Nuvia
S_LYSO_a=(480 130 72 180 0 A 3 -600) #Capad




function pSQLpreparation()
{
    for i in $Probes; do 
        for j in x y z; do
            echo ALTER TABLE shots
            echo ADD COLUMN "$j"_"$i" INTEGER\;
            echo COMMENT on COLUMN shots."$j"_"$i" IS \'$j position of the probe $i [cm]\'\;
        done
        for j in theta phi; do
            echo ALTER TABLE shots
            echo ADD COLUMN "$j"_"$i" INTEGER\;
            echo COMMENT on COLUMN shots."$j"_"$i" IS \'$j direction of the probe $i [deg]\'\;
        done
        for j in Voltage Current; do
            echo ALTER TABLE shots
            echo ADD COLUMN "$j"_"$i" INTEGER\;
            echo COMMENT on COLUMN shots."$j"_"$i" IS \'$j on the probe $i [V\|mA]\'\;
        done
    
        
    done
}


function Depot
{
    # Copy paste from database
    grep -v Browse DropColumns|awk '{print "ALTER TABLE shots\n DROP column " $1";"}' > DropCommands 
    cat DropCommands | psql -U golem golem_database
    
    
    cat AddCommands |grep COMMENT|grep grad|sed 's/grad/deg/g' > COMMcommands
    cat AddCommands |grep COMMENT|grep mA|sed 's/mA/uA/g' >COMMcommands
    cat COMMcommands | psql -U golem golem_database

}


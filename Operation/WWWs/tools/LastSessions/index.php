<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
 <body>
 <h1>List of all tGOLEM sessions during last year (chamber conditioning excluded)</h1>
<h2>Table</h2>
 <table border=1>
 <tr><th>The first shot (Session id) -> the last shot of the Session</th><th>Date</th><th>Setup name</th><th>Staff</th><th>On stage diagnostics</th><th>Off stage diagnostics</th><th>Analysis</th><th>Infrastructure</th></tr>
<?php
$out = array();
exec ('\
export PGPASSWORD=`cat /golem/production/psql_password`;psql -qAt -U golem golem_database -c "SELECT s.start_shot_no,s.end_shot_no,to_timestamp(s.start_timestamp, \'YY-MM-DD HH24:MI\'),s.session_setup,s.session_mission,s.staff_list,regexp_replace(s.onstage_wave,E\'[\\n\\r]+\',E\'<br>\', \'g\'),regexp_replace(s.offstage_wave,E\'[\\n\\r]+\',E\'<br>\', \'g\'),regexp_replace(s.analysis,E\'[\\n\\r]+\',E\'<br>\', \'g\'),regexp_replace(s.infrastructure,E\'[\\n\\r]+\',E\'<br>\', \'g\'), d.\"X_discharge_command\" FROM operation.sessions s JOIN operation.discharges d ON d.shot_no=s.end_shot_no WHERE to_timestamp(s.start_timestamp, \'YY-MM-DD HH24:MI:SS\' ) > now() - INTERVAL \'360 DAYS\' AND s.session_setup != \'ChamberConditioning\' ORDER BY s.start_shot_no DESC LIMIT 1000"|awk -F\'|\' \'{print "<tr><td colspan="10" ><center><b>"$5"</b></center></td></tr><td><center><a href=\"http://golem.fjfi.cvut.cz/shots/"$1"\">"$1"</a><br/><a href=\"http://golem.fjfi.cvut.cz/shots/"$1"/Infrastructure/Homepage/psql/ShotsOfTheSession.php\">-\></a><br/><a href=\"http://golem.fjfi.cvut.cz/shots/"$2"\">"$2"</a></center></td><td>"$3"</td><td>"$4"</td><td>"$6"</td><td>"$7"</td><td>"$8"</td><td>"$9"</td><td>"$10"</td></tr><tr><td colspan="8">"$11"</td></tr>"}\'', $out);
foreach($out as $line) {
    echo $line;
    echo "\n";
}

echo '</table>';

?>

<h2>SQL code:</h2>
 <pre>
SELECT start_shot_no,to_timestamp(start_timestamp, \'YY-MM-DD HH24:MI\'),session_setup,session_mission,staff_list,regexp_replace(onstage_wave,E\'[\\n\\r]+\',E\'<br>\', \'g\'),regexp_replace(offstage_wave,E\'[\\n\\r]+\',E\'<br>\', \'g\'),regexp_replace(analysis,E\'[\\n\\r]+\',E\'<br>\', \'g\'),regexp_replace(infrastructure,E\'[\\n\\r]+\',E\'<br>\', \'g\') FROM operation.sessions WHERE to_timestamp(start_timestamp, \'YY-MM-DD HH24:MI:SS\' ) > now() - INTERVAL \'360 DAYS\' ORDER BY start_shot_no DESC LIMIT 1000
 </pre>

 <a href=https://gitlab.com/golem-tokamak/dirigent/><img src="/_static/logos/GitLab.png" width="80px"></img></a>

</body></html>


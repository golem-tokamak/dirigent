#!/bin/bash

source Commons.sh

if [ -e $SHMS/session.setup ];  then 
    source $SHMS/session.setup
    for i in `ls /golem/shm_golem/Production/*`; do 
        issue=`basename $i`;
        #echo $issue
        declare $issue="`cat /golem/shm_golem/Production/$issue`";
        export $issue
    done;

#    Operation="Dirigent/HigherPower/Standard $Operation" # extra Dirigent Power
fi

if [ -e /dev/shm/golem/shot_no ]; then SHOT_NO=$(<$SHM/shot_no); fi




TASK=$1
#COMMANDLINE=`echo $@|sed 's/-r //g'`
# ToDo replace special chars: echo '!@#[]$%^&*()-' | sed 's/[]!@#[$%^&*()-]/\\&/g' ..... \!\@\#\[\]\$\%\^\&\*\(\)\-


case "$TASK" in
   "") 
      echo "Usage:
      *** Hotkeys:
      -e,-h,-i,-gr,-cmt,-hfs,-ts,-hints,-xrd,-snb,-tmux :Emergency, Help, Interupt, Grep, commit with bookmarks, hotfixes, hints, xrandr, sandbox
      -rs   :rsync everything @ Rasps, SHMS a SHM0 (not a functions@ShotNo!)
      -s w,p,os@d,r,tr,sdc,s         :session issues: wake-up,ping,opensession@devices,reset, total reset incl rasps, sleep-down, shutdown
      -d (c)d,m,sws,s/swos,sujb,fs,v :dummy,modest,standard w/o stab,sujb,fullsteam, vacuum
      -t rr,b,mc,rd,psa,bu             :rasps reboot,Go to ShotNo in thousands, backup, mount cams, remote daemon, golem proccesses, bookmarks aktualizace
      -gt $(<$SHM/shot_no)           :Goto functions: e.g. Dg -gt $(<$SHM/shot_no)
      -t t1x,t100x              :trigry
      -ch gd, gdwg_i, gdwg_o                    :Glow discharge macro, initiate WG flow, operate WG flow
      locals: Dgscps,Dgkillxterms
      -c                       :Call function@script in $SHMS, e.g. Dg -c Devices/Oscilloscopes/TektrDPO3014-a Wake-upCall
      -cl   :Call with actualization from local, eg: Dg -cl Devices/Oscilloscopes/TektrMSO64-a/LangBallPenProbe OpenSession
      -rel <<which>> ON/OFF,crel:relay controll, lis all relays
      -dbrc                     :DELETE FROM remote.shots WHERE shots.status = 0 OR shots.status = 1
      -HpV,-HepV,-SpV,-offpV  <<Voltage>>         :GasValveTo \$Voltage @H @He @SecondaryValve
      -HmPa,-HemPa,-offpV,-vent  <<pressure pPa>>       :GasFlow Controller \$Voltage @H @He off, flush gas from pipes
      -wgc                      :WG default calibration
      -r wr <<ShotNo>>, wrld : WWW reconstruction, e.g Dg -r wr $(<$SHM/shot_no),last discharge
      -nanoDgds"

      RETVAL=1
      ;;
      -e)
        SwitchRelay PowerSupplyHVcontactor@PowerSupplies OFF
        mRelax
        SwitchRelay ShortCircuits@PowerSupplies OFF
        Broadcast $SHMS "$Ensemble" SecurePostDischargeState
        ssh Charger "ps -Af|grep GetReadyTheDischarge|awk '{print \$2}'|xargs kill"
        SubmitTokamakState "idle"
      ;; 
      -g)
        cd /golem/database/operation/shots/$2;echo pwd: $PWD;echo cd $PWD
      ;;
      -tmux)
      xterm -hold -geometry 200x50+0+0 -fn "-misc-fixed-medium-r-normal--20-*-*-*-*-*-iso8859-15" +sb  -fg yellow -bg blue -title "Session log@GOLEM tokamak" -e "bash /golem/Dirigent/Operation/Session/Setup/Tmux.sh" &
      ;;
      -HpV)
      Voltage=$2
      Call $SHMS/Infrastructure/WorkingGas H2Engage
      Call $SHMS/Infrastructure/WorkingGas OpenRVC300Valve
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo $Voltage
      ;;
      -HepV)
      Voltage=$2
      Call $SHMS/Infrastructure/WorkingGas HeEngage
      Call $SHMS/Infrastructure/WorkingGas OpenRVC300Valve
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo $Voltage
      ;;
      -SpV)
      Voltage=$2
      Call $SHMS/Infrastructure/WorkingGas OpenRVC300Valve
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@SecondaryGasValveTo $Voltage
      ;;
      -HmPa)
      Pressure=$2
      Call $SHMS/Infrastructure/WorkingGas H2Engage
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo 23 # TD change to `tGparm U_OperPress^GD@PS_WG-H` or something simmilar
      Call $SHMS/Infrastructure/WorkingGas PIDSetpoint@RVC300Valve $Pressure
      ;;
      -HemPa)
      Pressure=$2
      Call $SHMS/Infrastructure/WorkingGas HeEngage
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo 43
      Call $SHMS/Infrastructure/WorkingGas PIDSetpoint@RVC300Valve $Pressure
      ;;
      -offpV)
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo 0
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@SecondaryGasValveTo 0
      Relax
      Call $SHMS/Infrastructure/WorkingGas DisEngage
      Call $SHMS/Infrastructure/WorkingGas CloseRVC300Valve
      ;;
      -vent)
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@GasValveTo 0
      Call $SHMS/Infrastructure/WorkingGas SetVoltage@SecondaryGasValveTo 0
      Relax
      Call $SHMS/Infrastructure/WorkingGas DisEngage
      Call $SHMS/Infrastructure/WorkingGas OpenRVC300Valve
      sleep 5s
      if [[ "$2" != "" ]] && [[ "$2" -eq "$2" ]]; then
          thresholdPressure=$2
      else
          thresholdPressure=1
      fi
      echo "waiting for pressure to reach $thresholdPressure mPa (max. 1 min)"
      for ((i=60; i>=0; i--)); do
          currPressure=$(cat "$SHML/ActualChamberPressuremPa")
          (( $(echo "$currPressure < $thresholdPressure" | bc -l) )) && break
          echo "Pressure $currPressure mPa ($i s till timeout)"
          sleep 1
      done
      Call $SHMS/Infrastructure/WorkingGas CloseRVC300Valve
      ;;
      -i)
        touch $SHM0/Operation/Discharge/Parameters/Interrupt
      ;;
      -c)
        Call $SHMS/$2 $3  
      ;;
      -cl)
        cp  -v $SW/$2/* $SHMS/`dirname $2`/
        Call $SHMS/`dirname $2` $3
      ;;
      -hfs) #hot fixes
        echo 'Dg -rel 2NI_PC-VoSv@InfrastructureSockets OFF;sleep 2s;Dg -rel 2NI_PC-VoSv@InfrastructureSockets ON'
        echo 'Dg -rel TimepixDetector@TokamakSockets ON'
        echo 'Dg -c Devices/Oscilloscopes/TektrMSO64-a OpenSession'
        echo 'Dg -cl Devices/Oscilloscopes/TektrMSO64-a/LangBallPenProbe OpenSession'
        echo 'ssh Charger "source Rasp-Bt_Ecd.sh ;ScopeConfiguration"'
        echo '============ Nahozeni po E fail============'
        echo 'Dg -rel Kepcos@StabilizationOFF'
        echo 'Dg -c Devices/Amplifiers/KepcoStabilizationRack  Wake-upCall'
        echo 'Dg -c Infrastructure/PositionStabilization OpenSession'
        echo 'echo idle >/golem/production/tokamak_state'
        echo '============ Nahozeni po Basic diagnostics osc fail============'
        echo '@LC: google-chrome --new-window --app=http://192.168.2.143/Tektronix/#/client/c/Tek%20e*Scope'
        echo '@Dg: Dg -c Devices/Oscilloscopes/TektrMSO56-a OpenSession'
        echo '@LC: bash /golem/Dirigent/Operation/Session/Setup/ChamberLog.sh'
      ;;
      -hints) 
        echo 'Rigol:rigol@admin'
        echo '192.168.2.116, tek_drop, golem, tokamak'
        echo 'Remote interface je zatim na /golem/database/www/remote_app/golem_web_app @ Je nutny Apache restart'
        echo 'CO=Kvartet;source Commons.sh;Call $SHMS/Diagnostics/BasicDiagnostics4TrainCourses/ TeacherScopesSetting'
        echo 'Dg -c Infrastructure/Preionization GetReadyTheDischarge/SecurePostDischargeState'
        echo 'Dg -c Devices/Oscilloscopes/XYosc OpenSession';
      ;;
      -ts)
        echo 'FastCameras: PC mounted? df@Dg://PhotronCamerasPC/FastCamGOLEM ..'
      ;;
      -rel)
        #Call $SHMS/Devices/RelayBoards  $2 $3  
        bash -c "cd $SW/Devices/RelayBoards;source RelayBoards.sh;$2 $3" 
      ;;
      -crel)
        grep function $SW/Devices/RelayBoards/RelayBoards.sh
      ;;
      -p)
        PingAllDevicesNtimes -N 1
      ;;
      -xrd)
        nano /golem/Dirigent/Operation/Session/Setup/Monitors4GOLEM.sh
      ;;
      -gr)
      grep -r "$2" *|grep -v zmbs|grep -v html|grep -v Deposit|grep -v svg|grep -v ipynb|grep -v cpp|grep -v x3dom|grep -v Setups|grep -v Bookmarks|grep -v remote_app;
      ;;
      -psa)
      ps -Af|grep golem|grep -v VacuumLog|grep -v OpenSession|grep -v Discharge.sh|grep -v sshd|grep -v "\-bash"|grep -v "\-k start"|grep -v sftp|grep bash|grep -v "\-\-discharge"|grep -v "\-\-session"
      ;;

      -wgc)
        xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
        Call $SHMS/Infrastructure/WorkingGas H2Calibration 18 1 50 20"
        ;;
      -wgc-nz)
        xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
        Call $SHMS/Infrastructure/WorkingGas H2Calibration-nz 18 1 50 20"
        ;;
      -dbrc)
        $psql_password;psql -c "DELETE FROM remote.shots WHERE shots.status = 0 OR shots.status = 1" -q -U golem -d golem_database
      ;;
      -rs) #SW update everywhere
            source Commons.sh ; 
            rsyncRASPs 
            cd $SW
            cp Commons.sh Dirigent.sh session.setup $SHMS/;
            cp Commons.sh Dirigent.sh session.setup  $SHM0/;
            source session.setup
            PrepareEnvironment@SHM $SHMS
            PrepareEnvironment@SHM $SHM0
            cp $SW/Devices/RelayBoards/RelayBoards.sh $SHMS/Devices/RelayBoards/
            cp $SW/Devices/Infrastructure/Racks/Universals.sh $SHMS/Devices/Infrastructure/Racks/
        ;;
        -cmt)
            git add .; git commit -am "$2";git push #V Kate musi byt vse ulozeno/zavreno !
            echo Bookmarks aktualizace /diky OpenEnvironment/:
            cd /golem/Dirigent/Operation/WWWs/ShowroomBookmarks/;bash script.sh;cd $OLDPWD
        ;;
        -w)
          xterm -fg yellow -bg black -fa 'Monospace' -fs 30 -title "Watch" -e "watch -t \"echo -n Time:;date '+%H:%M:%S';echo -n Shot No:; cat /golem/shm_golem/shot_no;echo -n Pressure:; cat /golem/shm_golem/ActualSession/SessionLogBook/ActualChamberPressuremPa;echo ' mPa';if [ -f "/golem/shm_golem/ActualShot/comment" ];then echo -n On air:;cat /golem/shm_golem/ActualShot/comment;fi;echo -n Status:;cat /golem/shm_golem/ActualSession/SessionLogBook/tokamak_state\""
        ;;
        -sandbox)
        echo ahoj `tGparm U_BDPress^GD@PS_WG-H`
        ;;
       -nanoDgds)
       cd Operation/Discharge/Basic/Styles/; nano standard-without-position-stabilizationCMD.html;cd -
        ;;
       -gt)
          cd /golem/database/operation/shots/$2;pwd
        ;;


      -h)
        echo "      ============ Session issues ============
      -s SetupSetup <<PathToSetup>>       :ln -s \$FromRootPathToSetup session.setup
      -s ping|p                 :PingAllDevices
      -s wake|w                 :Broadcast Devices Wake-upCall
      -s sleep|sdc              :Broadcast Devices Sleep-downCall
      -s open|o                 :Call Operation/Session/Basic SetupSession
      -s os@d                   :Broadcast Devices OpenSession
      -s reset|r                :Call Operation/Session/Basic ResetSession
      -s shutdown|s             :Broadcast Devices CloseSession
      ============ Discharge issues ============
      -d <<Parameters>>                   :make the discharge with all possible Parameters
      -d dummy|d              :bash Operation/Discharge/Basic/Styles/dummyCMD.html
      -d modes|m              :bash Operation/Discharge/Basic/Styles/modestCMD.html
      -d standard|s           :bash Operation/Discharge/Basic/Styles/standardCMD.html
      -d high|h               :bash Operation/Discharge/Basic/Styles/highCMD.html      
      -d vacuum|v             :bash Operation/Discharge/Basic/Styles/vacuumCMD.html      
      -d sujb|dds             :bash Operation/Discharge/Basic/Styles/sujbCMD.html      
      -d fullsteam|fs         :bash Operation/Discharge/Basic/Styles/fullsteamCMD.html
      -d a                    :apparent (bez HW)
      ============ Chamber issues ============
      -ch pumping='on'|pon      :Call Chamber/Basic PumpingON
      -ch pumping='off'|poff    :Call Chamber/Basic PumpingOFF
      -ch baking='on'|bon       :Call Chamber/Basic Baking_ON <<TempLimit>> <<PressLimit>>, eg. Dg -ch bon 200 20
      -ch bon-def               :Call Chamber/Basic Baking_ON (200 5)
      -ch baking='off'|boff     :Call Chamber/Basic Baking_OFF
      -ch wgsv <<Voltage>>      :SetVoltage@GasValveTo \$Voltage @ChamberRASP
      -ch wgcal <<U_LowerVoltageLimit>> <<U_Step>> <<p_UpperPressureLimit>> <<t_Relax>>, e.g. Dg --ch wgcal 16 0.25 50 25
      -ch wgcal-def             :(default) ~ Dg -ch wgcal 17 0.5 50 25
      -ch gdi                   :Call Chamber/Basic GlowDischInitiate
      -ch gdwg_i                :Call Chamber/Basic GlowDischGasFlowSetup START
      -ch gdwg_o                :Call Chamber/Basic GlowDischGasFlowSetup OPERATE
      -ch gdfs <<Voltage2Valve>>:Call Chamber/Basic GlowDischGasFlowSetup \$Voltage2Valve (68/45)
      -ch gds                   :Call Chamber/Basic GlowDischStop
      -ch gdw <<Time>>          :Call Chamber/Basic GlowDischWaitForStop \$Time
      ============ Retro actions ============
      -r WWWsReconstruction|wr <<ShotNo>>        : www for the \$ShotNo reconstruction;; e.g. Dg -r wr 41370
      -r PostDischargeAnalysis|pda <<ShotNo>> <<WHAT>>   : PostDischargeAnalysis for \$What@\$ShotNo; e.g. Dg -r pda 41370 Diagnostics/BasicDiagnostics/DetectPlasma/
      ============ Tools ============
      -t goto|g <<ShotNo>>        : cd /golem/database/operation/shots/\$ShotNo; e.g. Dg -t g 41338
      -t gotolastshot|gls        : cd /golem/database/operation/shots/<<LastShot>>
      -t gotointhousands|git <<XXShotNo>>        : cd /golem/database/operation/shots/((shot_no/1000*1000))+\$ShotNo; e.g. Dg -t git 338
      -t call|c <<FullPathToScript>> <<Function>>: e.g. Dg -t c /golem/database/operation/shots/41370/Diagnostics/BasicDiagnostics/DetectPlasma GetReadyTheDischarge
      -t rd                     :Run Remote deamon
      -t trigger|t1x or t100x                             :Make a trigger
      -t backup|b                              : Backup
      -t raspsreboot|rr         :Reboot rasps
      -t raspsync|rs            :Rsync rasps
      -t mfc                            :mount fast cameras
      --broadcast|-b opensession|os     :Broadcast Devices OpenSession
      --broadcast|-b emergency|e        :Broadcast Ensemble SecurePostDischargeState
      -t grep                           :Smart grep (-v zmbs html Deposit svg ipynb cpp js )
      -t cnp                            :Control panel (Ctrl-Supr-G)
      ============ Database issues ============
      -dbrc                     :DELETE FROM remote.shots WHERE shots.status = 0 OR shots.status = 1
      ============ @Dirigent NB ============
      Dgfgp                   :Reset feedgnuplot (run on local NB as alias)
      bash /golem/Dirigent/Operation/Session/Setup/Scopes.sh
      Dgkillxterms            :Sestreli vsechny xterminaly
      ssh4sql                 :Nahodi ssh reverse pro sql
      DgMons                  :cat Operation/Session/Setup/Monitors4GOLEM.sh"
      ;; 

      --tools|-t)
      case "$2" in 
        raspsreboot|rr)
            ssh golem@Chamber "sudo /sbin/reboot";
            ssh golem@Charger "sudo /sbin/reboot";
            ping Chamber
        ;;
        cnp)
            Call Infrastructure/Control/ControlPanel BasicManagementCore
        ;;
        nd)
          wget http://192.168.2.244/set.xml?type=v\&val="$SHOT_NO"
        ;;

        mc)
            sudo mount.cifs //PhotronCamerasPC/FastCamGOLEM /mnt/share/PhotronCamerasPC/ -o user=golem
        ;;
        goto|g)
            cd /golem/database/operation/shots/$3;pwd
        ;;
        grep)
            grep -r "$3" *|grep -v zmbs|grep -v html|grep -v Deposit|grep -v svg|grep -v ipynb|grep -v cpp|grep -v js;     
        ;;
        mfc)
            sudo mount.cifs //PhotronCamerasPC/FastCamGOLEM /mnt/share/PhotronCamerasPC/ -o user=golem
        ;;
        gotointhousands|git)
            cd /golem/database/operation/shots/$(($(</golem/shots/0/shot_no)/1000*1000+$3));pwd
        ;;
        gotolastshot|gls)
            cd /golem/database/operation/shots/$(($(</golem/shots/0/shot_no)));pwd
        ;;
        call|c)
            Call $3 $4  
        ;;
        t1x) #basic trigger test
            Call $SHMS/Infrastructure/Triggering TriggerTest1x
        ;;
        t100x) #basic trigger test
            Call $SHMS/Infrastructure/Triggering TriggerTest100x
        ;;
        psa)
            ps -Af|grep golem
        ;;
        rd) 
            xterm -fg yellow -bg blue -title "Remote daemon" -e "bash Operation/Remote/RemoteInterface.script"
        ;;
        backup|b) #backup
            $psql_password;pg_dump golem_database > golem_database.sql; 
            rsync -r -u -v -K -e ssh --exclude '.git' $PWD svoboda@bn:backup/Dirigent/`date "+%y%m%d"`
            zip golem_database golem_database.sql;mpack -s "GM database `date`" golem_database.zip tokamakgolem@gmail.com;
            rm golem_database.*
      ;;
      bu)
            echo Nejdrive ale @lc: scp .config/google-chrome-Dirigent/Default/Bookmarks golem@golem.fjfi.cvut.cz:/golem/Dirigent/Operation/WWWs/ShowroomBookmarks/
            cd /golem/Dirigent/Operation/WWWs/ShowroomBookmarks/;bash script.sh;cd $OLDPWD
        ;;

      esac
      return
      ;;
      --session|-s)
      case "$2" in 
        SetupSetup)
        cd $SW
        rm session.setup;ln -s $3 session.setup
        bash -c "cd $SW/Operation/Session/Basic;source Session.sh;SetupSession"
        ;;
      ping|p)
        PingAllDevicesNtimes -N 4
        #(for i in $(seq 1 4); do echo Ping $i/4; if PingAllDevices; then return; fi;done)
        #(PingAllDevices) #@Commons
        ;;
      ping_exit)
        (for i in $(seq 1 8); do echo Ping $i/8; if PingAllDevices; then exit; fi;done)
        ;;
      wake|w)
        (Broadcast $SHMS Devices Wake-upCall)
        ;;
      sleep|sdc)
        (Broadcast $SHMS Devices Sleep-downCall)
        ;;
      reset|r)
        bash -c "cd $SW/Operation/Session/Basic;source Session.sh;ResetSession"
        ;;
      open|o)
        Broadcast $SHMS Devices Wake-upCall
        (for i in $(seq 1 8); do echo Ping $i/8; if PingAllDevices; then exit; fi;done)
        for Dev in $RASPs;do Call $SHMS/Devices/ITs/`dirname $Dev` PrepareSessionEnv@SHM;done
        Broadcast $SHMS  "$Ensemble" OpenSession
        SubmitTokamakState "idle" 
        ;;
      os@d) 
        (Broadcast $SHMS Devices OpenSession)
        SubmitTokamakState "idle"
      ;;
      totalreset|tr)
            bash -c "cd $SW/Operation/Session/Basic;source Session.sh;ResetSession"
            ssh golem@Chamber "sudo /sbin/reboot";
            ssh golem@Charger "sudo /sbin/reboot";
            ping Chamber
        ;;
      shutdown|s)
      Call $SHMS/Operation/Session ShutDown
      ;;

      esac
      ;;
      --chamber|-ch)
      case "$2" in 
        pumping='on'|pon)
        xterm -fg yellow -bg blue -title "Golem pumping start" -e "source Commons.sh;Call $SHMS/Infrastructure/Chamber PumpingON"
        echo pumpingON
        ;;
        pumping='off'|poff)
        xterm -fg yellow -bg blue -title "Golem pumping end" -e "source Commons.sh;Call $SHMS/Infrastructure/Chamber PumpingOFF" 
        ;;
        baking-def='on'|bon-def) #baking ON with default values
        xterm -fg yellow -bg blue -title "Baking status" -hold -e "cd $SHMS/Infrastructure/Chamber; source Chamber.sh; Baking_ON 150 15" #Final Temperature Pressure
        ;;
        baking='on'|bon) #baking ON
        TempLimit=${3:-150};PressLimit=${4:-15};
        xterm -fg yellow -bg blue -title "Baking status" -hold -e "cd $SHMS/Infrastructure/Chamber; source Chamber.sh; Baking_ON $TempLimit $PressLimit" #Final Temperature Pressure
        ;;
        baking='off'|boff) #baking OFF
        cd $SHMS/Infrastructure/Chamber; source Chamber.sh; Baking_OFF; cd $SW
        ;;
        wgsv)
        Voltage=$3
        Call $SHMS/Infrastructure/WorkingGas/Hydrogen SetVoltage@GasValveTo $Voltage
        ;;
        wg0)
        ssh Chamber "source Rasp-WorkingGas.sh ;GasH2OFF;GasHeSetVoltage@GasValveTo $3";
        ;;
        wgcal) #With Lower $1 and Uppper $2 limits 
        # e.g. ./Dirigent.sh --wgcal 16 0.5 24 25
            LowerLimit=${3:-16.5};Step=${4:-0.25};UpperLimit=${5:-21};RelaxTime=${6:-15}
            xterm -fg yellow -bg blue -title "Golem WG calibration" -e "source Commons.sh;
            Call $SHMS/Infrastructure/WorkingGas H2Calibration $LowerLimit $Step $UpperLimit $RelaxTime"
        ;;
        # ============ Glow discharge issues START ============
        gd) #Glow discharge init
        if [[ "$3" == "" ]]; then
         read -e -p "GD v jakém plynu? [H2/He]" -i 'H2' gasType; 
        else
          gasType="$3"
        fi
        if [ $gasType == "H2" ]; then
          Call $SHMS/Infrastructure/Chamber GlowDischInitiate
          Call $SHMS/Infrastructure/WorkingGas OpenRVC300Valve
          Relax
          Call $RelayBoards H2-pipe@Vacuum ON
          #GDStartVoltage=`tGparm U_BDPress^GD@PS_WG-H`
          GDStartVoltage=37
          #GDOperationVoltage=`tGparm U_OperPress^GD@PS_WG-H`
          GDOperationVoltage=23
          #waitTime=`tGparm t_Oper^GD@WG-H`
          waitTime=10
        elif [ $gasType == "He" ]; then
          Call $SHMS/Infrastructure/Chamber GlowDischInitiate
          Call $SHMS/Infrastructure/WorkingGas OpenRVC300Valve
          Relax
          Call $RelayBoards He-pipe@Vacuum ON
          #GDStartVoltage=`tGparm U_BDPress^GD@PS_WG-He`
          GDStartVoltage=73
          #GDOperationVoltage=`tGparm U_OperPress^GD@PS_WG-He`
          GDOperationVoltage=43
          #waitTime=`tGparm t_Oper^GD@WG-He`
          waitTime=10
        else
          echo "H2 nebo He; neznámá volba $gasType"
          return
        fi
        if [[ "$4" != "" ]] && [[ "$4" -eq "$4" ]]; then
           waitTime=$4
           echo "GD po dobu $waitTime min"
        fi
        echo "Otevri ventil plynu a pust HV"
        Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $GDStartVoltage
        read -e -p "GD nahozen, snizit tlak? [y/n]" -i 'y' answer; if [ $answer == "y" ]; then 
          Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $GDOperationVoltage
        fi
        Call $SHMS/Infrastructure/Chamber GlowDischWaitForStop $waitTime
        ;;
        gds) #Glow discharge stop 
        Call $SHMS/Infrastructure/Chamber GlowDischStop
        ;;
        gdwg_i) #Glow discharge init 
        ;;
        gdwg_o) #Glow discharge operate
        ;;
        gdfs) 
        Voltage=$3
        #Call $SHMS/Infrastructure/Chamber GlowDischGasFlowSetup $Voltage
        Call $SHMS/Devices/PowerSupplies/GWInstekPSW-a GasFlowSetup $Voltage
        ;;
        gdw) 
        Time=$3 #[min]
        echo "PROCES je @ golem@Dirigent>ps -Af|grep golem|grep GlowDischWaitForStop (awkkill pripadne)"
        Call $SHMS/Infrastructure/Chamber GlowDischWaitForStop $Time
        ;;
      # ============ Glow discharge END ============
      esac
      ;;
      --retro|-r)
      case "$2" in
        WWWsReconstruction|wr)
        bash -c "
        cd /golem/database/operation/shots/$3/Infrastructure/Homepage;\
        zip `date '+%d%m%y-%H%M'`_WWWsReconstruction * -x *.zip;\
        cp -r $SW/Analysis/Homepage/Basic/* .;\
        cp $SW/Commons.sh ../../
        source Homepage.sh; MakeProgressingPage Finalization";
        ;;
        WWWsReconstruction|wrld)
        bash -c "
        cd /golem/database/operation/shots/$(<$SHM/shot_no);\
        cp $SW/Commons.sh $SW/Dirigent.sh .;\
        source Commons.sh;\
        source session.setup
        PrepareEnvironment@SHM /golem/database/operation/shots/$(<$SHM/shot_no)/
        Call Diagnostics/BasicDiagnostics GenerateDiagWWWs;\
        Broadcast . Diagnostics_OnStage GenerateDiagWWWs;\
        Broadcast . Diagnostics_OffStage GenerateDiagWWWs;\
        cd Infrastructure/Homepage;\
        source Homepage.sh; MakeProgressingPage Finalization";
        ;;
        wr0) #zatim nefunguje ...
        bash -c "
        cd $SHM0;\
        cp $SW/Commons.sh .;\
        source Commons.sh;\
        Broadcast $SHM0 "$Ensemble" GenerateDiagWWWs;\
        cd $SHM0/Infrastructure/Homepage;\
        cp -r $SW/Analysis/Homepage/Basic/* .;\
        source Homepage.sh; MakeProgressingPage Finalization";
        ;;
        PostDischargeAnalysis|pda)
        ShotNo=$3;What=$4
        bash -c "\
        cd /golem/database/operation/shots/$ShotNo/$What;\
        zip `date '+%d%m%y-%H%M'`_PostDischargeReconstruction * -x *.zip;\
        cp -r $SW/$What/* .;\
        source $(echo $What|xargs dirname|xargs basename).sh; PostDischargeAnalysis;
        ls -all;pwd";
        ;;
      esac
      ;;
      --broadcast|-b)
      case "$2" in
      opensession|os) 
        (Broadcast Devices OpenSession)
      ;;
      emergency|e)

        Broadcast "$Ensemble" SecurePostDischargeState
      ;; 
      esac
      ;;
      # ============ Tests START ============
      -d)
      case "$2" in 
        style='dummy'|dummy|d)
        echo 'dummy' > $SHM/Production/style
        grep -v 'pre>' Operation/Discharge/Basic/Styles/dummyCMD.html|bash
        ;;
        cd)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/dummyCMD.html
        ;;
        style='modest'|modest|m)
        pwd
        grep -v 'pre>' Operation/Discharge/Basic/Styles/modestCMD.html|bash
        ;;
        cm)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/modestCMD.html
        ;;
        style='standard-ws'|standard-with-stabilization|sws)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/standard-with-position-stabilizationCMD.html|bash
        ;;
        style='standard-wos'|standard-without-stabilization|swos|s)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/standard-without-position-stabilizationCMD.html|bash
        ;;
        style='vacuum'|vacuum|v)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/vacuumCMD.html|bash
        ;;
        cv)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/vacuumCMD.html
        ;;
        cswos|cs)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/standard-without-position-stabilizationCMD.html
        ;;
        csws)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/standard-witho-position-stabilizationCMD.html
        ;;
        style='sujb'|sujb|dds)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/sujbCMD.html|bash
        ;;
        style='fullsteam'|fs)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/fullsteamCMD.html|bash
        ;;
        cfs)
        grep -v 'pre>' Operation/Discharge/Basic/Styles/fullsteamCMD.html
        ;;
        a)
        echo apparent discharge
        ;;
      esac
      ;;
      --discharge)
      if [[  $(</golem/production/tokamak_state) != 'idle' ]]; then EchoItColor 1 "NELZE! Nejaky vyboj uz asi bezi! ";Speaker Problems/The-discharge-cannot-be-proces;exit;fi
        rm -rf  $SHM0 $SHMCLP/* 
        # Necessary Inicialization:
        mkdir -p $SHM0/Operation/Discharge $SHMCLP $SHM0TAGS
        echo $$ > $SHM0/Operation/Discharge/Discharge.pid #PID info
        cp Dirigent.sh Commons.sh $SHM0/ 
        cp $SW/Operation/Discharge/Basic/Discharge.sh $SHM0/Operation/Discharge/ #Sorry
        mv $SHM/Tags/* $SHM0TAGS/ 2>/dev/null
        cd $SHM0


        args=("$@")
        
        CL='./Dirigent.sh --discharge'
        #CL='./Dirigent.sh'

        while [ $# -gt 0 ]; do
        if [[ $1 == *"--"* ]]; then
            v="${1/--/}"
            if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "infrastructure." ]] || [ $v == "ScanDefinition" ] || [[ "$v" =~ "operation." ]]; then
                CL="$CL --$v \"$2\"";
                echo $2 > $SHMCLP/$v
            elif [ $v == "discharge" ]; then
                :
            else
                declare $v="$2"
                CL="$CL --$v $2"
                echo $2 > $SHMCLP/$v
            fi
        fi
    shift
    done
    echo "$CL" > $SHMCLP/CommandLine

    set -- $args
        Call $SHM0/Operation/Discharge Discharge
    ;;
          --dischange|--dc)
        if [[  $(</golem/production/tokamak_state) != 'idle' ]]; then EchoItColor 1 "NELZE! Nejaky vyboj uz asi bezi! ";Speaker Problems/The-discharge-cannot-be-proces;exit;fi
        rm -rf  $SHM0 
        # Necessary Inicialization:
        mkdir -p $SHM0/Operation/Discharge/ $SHMCLP $SHM0TAGS
        cp Dirigent.sh Commons.sh $SHM0/ 
        cp $SW/Operation/Discharge/Basic/Discharge.sh $SHM0/Operation/Discharge/ #Sorry  for Basic
        mv $SHM/Tags/* $SHM0TAGS/ 2>/dev/null

        args=("$@")
        
        CL='./Dirigent.sh --dischange'
        #CL='./Dirigent.sh'

        while [ $# -gt 0 ]; do
        if [[ $1 == *"--"* ]]; then
            v="${1/--/}"
            declare $v="$2"
            echo $2 > $SHMCLP/$v
    #       echo $v=$2
            if [ $v == "comment" ] || [[ "$v" =~ "diagnostics." ]] || [[ "$v" =~ "infrastructure." ]] || [ $v == "ScanDefinition" ] || [[ "$v" =~ "operation." ]]; then
                CL="$CL --$v \"$2\"";
            elif [ $v == "dischange" ]; then
            echo ;
            else
            CL="$CL --$v $2"
            fi
        fi
    shift
    done
    echo "$CL" > $SHMCLP/CommandLine

    set -- $args
        Call $SHMS/Operation/Discharge Discharge
    
#      ;;
esac


#Tuning ...

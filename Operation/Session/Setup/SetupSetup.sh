#!/bin/bash

xterm -sb -bg black -fg white -hold -geometry 200x50+0+0 -title "Session setup: `basename $1 .setup` " -e "
function DoAtDirigent { 
read -e -p \"Do you want to \$1? [y/n] \" -i 'y' answer; 
#echo \$answer;
if [ \$answer == "y" ]; then 
    ssh -Y golem@golem 'export TERM=vt100;cd /golem/Dirigent/;./Dirigent.sh '\$2' '\$3' '\$4''; 
fi
}

function DoDirigent { 
    ssh -Y golem@golem 'export TERM=vt100;cd /golem/Dirigent/;./Dirigent.sh '\$1' '\$2' '\$3' '\$4''; 
}

function DoBashIt { 
read -e -p \"\$1? [y/n] \" -i 'y' answer; 
#echo \$answer;
if [ \$answer == "y" ]; then 
#    echo \$2
     bash -c /golem/Dirigent/Operation/Session/Setup/\$2.sh 
fi
}



echo Zastaveni stareho setup ..
echo =======================
ssh -Y golem@golem 'export TERM=vt100;cd /golem/Dirigent/;./Dirigent.sh --session reset;rm /golem/Dirigent/session.setup'; 
killall gnuplot
echo Nastaveni noveho setup .. '$1'
echo =======================
DoDirigent --session SetupSetup '$1';
#DoDirigent --session wake
#read
#DoDirigent --session ping_exit
bash -c /golem/Dirigent/Operation/Session/Setup/Monitors4GOLEM.sh #Without question
bash -c /golem/Dirigent/Operation/Session/Setup/OpenCurrentSession.sh
if ! (wmctrl -lG|grep -q '192.168') && grep -Rq 'Operation/Discharge/Basic' /golem/shm_golem/Production/AllInstruments ; then bash -c /golem/Dirigent/Operation/Session/Setup/Scopes.sh; fi
echo 'Nezapomen udelat DDS4SUJB!: Dgdds'
echo 'PAK Baking (Ctrl+Supr+B)'
echo 'pak Glow discharge (Ctrl+Supr+G)'
echo end
read
"

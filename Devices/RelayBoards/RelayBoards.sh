BasePath=../..;source $BasePath/Commons.sh

function Relay {
RelayBoardName=$1
RelayNo=$2
SwitchTo=$3
    LogIt "#$RelayNo@$RelayBoardName  (${FUNCNAME[1]}) goes $SwitchTo .. "; 
    case "$SwitchTo" in
    'ON')  
    echo "*B1OS"$RelayNo"H"|telnet Quido-$RelayBoardName 10001  1>/dev/null 2>/dev/null;
    ;;
    'OFF')
    echo "*B1OS"$RelayNo"L"|telnet Quido-$RelayBoardName 10001 1>/dev/null 2>/dev/null;
    ;;
    esac
}

function Relay_ON-OFF
{
RelayBoardName=$1
RelayNo=$2

    Relay $RelayBoardName $RelayNo ON
    Relax
    Relay $RelayBoardName $RelayNo OFF
}

function Relay@Charger {
RaspName=Charger
RelayNo=$1
SwitchTo=$2
    LogIt "#$RelayNo@$RelayBoardName  (${FUNCNAME[1]}) goes $SwitchTo .. "; 
    case "$SwitchTo" in
    'ON')  
    ssh $RaspName "source Rasp-Bt_Ecd.sh;RelayON $RelayNo"
    ;;
    'OFF')
    ssh $RaspName "source Rasp-Bt_Ecd.sh;RelayOFF $RelayNo"
    ;;
    esac
}

# ============ functions@Charger  ============
function BtHVrelay@Charger  { Relay@Charger 4 $1; }
function LV_PS@Charger  { Relay@Charger 3 $1; }
function CdHVrelay@Charger { Relay@Charger 2 $1; }
function Cdcontactor@Charger { Relay@Charger 13 $1; }
function BtCommutatorClockWise { Relay@Charger 5 ON ;mRelax; Relay@Charger 5 OFF; }
function BtCommutatorAntiClockWise { Relay@Charger 6 ON ;mRelax; Relay@Charger 6 OFF; }







#echo "*B1OS5H"|telnet Quido-Racks 10001

# ============ functions@South Sockets  ============
# 8-d .110 :5e ;P2@SwitchBoard


function ThirdFloor@SouthSockets  { Relay SouthSockets 7 $1; }
function Interferometry@SouthSockets  { ThirdFloor@SouthSockets "$@"; }
function Interferometry  { ThirdFloor@SouthSockets "$@"; }

function SecondFloor@SouthSockets  { Relay SouthSockets 6 $1; }

function FirstFloor@SouthSockets  { Relay SouthSockets 5 $1; }
function GroundFloor@SouthSockets  { Relay SouthSockets 4 $1; }
function CurrentStabilization@SouthSockets  { GroundFloor@SouthSockets "$@"; }
function CurrentStabilization  { GroundFloor@SouthSockets "$@"; }




# ============ functions@Tokamak sockets ============
# 8-e .111 :EE ;P3@SwitchBoard

#?? F30
#NorthWestDown F3
#EastUp F32
function SouthWestDown@TokamakSockets { Relay TokamakSockets 1 $1; }  #F34
function TimepixDetector@TokamakSockets { Relay TokamakSockets 1 $1; } #SouthWestDown
function Manipulator@TokamakSockets { Relay TokamakSockets 4 $1; } #NorthEastDown F33
#SouthEastDown F35
function SouthUp@TokamakSockets { Relay TokamakSockets 5 $1; } #F36 SouthUp
function FastCameras@TokamakSockets { Relay TokamakSockets 5 $1; } #F36 SouthUp
function EastUp@TokamakSockets { Relay TokamakSockets 6 $1; } #EastUp
function NortWestUp@TokamakSockets { Relay TokamakSockets 7 $1; } #NortWestUp F37
function FastSpectrometry@TokamakSockets { NortWestUp@TokamakSockets "$@"; }

# ============ functions@Infrastructure sockets ============
# 8-h .114 :8D ;P7@SwitchBoard

function XY@InfrastructureSockets { Relay InfrastructureSockets 8 $1; }
function Chiller@InfrastructureSockets { Relay InfrastructureSockets 5 $1; }
function Police-I@InfrastructureSockets { Relay InfrastructureSockets 5 $1; }
function Police-II@InfrastructureSockets { Relay InfrastructureSockets 6 $1; }
function Police-III@InfrastructureSockets { Relay InfrastructureSockets 7 $1; }
function 2NI_PC-VoSv@InfrastructureSockets { Relay InfrastructureSockets 3 $1; } 
function MainSwitch4CurrentStabilization { Relay InfrastructureSockets 1 $1; }
function AETECHRON4CurrentStabilization { Relay InfrastructureSockets 2 $1; }
function Vlevo-I@InfrastructureSockets { Relay InfrastructureSockets 1 $1; }
function Vlevo-II@InfrastructureSockets { Relay InfrastructureSockets 2 $1; }
function Vlevo-Strop@InfrastructureSockets { Relay InfrastructureSockets 8 $1; }
function Vpravo-I@InfrastructureSockets { Relay InfrastructureSockets 3 $1; }
function Vpravo-II@InfrastructureSockets { Relay InfrastructureSockets 4 $1; }

# ============ functions@North & Papouch Sockets  ============
# 8-f .112 :ef ;P4@SwitchBoard
function XY { Relay NorthSockets_Papouch 1 $1; } #First floor
function MiniSpectrometer@NorthSockets_Papouch { Relay NorthSockets_Papouch 2 $1; } #Second floor
function NorthWall@NorthSockets_Papouch { Relay NorthSockets_Papouch 3 $1; } # North wall
function Pap-Kopecky@NorthSockets_Papouch  { Relay NorthSockets_Papouch 4 $1; }
function Pap-Zacek@NorthSockets_Papouch  { Relay NorthSockets_Papouch 5 $1; }
function Pap-Jakubek@NorthSockets_Papouch  { Relay NorthSockets_Papouch 6 $1; }
function Pap-Stockel@NorthSockets_Papouch  { Relay NorthSockets_Papouch 7 $1; }
function Pap-Jiranek@NorthSockets_Papouch  { Relay NorthSockets_Papouch 8 $1; }


# ============ functions@Miscellaneous ============
# 4-a .240 :4C
function Engage12V_24V_intosystem@Miscellaneous  { Relay Miscellaneous 1 $1; }
function Preionization-Heater@Miscellaneous  { Relay Miscellaneous 2 $1; }
function Preionization-Shortcut@Miscellaneous  { Relay Miscellaneous 3 $1; }
function Preionization-Engage@Miscellaneous  { Relay Miscellaneous 4 $1; }

# ============ functions@PowerSupplies ============
# 8-a .248 :8D
function Kepcos4PositionStab@PowerSupplies  { Relay PowerSupplies 1 $1; }
function PowerSupplyHVcontactor@PowerSupplies  { Relay PowerSupplies 2 $1; }
function ShortCircuits@PowerSupplies  { Relay PowerSupplies 3 $1; }
function Baking@PowerSupplies  { Relay PowerSupplies 4 $1; }
function GlowDischPS@PowerSupplies  { Relay PowerSupplies 5 $1; }

#source Commons.sh;Call $RelayBoards Baking@PowerSupplies ON

# ============ functions@Permanent ============
# 8-b .254 :59
function Aten@Permanent  { Relay Permanent 4 $1; }
function BlueSockets@Permanent  { Relay Permanent 4 $1; }
function Stop@Permanent  { Relay Permanent 7 $1; }
function TotalStop@Permanent  { Relay Permanent 8 $1; }

# ============ functions@Signalization ============ 
# 8-c .252 :a3
function Zarivky@MiscII  { Relay MiscII 1 $1; }
function Reflektory@MiscII  { Relay MiscII 2 $1; }
function SignalTowerRed@MiscII  { Relay MiscII 8 $1; }
function SignalTowerOrangeGreen@MiscII  { Relay MiscII 7 $1; }


# ============ functions@Racks ============
# 8-g .113 :FA

function Miscellaneous@Racks { Relay Racks 1 $1; }
function Vacuum@Racks { Relay Racks 2 $1; }
function Bt@Racks { Relay Racks 3 $1; }
function Ecd@Racks { Relay Racks 4 $1; }
function TriggerStab@Racks { :; } #N/A

# ============  functions@Vacuum ============ 
#16-a .241 :EC
function Majak@Vacuum { Relay Vacuum 1 $1; }
function GDmeter@Vacuum { Relay Vacuum 2 $1; }
function TMP-bStandby@Vacuum { Relay Vacuum 3 $1; }
function TMP-b@Vacuum { Relay Vacuum 4 $1; }
function MajorVent-b@Vacuum { Relay Vacuum 5 $1; }
function TMP-aStandby@Vacuum { Relay Vacuum 6 $1; }
function TMP-a@Vacuum { Relay Vacuum 7 $1; }
function MajorVent-a@Vacuum { Relay Vacuum 8 $1; }
function MinorVent-a@Vacuum { Relay Vacuum 9 $1; }
function MinorVent-b@Vacuum { Relay Vacuum 10 $1; }
function RotPump@Vacuum { Relay Vacuum 11 $1; }
function H2-pipe@Vacuum { Relay Vacuum 12 $1; }
function He-pipe@Vacuum { Relay Vacuum 13 $1; }

#source Commons.sh;Call $SHMS/Devices/RelayBoards RotPump@Vacuum ON




# ============ functions@Galvanics & PositionStabilization ============ 
# 16-b .233 :A0
# Galvanics
function SocketNo1@Galvanics { Relay Galvanics 1 $1; }
function TektrMSO64-a@Galvanics { SocketNo1@Galvanics "$@"; }
function TektrMSO58-a@Galvanics { Relay Galvanics 2 $1; }
function TektrMSO56-a@Galvanics { Relay Galvanics 3 $1; } #Basic diagnostics
function RigolMSO5204-c@Galvanics { Relay Galvanics 4 $1; } #Charger
function RigolMSO5204-d@Galvanics { Relay Galvanics 5 $1; } #Osciloscope on top of the tG
function GWInstekPSW-b@Galvanics { Relay Galvanics 6 $1; } #Preionization
function Radiometer@Galvanics { Relay Galvanics 7 $1; }
function Biasing@Galvanics { Relay Galvanics 8 $1; } 
#PositionStabilization
function MainSwitch4PositionStabilization { Relay PositionStabilization 12 $1; }
function BlokI4PositionStabilization { Relay PositionStabilization 13 $1; }
function Kepcos@StabilizationON
{
for i in `seq 13 16`; do Relay PositionStabilization $i ON; sleep 2;done
}
function Kepcos@StabilizationOFF
{
for i in `seq 13 16`; do Relay PositionStabilization $i OFF; sleep 2;done
}
#Call $SHMS/Devices/RelayBoards Kepcos4PositionStab@PowerSupplies ON
#Call $RelayBoards Relay PositionStabilization 13 OFF

# MW preion 8-j .250

# ============ functions@Gabo New energetics ============
# Bt-Ecd 16-c .251 :7B
# Under GABO construction (horizon)
#function Rel1@Bt_Ecd { Relay Bt_Ecd 1 $1; }
function Bt-Disc@Bt_Ecd { Relay_ON-OFF Bt_Ecd 1; }
function Bt-ACW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 2; }
function Bt-CW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 3; }

#Budouci reseni az bude Velke rele
#function CBt-Disc@Bt_Ecd { Relay_ON-OFF Bt_Ecd 4; }
#function CBt-Pl@Bt_Ecd { Relay_ON-OFF Bt_Ecd 5; } #Plus
#function CBt-Mn@Bt_Ecd { Relay_ON-OFF Bt_Ecd 6; } #Minus

#Provizorni reseni nez bude Velke rele
function CBt-Disc@Bt_Ecd { Relay Bt_Ecd 4 $1; }

function Ecd-Disc@Bt_Ecd { Relay_ON-OFF Bt_Ecd 7; }
function Ecd-ACW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 8; }
function Ecd-CW@Bt_Ecd { Relay_ON-OFF Bt_Ecd 9; }

#10 a 11 volne

function Trig-act@Bt_Ecd { Relay Bt_Ecd 12 $1; }
function HV_Bt-Commut@Bt_Ecd { Relay Bt_Ecd 13 $1; }
function HV_Bt-Engage@Bt_Ecd { Relay Bt_Ecd 14 $1; }
function LV_Bt-Engage@Bt_Ecd { Relay Bt_Ecd 15 $1; }
function HV_Ecd-Engage@Bt_Ecd { Relay Bt_Ecd 16 $1; }
function LV_Ecd-Engage@Bt_Ecd { Relay Bt_Ecd 17 $1; }
function HV_Ebd-Engage@Bt_Ecd { Relay Bt_Ecd 18 $1; }
function LV_Ebd-Engage@Bt_Ecd { Relay Bt_Ecd 19 $1; }
function LV-Engage@Bt_Ecd { Relay Bt_Ecd 20 $1; }
function Bt-AdditCapac@Bt_Ecd { Relay Bt_Ecd 20 $1; }

# LEDstrips 16-d .239 :35
#


# ============ functions@Electrostatic probes ============

# ElectrostaticProbes 16-e .245 :CC
#
function Relay@ElectrostaticProbes { Relay ElectrostaticProbes $1 $2; }
function AllRelaysOFF@ElectrostaticProbes { for i in `seq 1 16`; do Relay ElectrostaticProbes $i OFF;mRelax; done }
function Voltage@ElectrostaticProbes { request=$1;V9bat=9;for i in `seq 1 $((request/V9bat))`; do Relay ElectrostaticProbes $i ON;mRelax; done  }
function RelaysON@ElectrostaticProbes { for i in `seq 1 $1`; do Relay ElectrostaticProbes $i ON;mRelax; done  }
function VoltagePOS@ElectrostaticProbes { Relay ElectrostaticProbes 11 ON; Relay ElectrostaticProbes 13 ON; }
function VoltageNEG@ElectrostaticProbes { Relay ElectrostaticProbes 11 OFF; Relay ElectrostaticProbes 13 OFF; }


#@Dg:
#cd /golem/Dirigent;source Commons.sh
#cd /golem/Dirigent/Devices/RelayBoards/
#source  RelayBoards.sh;
#VoltageNEG@ElectrostaticProbes #Tady Petr meri Isat Ionovy nasyceny proud, chvost VA charakteristiky
#RelaysON@ElectrostaticProbes 3 #MAX 10
#AllRelaysOFF@ElectrostaticProbes
#VoltagePOS@ElectrostaticProbes
#RelaysON@ElectrostaticProbes 3
#AllRelaysOFF@ElectrostaticProbes




#Development issues
####################
#e.g. Call/DelayCall $SW/Devices/RelayBoards ShortCircuits@PowerSupplies OFF
#source Commons.sh;Call $SHMS/Devices/RelayBoards <<Copy/Paste>> ON/OFF

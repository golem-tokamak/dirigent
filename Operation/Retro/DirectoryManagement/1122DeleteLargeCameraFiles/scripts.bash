function Preparation
{
    for i in `seq 39401 40200`; do dir=/golem/shots/$i/Diagnostics/FastCameras/; if [[ -d $dir ]]; then echo -ne $i:" " ; du -s $dir;fi;done|tee /tmp/du_cameras; echo "*************SORTED*************";sort -k2 -n /tmp/du_cameras|awk '{printf "%d  #%d  %.1f kB\n", NR, $1, $2/1000}'|tee /tmp/du_cameras_sorted ;scp /tmp/du_cameras_sorted svoboda@bn:
}

function Action #@Dg
{
    for i in `awk '{print $2}' du_cameras_sorted_selected`; do  rm /golem/shots/$i/Diagnostics/FastCameras/Camera_*/Data.*;done
}

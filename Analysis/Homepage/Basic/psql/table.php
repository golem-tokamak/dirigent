<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
        body{
            padding: 50px;
        }
        td{
        padding:10px;
        margin: auto;
        }
        table, th, td {
        border: 1px solid black;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </head>
  <body>
  <div style="display: grid; grid-template-columns: repeat(2,2fr);justify-content: center;">
  <div style="text-align:center; margin-top:100px;">
    <?php
        // shot_no, timestamp, who, valid, comment, confirmed, done
        $date_now =  date("y-m-d");
        $items = array('Ip_mean','U_loop_mean','t_plasma_duration');
        $periods = array('Variable', 'Day', 'Month', 'Year', 'All');

        // create select table
        echo "<table style='margin: auto; width: 50%;' class='table table-hover table-responsive table-sm'>";
        echo "<tr>";
        foreach($periods as $period){
            echo "<th>".$period."</th>";
        }
        $row = 0;
        foreach($items as $item){
            echo "<tr>";
            $col = 0;
            for($i=0; $i<sizeof($periods); $i++ ){
                echo "<td>";
                if($col == 0){echo $item;}
                else{
                echo <<<EOD
                    <form method=POST>
                    <input name="data[]" type="hidden" value="$item">
                    <input name="data[]" type="hidden" value="$periods[$col]">
                    <input name="data[]" type="hidden" value="ASC">
                    <input name="data[]" type="hidden" value="$periods[$col]">
                    <button> ASC </button>
                    </form>
                    <form method=POST>
                    <input name="data[]" type="hidden" value="$item">
                    <input name="data[]" type="hidden" value="$periods[$col]">
                    <input name="data[]" type="hidden" value="DESC">
                    <input name="data[]" type="hidden" value="$periods[$col]">
                    <button> DESC </button>
                    </form>
                    EOD;
                }
                echo "</td>";
                $col = $col+1;
            }
            echo "</tr>";
        }
        echo "</table>";

    ?>
    </div>

    <div style="text-align:center">
    <?php
    // connect to databse part
    $host        = "host = 127.0.0.1";
    $port        = "port = 5432";
    $dbname      = "dbname = golem_database";
    $credentials = "user = golem password=rabijosille";

    // catch the data from database
    if(isset($_POST["data"])) {
        $signal = $_POST["data"][0];
        $timePeriod = $_POST["data"][1];
        $ASC_DESC = $_POST["data"][2];
        $time_select = $_POST["data"][3];
        
        // create day string for filtering the data by day, week (TODO), month and year
        $day_string = "";
        switch($time_select){
            case "Day":
                $day_string = date("y-m-d")." %";
                break;
            case "Week":
                $day_string = date("y-m-")."%";
                break;
            case "Month":
                $day_string = date("y-m-")."%";
                break;
            case "Year":
                $day_string = date("y-")."%";
                break;
            default:
                $day_string = "%";
                break;
        }


//         echo "You have chosen signal $signal and period $timePeriod";
// DATABASE operations ---------------------
// the golem database is connected in this section, querries are send
        $db = pg_connect( "$host $port $dbname $credentials");
        $querry = <<<EOF
            SELECT shot_no,"X_discharge_command","$signal" FROM operation.discharges WHERE "$signal" IS NOT NULL AND shot_no >= 35000 AND "start_timestamp" LIKE '$day_string' AND CAST("$signal" AS NUMERIC) > 0 ORDER BY "$signal" $ASC_DESC LIMIT 50
        EOF;
//         echo "<br>". $querry."<br>";
        $ret = pg_query($db, $querry);
        pg_close();
        $outputs = array();
        $shots = array();
        while ($row = pg_fetch_array($ret)) {
            $shots[] = $row[0];
            $outputs[] = $row[2];
            }

        // The right hand side of webpage including psql command and figures
        $url = 'http://golem.fjfi.cvut.cz/shots/';
        echo "<h1> You have chosen signal $signal and period $timePeriod</h1>";
        echo "(psql command: ".$querry.")";
        echo "<div id='form_div'></div>";
        echo "<table style=' margin:auto;'>";
        for($i=0; $i<sizeof($shots); $i++){
            $outputs_rounded = round($outputs[$i], 3);
            if($shots[$i] >= 40421){
                echo <<<EOF
                <tr><th><input form='select_form' type='checkbox' value='$shots[$i]' name=shots[] id='$shots[$i]'> </input><a href='http://golem.fjfi.cvut.cz/shots/$shots[$i]'>$shots[$i]</a>, $signal = $outputs_rounded</th></tr>
                <tr><td><label for='$shots[$i]'><img width=500 src='http://golem.fjfi.cvut.cz/shots/$shots[$i]/Diagnostics/BasicDiagnostics/Basic/icon-fig.png'></img></label></td></tr>
                EOF;
            }
            else{
                 echo <<<EOF
                <tr><th><input form='select_form' type='checkbox' value='$shots[$i]' name=shots[] id='$shots[$i]'> </input><a href='http://golem.fjfi.cvut.cz/shots/$shots[$i]'>$shots[$i]</a>, $signal = $outputs_rounded</th></tr>
                <tr><td><label for='$shots[$i]'><img width=500 src='http://golem.fjfi.cvut.cz/shots/$shots[$i]/Diagnostics/BasicDiagnostics/icon-fig.png'></img></label></td></tr>
                EOF;               
            }
        }

         echo '</table>';
         // if(isset($_SESSION['user'])){$user_name = $_SESSION['user'];}
         // else{$user_name = '';}
        echo <<<EOF
            <form id='select_form' method=POST>
                <input type='text' name='user' class="form-control" placeholder='User' style="width: 20%; margin: 0 auto;"> </input>
                <input type='text' name='comment' class="form-control" placeholder='Comment' style="width: 20%; margin: 0 auto;"> </input>
                <select name='validity' class="form-select" style="width: 25%; margin: 0 auto;"> 
                    <option value="" disabled selected hidden>Choose reasson</option>
                    <option value='wrong time'>Wrong time</option>
                    <option value='invalid data'>Invalid data</option>
                </select>
                <button> submit </button>
            </form>
            <input type='button' value='Select All' id='selectAllID' onclick='selectAll()'></input>
            <input type='button' value='deSelect All' id='deselectAllID' onclick='deselectAll()'></input>
        EOF;
    }

    // Database part - here the data are passed into requests table
    if(isset($_POST["shots"]) and isset($_POST["user"]) and isset($_POST["comment"]) and isset(
        $_POST["validity"]) and !empty($_POST["shots"]) and !empty($_POST["user"]) and !empty($_POST["comment"]) and !empty(
        $_POST["validity"])) {
        $user = $_POST['user'];
        // if(!isset($_SESSION['user'])){$_SESSION['user'] = $user;}
        $comment = $_POST['comment'];
        $validity = $_POST['validity'];
        foreach($_POST["shots"] as $shot){
            $shot_date = date("y-m-d H:i:s");
            $db = pg_connect( "$host $port $dbname $credentials");
            $test_querry = <<<EOF
                SELECT shot_no FROM production.requests WHERE shot_no='$shot'
            EOF;
            $test_ret = pg_query($db, $test_querry);
            if(pg_num_rows($test_ret) == 0){
                $querry = <<<EOF
                    INSERT INTO production.requests (shot_no,timestamp,who,valid,comment,confirmed,done) VALUES ($shot,'$shot_date','$user','$validity','$comment',FALSE,FALSE)
                EOF;
                $ret = pg_query($db, $querry);
                echo "New report was added into psql table!<br>";
            }
            else{
                $querry = <<<EOF
                    UPDATE productionrequests SET comment='$comment' WHERE shot_no='$shot'
                EOF;
                $ret = pg_query($db, $querry);      
                echo "The comment to discharge ".$shot." was edited<br>";        
            }
        }
        pg_close();
    }
//     else{
//         echo "Some input missing! Please fill all forms.";
//     }
    ?>
    </div>
    </div>
  </body>
  <script>
        $("#select_form").appendTo("#form_div");
        $("#selectAllID").appendTo("#form_div");
        $("#deselectAllID").appendTo("#form_div");
</script>
<script>
    function selectAll(){
        var ele = document.getElementsByName('shots[]');
        for(var i = 0; i<ele.length; i++){
            if(ele[i].type == 'checkbox'){
                ele[i].checked = true;
            }
        }
    }
    function deselectAll(){
        var ele = document.getElementsByName('shots[]');
        for(var i = 0; i<ele.length; i++){
            if(ele[i].type == 'checkbox'){
                ele[i].checked = false;
            }
        }
    }
</script>
</html>

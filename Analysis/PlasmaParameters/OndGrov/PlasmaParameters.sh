BasePath=../../..;source $BasePath/Commons.sh

whoami="Analysis/PlasmaParameters/OndGrov/PlasmaParameters"



function PostDischargeAnalysis() 
{
    #export SHOT_NO=`cat ../../shot_no` #linux only
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" `basename $whoami`.ipynb
    mkdir Results
    jupyter-nbconvert  --ExecutePreprocessor.timeout=30 --to html --execute `basename $whoami`.ipynb --output analysis.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

    convert -resize $icon_size icon-fig.png graph.png
    
    GenerateAnalysisWWWs $whoami # @Commons.sh



}


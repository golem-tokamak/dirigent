from dataclasses import dataclass
import typing
from typing import Union
from typing import List, Dict
import shlex
import yaml
from decimal import Decimal
from collections import OrderedDict
import re
import pyparsing as pp
import os
from pathlib import Path
import logging
#import importlib_resources

#this_package_path = importlib_resources.files(__name__)
this_package_path = Path('/golem/Dirigent/Operation/WWWs/remote_app/golem_web_app/dirigent_parser')


ALL_INSTRUMENTS_PATH = '/golem/shm_golem/Production/AllInstruments'
DIAGNOSTICS_ONSTAGE = '/golem/shm_golem/Production/Diagnostics_OnStage'
DIAGNOSTICS_OFFSTAGE = '/golem/shm_golem/Production/Diagnostics_OffStage'
INFRASCTRUCTURE = '/golem/shm_golem/Production/Infrastructure'
ACTUAL_SESSION_PATH = '/golem/shm_golem/ActualSession/'


## function from later python version
def removeprefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    else:
        return text

@dataclass(init=False)
class DischargeSubParameter:
    name: str = None
    type: str = None
    known: bool = False
    full_id_name : str = None
    title: str = None
    comment: str = None

    default: str = None

    parsed_value: str = None
    new_value: Union[None, str] = None

    unit: Union[None, str] = None
    
    # by default always show parameter
    use_from_level : int = 1
    
    # to be used by Flask app
    hidden :bool = False
    
    
    @property
    def value(self):
        if self.new_value is not None:
            return self.new_value
        else:
            return self.parsed_value
    

    _type = str

    def __init__(self, value: str, system_name: str, helper=None, name =  None):
        self.parsed_value = self._type(value)

        if helper is not None:
            parse_parameter_options(self, helper, self._type)
            self.known = True
            self.full_id_name = system_name+'_'+self.name if self.name else system_name
            
        else:
            
            self.name = name
            self.full_id_name = system_name+'_'+name if name else system_name 
            
        self.full_id_name = self.full_id_name.replace('.','__')
        
        if self._type == str:
            self.parsed_value = self.parsed_value.replace("'", "")


@dataclass(init=False)
class IntegerParameter(DischargeSubParameter):
    default: int
    parsed_value: int
    new_value: Union[None, int] = None

    min: int = 0
    max: int = 1000
    step: int = 1

    _type = int


@dataclass(init=False)
class FloatParameter(DischargeSubParameter):
    default: Decimal
    parsed_value: Decimal
    new_value: Union[None, Decimal] = None

    min: Decimal = Decimal(0)
    max: Decimal = Decimal(1000)
    step: Decimal = Decimal(0.1)

    _type = Decimal


@dataclass(init=False)
class OptionParameter(DischargeSubParameter):
    options: List[str]
    options_alt_text: List[str] = None

@dataclass(init=False)
class WaveformParameter(DischargeSubParameter):
    options: List[str]
    options_alt_text: List[str] = None
    

@dataclass(init=False)
class ShotNoParameter(DischargeSubParameter):
    default: int
    parsed_value: int
    new_value: Union[None,int] = None

    shot_no_filter: List[str] = None

    _type = int

    def __init__(self, value: str, system_name: str, helper=None):
        super().__init__(value, system_name,helper)

        match = re.search(r"shot_no\[(.+)\]", self.type)
        if match:
            self.shot_no_filter = match.group(1).split(",")

        self.type = "shot_no"


@dataclass(init=False)
class BinaryParameter(DischargeSubParameter):
    length: int

    def __init__(self, value: str,system_name: str, helper=None):
        super().__init__(value,system_name, helper)

        match = re.search(r"binary\[(\d+)\]", self.type)
        if match:
            self.length = int(match.group(1))
        else:
            self.length = 12
            
        self.type = "binary"

@dataclass(init=False)
class TernaryParameter(DischargeSubParameter):
    length: int

    def __init__(self, value: str,system_name: str, helper=None):
        super().__init__(value,system_name, helper)

        match = re.search(r"ternary\[(\d+)\]", self.type)
        if match:
            self.length = int(match.group(1))
        else:
            self.length = 12
            
        self.type = "ternary"

@dataclass
class DescriptionFiles():
    has_markdown : bool = False
    has_img : bool = False
    
    markdown_path : Path = None
    img_path : Path = None


@dataclass
class DischargeSubSystem:
    command: str
    title: str = ""
    parameters: typing.OrderedDict[str, DischargeSubParameter] = None
    is_parameter : bool = False

    raw_params: str = ""
    known :bool = False
    # use post_init for DefaultBehaivour ?? https://stackoverflow.com/questions/51564841/creating-nested-dataclass-objects-in-python
    description: DescriptionFiles = None

    use_from_level : int = 1
    
    # to be used by Flask app
    hidden :bool = False

    def __getitem__(self, key):
        return self.parameters[key]

    # def __setitem__(self, key, value):
    #     # ToDo: add type conversion
    #     self.parameters[key].parsed_value = value
    
    def assemble_parameters(self) -> str:
    
        if self.parameters is None or len(self.parameters) == 0:
            return ""
        
        if self.is_parameter:
            parameter = first(self.parameters)
            if parameter.new_value is not None:
                return f"\"{parameter.new_value}\""  
            else:
                return f"\"{parameter.parsed_value}\""
        
        params_str = "\""
        for index, (param_name, parameter) in enumerate(self.parameters.items()):
            params_str += f"{param_name}="
            
            
            if parameter.type in ['int', 'float']:
                params_str += str(parameter.new_value) if parameter.new_value is not None else str(parameter.parsed_value)
            else:
                raw_param = str(parameter.new_value) if parameter.new_value is not None else str(parameter.parsed_value)
                params_str += f"\'{raw_param}\'"
                
            if index != len(self.parameters)-1:
                params_str += ","
        params_str += "\""
        
        return params_str
    
    def assemble_subsystem(self) -> str:
        command = f" --{self.command} "
        command+= self.assemble_parameters()
        
        return command
    
    def __str__(self) -> str:
        return self.assemble_subsystem()
    
    @property
    def safe_name(self) -> str:
        return self.command.replace('.','')
        


@dataclass
class DischargeCommand:
    raw_command: str
    subsystems: typing.OrderedDict[str, DischargeSubSystem]

    def __getitem__(self, key):
        return self.subsystems[key]
    
    def parse_command(self):
        pass
    
    def assemble_command(self, human_readable = True):
        command = " ./Dirigent.sh " + self.assemble_subsystems(human_readable)
        return command
    
    def assemble_subsystems(self, human_readable = True) -> str:
        command = ""
        for index, subsystem in enumerate(self.subsystems.values()):
            command+= f"{subsystem.assemble_subsystem()}"
            if human_readable and index != len(self.subsystems)-1:
                command+="\n"
        return command
        
    def __str__(self) -> str:
        return self.assemble_command()


def is_valid_helper(helper: Dict) -> bool:
    try:
        if 'command' not in helper:
            logging.warning('%r is missing "command" field', helper)
            return False
        if 'parameters' in helper:
            if len(helper['parameters']) > 0:
                for parameter in helper['parameters']:
                    if 'name' not in parameter:
                        logging.warning('%r has parameter without "name" field', helper)
                        return False
                    if 'default' not in parameter:
                        logging.warning('%r no "default" value for parameter %s', helper, parameter['name'])
                        return False
    except Exception:
        logging.warning('cannot check validity of %r', helper)
        return False
        
    return True

def read_yaml(fname):
    yaml_list = list()
    
    # TD ensure validity of parsed yaml
    
    if not os.path.isfile(fname):
        #logging.warning('Canot read %s not a file', fname)
        return yaml_list
    
    with open(fname, "r") as file:
        yaml_list = list(yaml.load_all(file, Loader=yaml.FullLoader))
        #logging.warning('Read %s', fname)
        #logging.warning('Content %s', yaml_list)
    return yaml_list


def get_default_options():
    default_discharge_options = read_yaml(this_package_path.joinpath("dirigent_params.yml"))
    return default_discharge_options

def read_defaults_from_csv(path, opt):
    if os.path.isfile(path):
        with open(path,'r') as default_params_file:
            default_params_str = default_params_file.read(4098)
            if "parameters" in opt.keys():
                default_params = parse_parameters(default_params_str, opt['parameters'], '')
                for param in opt['parameters']:
                    name = param['name']
                    if name in default_params:
                        param['default'] = default_params[name].value
                
            if "type" in opt.keys():
                parameter = get_parameter(default_params, helper=opt, system_name='')
                opt['default'] = parameter.value

def get_session_config():
    
    if not os.path.isfile(ALL_INSTRUMENTS_PATH):
        return None
    #session_config = read_yaml("dirigent_params.yml")
    
    session_config = read_yaml(this_package_path.joinpath("pre_dirigent_params.yml"))
    
    # # # traverse Diagnoscits / Infrasctucture folders
    # # # and load yaml files with config
    
    instruments_folders = []
    with open(ALL_INSTRUMENTS_PATH, 'r') as instrument_list_file:
        instruments_folders = instrument_list_file.read().split()
        
        # Check for Discharge. To avoid ChamberCondicioning setups
        if not any('Operation/Discharge' in instrument for instrument in instruments_folders):
            return None
    
    #with open(DIAGNOSTICS_ONSTAGE, 'r') as instrument_list_file:
    #    diagnostics_onstage_folders = instrument_list_file.read().split()
    
    if os.path.isfile(DIAGNOSTICS_OFFSTAGE):    
        with open(DIAGNOSTICS_OFFSTAGE, 'r') as instrument_list_file:
            diagnostics_offstage_folders = instrument_list_file.read().split()
            diagnostics_offstage_folders = ['Diagnostics/'+d for d in diagnostics_offstage_folders]
    else:
        diagnostics_offstage_folders = []
    
    if os.path.isfile(INFRASCTRUCTURE):    
        with open(INFRASCTRUCTURE, 'r') as instrument_list_file:
            infrasctructure_folders = instrument_list_file.read().split()
            infrasctructure_folders = ['Infrastructure/'+i for i in infrasctructure_folders]
    else:
        infrasctructure_folders = []
        
    
    for instrument in instruments_folders:
        pre_path = os.path.join(ACTUAL_SESSION_PATH,instrument)
        folder_path = Path(pre_path).parent
        path = folder_path / 'params.yml'
        helper = read_yaml(path)
        
        if len(helper) == 1:
            # unpack helper from list
            opt = helper[0]
            
            if not is_valid_helper(opt):
                logging.warning(f'{path} is not valid param file ... skipping')
                continue
            
            # try to parse .default file            
            try:
                default_params_file_path = folder_path / f"{opt['command']}.default"
                read_defaults_from_csv(default_params_file_path, opt)
            except Exception as e:
                # to be safe re-read yaml file on exption
                helper = read_yaml(path)
                opt = helper[0] 
        
            ## if not set in helper use on stage diagnosctis from level 2
            ## and offstage diagnostics from level 3
            if 'use_from_level' not in opt:
                if instrument not in infrasctructure_folders or instrument not in diagnostics_offstage_folders:
                    opt['use_from_level'] = 2
                if instrument in diagnostics_offstage_folders:
                    #logging.warning(f' Parser {instrument} in offstage -> level 3')
                    opt['use_from_level'] = 3
                   
            # add description.md file if exists
            opt['description'] = DescriptionFiles()
            description_path = folder_path / "description.md" 
            
            if os.path.isfile(description_path):
                opt['description'].has_markdown = True
                opt['description'].markdown_path = description_path.relative_to(ACTUAL_SESSION_PATH)
            
            image_path = folder_path / opt['image_name'] if 'image_name' in opt else folder_path / "setup.png" 
            if os.path.isfile(image_path):
                opt['description'].has_img = True
                opt['description'].img_path = image_path.relative_to(ACTUAL_SESSION_PATH)
            
            logging.debug(f'adding {helper} to session_config')    
            session_config+=helper
            
        elif len(helper) == 0:
            logging.debug(f'{path} has not returned helper')
        else:
            logging.warning(f'{path} has multiple helpers ... skipping')
     
    session_config+= read_yaml(this_package_path.joinpath("post_dirigent_params.yml"))

    return session_config


def get_basic_config():
    try:  
        session_config = read_yaml(this_package_path.joinpath("pre_dirigent_params.yml"))
        
        
        # load only instrumenst needed for simple discharge control room
        
        ## TD using basic_config for simple control room and as fallback 
        ## for session config results in double reading same file this should be avoided 
        
        for instrument in ["Infrastructure/Bt_Ecd", "Infrastructure/WorkingGas", "Infrastructure/Preionization"]:
            path_prefix = ACTUAL_SESSION_PATH
            path = path_prefix +  instrument + "/params.yml"
            helper = read_yaml(path)
            if len(helper) == 1:
                # unpack helper from list
                opt = helper[0]
                if not is_valid_helper(opt):
                    logging.warning(f'{path} is not valid param file ... will use fallback')
                    continue
                session_config+= helper
            else:
                logging.warning(f'no heleper or multiple in {path}  ... will use fallback')
        
        
        # use predefined fallback on error / some subcommmand missing
        loaded_subcommands = get_recognized_sub_commands(session_config)
        to_add_subcommand = list()
        for req_subcommand in ['infrastructure.workinggas', 'infrastructure.preionization',
            'infrastructure.bt_ecd']:
            
            if req_subcommand not in loaded_subcommands:
                to_add_subcommand.append(req_subcommand)
            
            fallback_session_config = read_yaml(this_package_path.joinpath("simple_dirigent_params.yml"))
            for sub_command in fallback_session_config:
                if sub_command['command'] in to_add_subcommand:
                    logging.warning(f' Parser adding {sub_command["command"]} from fallback -- config file is missing or is corrupted')
                    session_config.append(sub_command)
    
        # at last add discharge comment
        session_config+= read_yaml(this_package_path.joinpath("post_dirigent_params.yml"))

    except Exception:
        logging.warning('Parser - Loading last resort fallback discharge configuration')
        session_config = read_yaml(this_package_path.joinpath("simple_dirigent_params.yml"))
    
    return session_config

def get_basic_config_and_add_session():
    basic_config = get_basic_config()
    session_config = get_session_config()
    
    if session_config is None:
        return basic_config
    
    basic_config_subsys = set([s['command'] for s in basic_config])
    session_config_subsys = set([s['command'] for s in session_config])
    
    missing = session_config_subsys.difference(basic_config_subsys)
    
    for helper in session_config:
        if helper['command'] in missing:
            basic_config.append(helper)

    return basic_config         
    
# as default use configuration for simple control room
default_discharge_options = get_basic_config()


def get_recognized_sub_commands(discharge_options = default_discharge_options):
    recognized_sub_commands = list()
    for opt in discharge_options:
        if "command" in opt.keys():
            recognized_sub_commands.append(opt["command"])
            
    return recognized_sub_commands


recognized_defailt_sub_commands = get_recognized_sub_commands()


def load_session_config():
    discharge_options = list()
    with open("dirigent_params.yml", "r") as file:
        discharge_options = list(yaml.load_all(file, Loader=yaml.FullLoader))
        
    return discharge_options
     
     
def is_known_sub_command(name: str, sum_commands_list = recognized_defailt_sub_commands):
    return name in sum_commands_list


def has_options(name: str, discharge_options = default_discharge_options):
    if not is_known_sub_command(name,get_recognized_sub_commands(discharge_options)):
        return False

    for opt in discharge_options:
        if name == opt["command"]:
            return "parameters" in opt.keys() or "type" in opt.keys()
    return False


def parse_parameter_options(parameter, helper, unit_converter=str):

    parameter.type = helper['type']

    if "name" in helper.keys():
        parameter.name = helper['name']
        
    if "title" in helper.keys():
        parameter.title = helper['title']
        
    if "comment" in helper.keys():
        parameter.comment = helper['comment']

    if "default" in helper.keys():
        parameter.default = unit_converter(helper["default"])

    if "min" in helper.keys():
        parameter.min = unit_converter(helper["min"])
    if "max" in helper.keys():
        parameter.max = unit_converter(helper["max"])
    if "step" in helper.keys():
        parameter.step = unit_converter(helper["step"])

    if "options" in helper.keys():
        parameter.options = helper["options"]

    if "options_alt_text" in helper.keys():
        parameter.options_alt_text = helper["options_alt_text"]

    if "unit" in helper.keys():
        parameter.unit = helper["unit"]
        
    if "use_from_level" in helper.keys():
        parameter.use_from_level = helper["use_from_level"]



def get_parameter(value, helper, system_name, name = None):
    if helper is None:
        return DischargeSubParameter(value, system_name, helper=None, name=name)
    
    if helper["type"] == "str":
        return DischargeSubParameter(value,system_name, helper)

    elif helper["type"] == "int":
        return IntegerParameter(value,system_name, helper)

    elif helper["type"] == "float":
        return FloatParameter(value,system_name, helper)

    elif helper['type'] == "switch":
        return OptionParameter(value,system_name, helper)

    elif helper['type'] == "waveform":
        return DischargeSubParameter(value,system_name, helper)

    elif helper['type'].startswith("binary"):
        return BinaryParameter(value, system_name, helper)

    elif helper['type'].startswith("ternary"):
        return TernaryParameter(value, system_name, helper)

    elif helper['type'].startswith("shot_no"):
        return ShotNoParameter(value, system_name, helper)
    
    elif helper['type'] == "comment":
        return DischargeSubParameter(value,system_name, helper)
    
    else:
        # fallback to str conversion
        return DischargeSubParameter(value, system_name,helper)

    raise NotImplementedError(helper['type'])


def get_known_parameters(helper):
    if helper is None:
        return []
    return [p["name"] for p in helper if "name" in p.keys()]


def get_helper_for_param(param_name, helper):
    param_helper = None
    if helper is not None:
        for known_param in helper :
            if param_name.lower() in known_param["name"].lower():
                param_helper = known_param
                break
    return param_helper

def parse_parameters(values, helper, system_name):
    known_parameters = get_known_parameters(helper)
    known_parameters = [p.lower() for p in known_parameters]

    parameters = OrderedDict()
    
    param_parser = pp.Word(pp.alphanums+"_") + '=' + (pp.Word(pp.alphanums + '.+-') | pp.QuotedString("'"))  + pp.Optional(',')
    parsing_result = param_parser.scan_string(values)
    
    
    for item, _,_ in parsing_result:
        if item[0].lower() in known_parameters:
            param_name = item[0]
        else:
            continue
        
        value = item[2]

        param_helper = get_helper_for_param(param_name, helper)

        # if param_helper is None:
        #     raise ValueError(
        #         f"Cannot parse {param_name} {value} in {values}")

        if param_helper is None:
            continue

        param_name = param_helper['name']
        parameter = get_parameter(value, param_helper, system_name, param_name)
        parameters[param_name] = parameter

    return parameters

def disassemble_sub_command_name_and_get_helper(name, discharge_options = None):
    if discharge_options is None:
        discharge_options = default_discharge_options
        
    found = False
    opt = []
    for opt in discharge_options:
        if name == opt["command"]:
            found = True
            break
        
    return found, opt

def add_missing_keys(first_dict : OrderedDict, default_dict: OrderedDict) -> OrderedDict:
    for key, value in default_dict.items():
        if key not in first_dict:
            first_dict[key] = value
    return first_dict


def sort_according_to_template(first_dict: OrderedDict, default_dict: OrderedDict) -> OrderedDict:
    sorted_dict = OrderedDict()
    
    # add keys as they apperar in default_dict to sorted_dict
    ordered_keys = default_dict.keys()
    for key in ordered_keys:
        if key in first_dict:
            sorted_dict[key] = first_dict[key]
    
    # add keys missing from default_dict to the end
    for key in first_dict.keys():
        if key not in sorted_dict:
            sorted_dict[key] = first_dict[key]        
    
    return sorted_dict

def parse_sub_command(name: str, params: str = None, config = None):
    found, opt = disassemble_sub_command_name_and_get_helper(name, config)
    # if not found:
    #     raise ValueError("Unknown command cannot parse is's paramters")

    ## TD: get_sub_command shares shame code 
    ## make get_sub_command and use it in get_sub_command_with_default and parse_sub_command

    kargs={}
    if "title" in opt.keys():
        kargs['title'] = opt["title"]
    if 'use_from_level' in opt:
        kargs['use_from_level'] = opt['use_from_level']
        
    description = opt['description'] if 'description' in opt else DescriptionFiles()
    

    parameters = OrderedDict()
    is_parameter = False
    if "parameters" in opt.keys():
        parameters = parse_parameters(params, opt["parameters"], name)
        default_parameters = get_default_parameters(opt["parameters"], name)

        parameters = add_missing_keys(parameters, default_parameters)
        parameters = sort_according_to_template(parameters, default_parameters)


    if "type" in opt.keys():
        parameter = get_parameter(value=params, helper=opt, system_name=name)
        parameter.name = name
        parameters[""] = parameter
        is_parameter = True
        
    if not found:
        if '=' in params:
                parameters = parse_parameters(params, None, name)
        else:
            parameters[""] = DischargeSubParameter(params, name, None, '')
            

    return DischargeSubSystem(
        command=name,
        parameters=parameters,
        raw_params=params,
        known=found,
        is_parameter=is_parameter,
        description = description,
        **kargs
    )


def get_default_parameters(helper, system_name):
    params = OrderedDict()
    for param_name in get_known_parameters(helper):
        param_helper = get_helper_for_param(param_name, helper)
        param = get_parameter(value=param_helper['default'], helper=param_helper, system_name=system_name)
        params[param_name] = param
        
    return params

def get_sub_command(name, config = None):
    found, opt = disassemble_sub_command_name_and_get_helper(name, config)
                                                          
    if not found:
        raise ValueError(f"Sub-Command '{name}' not recognised")
    
    
    kargs={}
    if "title" in opt.keys():
        kargs['title'] = opt["title"]
    if 'use_from_level' in opt:
        kargs['use_from_level'] = opt['use_from_level']
        
    description = opt['description'] if 'description' in opt else DescriptionFiles()

    parameters = OrderedDict()
    is_parameter = False
    if "parameters" in opt.keys():
        parameters =  get_default_parameters(opt["parameters"], name)

    if "type" in opt.keys():
        parameter = get_parameter(value=opt['default'], helper=opt, system_name=name)
        parameter.name = name
        parameters[""] = parameter
        is_parameter = True
        

    return DischargeSubSystem(
        command=name,
        parameters=parameters,
        raw_params='',
        known=found,
        is_parameter=is_parameter,
        description=description,
        **kargs
    )



def parse_command(dirigent_command_raw: str, use_config = None):
    tokens = shlex.split(dirigent_command_raw)
    recognized_sub_commands = get_recognized_sub_commands(use_config)
    
    discharge_sub_commands = OrderedDict()
    for i, token in enumerate(tokens):
        if token.startswith("--"):
            token = removeprefix(token,"--")
            sub_comand_name = token
            if is_known_sub_command(token,recognized_sub_commands):
                if has_options(token,use_config):
                    try:
                        params = tokens[i + 1]
                    except Exception:
                        params = ""

                else:
                    params = None
                
                sub_command = parse_sub_command(sub_comand_name, params, use_config)            
                discharge_sub_commands[sub_comand_name] = sub_command
                
            else:
                # do not handle unknown subcommnad
                pass
                

                # try:
                #     params = tokens[i + 1]
                # except Exception:
                #     params = ""

                # sub_command = parse_sub_command(sub_comand_name, params)            
                # discharge_sub_commands[sub_comand_name] = sub_command

    
    parsed_discharge_command = DischargeCommand(
        raw_command=dirigent_command_raw,
        subsystems=discharge_sub_commands,
    )
    
    if use_config is not None:
        use_sub_commands = get_recognized_sub_commands(use_config)
        added_sub_commands = dict()
        sub_commands_to_add = [ sub_command for sub_command in use_sub_commands 
                                    if sub_command not in parsed_discharge_command.subsystems.keys()]
        for sub_command in sub_commands_to_add:    
            added_sub_commands[sub_command] = get_sub_command(sub_command, use_config)
        
        ## force order of subcommands    
        all_subsystems = OrderedDict()
        for sub_commnad_name in use_sub_commands:
            if sub_commnad_name in parsed_discharge_command.subsystems:
                all_subsystems[sub_commnad_name] = parsed_discharge_command.subsystems[sub_commnad_name]
            else:
                all_subsystems[sub_commnad_name] = added_sub_commands[sub_commnad_name]

    
        parsed_discharge_command.subsystems = all_subsystems
    
    return parsed_discharge_command



def first(ordered_dict):
    for item in ordered_dict.values():
        return item
        

def main():
    test_discharge_cmd = """\
    ./Dirigent.sh \ 
    --discharge --operation.discharge "style='standard'" \ 
    --infrastructure.bt_ecd "U_Bt=1200,t_Bt=0,O_Bt='CW',U_cd=500,t_cd=1000,O_cd='CW'" \ 
    --infrastructure.preionization "S_device='HotCathode',SW_main='off',U_heater=0,U_accel=0" \ 
    --infrastructure.workinggas "S_mode='U_v',S_gas='H',p_H=15,U_v=22.5" \ 
    --infrastructure.positionstabilization "main_switch='on',radial_switch='on',radial_waveform='1000,0;2000,-5;5000,-10;9000,-20;30000,0,',vertical_switch='on',vertical_waveform='1000,0;2000,-5;7000,-10;8000,-20;30000,0'" \ 
    --diagnostics.rakeprobe "r_First_Tip=100,phi_First_Tip=0,X_mod='111111111111',I_SilverBox=12,r_manipulator=60,phi_manipulator=0" \ 
    --diagnostics.scintillationprobes "U_NIM_A1=600,U_NIM_A2=600,U_NIM_A3=600,U_NIM_A4=600" \ 
    --comment 'Normal discharge'
    """
    #test_discharge_cmd = ''' ./Dirigent.sh --discharge --operation.discharge "style='NotSpecified'" --infrastructure.preionization "S_device='HotCathode',SW_main='on',U_heater=100,U_accel=70" --infrastructure.workinggas "S_mode='U_v',S_gas='H',p_H=10,U_v=22" --infrastructure.positionstabilization "main_switch='on',radial_switch='on',radial_waveform='1000,0;2000,-5;5000,-10;9000,-20;30000,0',vertical_switch='on',vertical_waveform='1000,0;2000,-5;7000,-10;8000,-20;30000,0'" --infrastructure.bt_ecd "U_Bt=800,t_Bt=0,o_Bt='ACW',U_cd=500,t_cd=1000,O_cd='ACW'"  --diagnostics.limitermirnovcoils "vacuum_shot=-1" --comment "Default discharge" '''
    command = parse_command(test_discharge_cmd, use_config=get_session_config())
    print("%r" % command)
    print('-------')
    print(str(command))

if __name__ == "__main__":
    main()

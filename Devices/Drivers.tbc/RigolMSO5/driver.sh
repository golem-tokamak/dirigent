#!/bin/bash

#access: Rigol admin:rigol
source ../Commons.sh

function SendCommandToTheDevice()
{
    echo $1|netcat -w 1 $TheOscilloscope 5555
}


function LoadSetup()
{
    SendCommandToTheDevice ":LOAD:SETup $1"
}

function GetOscScreenShot()
{
	SendCommandToTheDevice ":SYSTem:KEY:PRESs MOFF"
	mRelax
    SendCommandToTheDevice ":DISPLAY:DATA?  ON,OFF,PNG"|tail -c +12
}

function DVMDisable()
{
    SendCommandToTheDevice ":STOP;:DVM:ENABle OFF"
}


function OpenOscDVMchannel()
{
    SendCommandToTheDevice ":DVM:ENABle ON;:DVM:SOURce CHANnel$1;DVM:MODE DC"
    mRelax
    SendCommandToTheDevice ":STOP;:CLEAR;:TRIGger:SWEEp AUTO;RUN"
    mkdir -p /dev/shm/$TheOscilloscope
    touch /dev/shm/$TheOscilloscope/OscRequestStream
    tail -f /dev/shm/$TheOscilloscope/OscRequestStream|netcat -N $TheOscilloscope 5555 >> /dev/shm/$TheOscilloscope/OscRespondStream &
}

# while [ 1 ]; do  ./Charger.sh -r ReadDVMOscilloscope ;sleep 1;done
function ReadDVMOscilloscope()
{
    echo ":SYSTem:TIME?">> /dev/shm/$TheOscilloscope/OscRequestStream;mRelax
    echo ":DVM:CURRENT?">> /dev/shm/$TheOscilloscope/OscRequestStream;mRelax
    echo `tail -1 /dev/shm/$TheOscilloscope/OscRespondStream|xargs printf '%4.0f\n'`
}


function ReadOscilloscope()
{
# $1: Osc name
# $2: Channel #
    
    echo ":SYSTem:TIME?">> /dev/shm/$1/OscRequestStream;mRelax
    echo ":MEAS:ITEM? VAVG, CHANNEL$2">> /dev/shm/$1/OscRequestStream
    echo `tail -1 /dev/shm/$1/OscRespondStream|xargs printf '%4.0f\n'`
}



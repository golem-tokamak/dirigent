from=44293
to=44325
soubor=index.html; 
url=https://golem.fjfi.cvut.cz/shots/
t_plasma_duration_limit=7
title='Fast cameras inventory March, 27'
#url=/shots

echo "<html><body>
<h1>$title</h1>
<h2>From #$from to #$to </h2>
<table>" > $soubor;
echo "<tr><th>ShotNo</th><th>Basic diagnostics</th><th>Fast cameras</th><th>Motion detection (south)</th><th>Motion detection (north)</th></tr>" >> $soubor;
Counter=0
for i in `seq $from $to`; do echo -ne '*'$i:; 
    let Counter++ #increment
    echo -ne yes';'
    echo "<tr>
    <td><a href=$url/$i>$i</a><br>`cat /golem/shots/$i/shot_date`<br>`cat /golem/shots/$i/shot_time`</td>
    <td><img src=$url/$i/Diagnostics/BasicDiagnostics/graph.png></td>
    <td><img src=$url/$i/Devices/AVs/PhotronMiniUX100-a/rawdata.jpg></td>
    <td><img src=$url/$i/Diagnostics/FastCameras/Camera_South/1s.jpeg width=120></td>
    <td><img src=$url/$i/Diagnostics/FastCameras/Camera_North/1s.jpeg width=120></td>
    </tr>" >> $soubor;
done
echo "</table>Alltogether $Counter discharges<hr/>
<h2>For generation bash script used</h2>
<pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;




#/bin/sh

function comm_exec_rvc300gnome232() {
    python3 -c "from vacuum.rvc300_gnome232 import RVC300GNOME232; ret = RVC300GNOME232('192.168.2.239', retries=5).$1"
}


function get_rvc300_pressure() {
    # use retries=2, so other commands have larger 'priority'
    python3 -c "from vacuum.rvc300_gnome232 import RVC300GNOME232; ret = RVC300GNOME232('192.168.2.239', retries=2).get_pressure(); print(ret)"
}

function set_pressure() {
    comm_exec_rvc300gnome232 "setpoint_mPa($1)"
}


function open() {
    comm_exec_rvc300gnome232 "open()"
}

function close() {
    comm_exec_rvc300gnome232 "close()"
}

function set_regulator_speed() {
    comm_exec_rvc300gnome232 "set_controller_speed($1)"
}

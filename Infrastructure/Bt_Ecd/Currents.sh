#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh


DAS="Oscilloscopes/RigolMSO5204-b/Driver"
DeviceList="$DAS"
cd `dirname $SW/Devices/$DAS`; source Driver.sh; cd $OLDPWD

LastChannelToAcq=3


diags=("" "U_Loop" "U_CurrPrb_Bt" "U_CurrPrb_Ecd" "Null")


#e.g. source Commons.sh ;OpenSessionSomewhere /golem/svoboda/Dirigent/Devices/Oscilloscopes/RigolMSO5104-a/Stabilization.sh


function OpenSession()
{
   echo "
CHANnel1:DISPlay ON;CHANnel1:PROBe 1;CHANnel1:SCALe 3;CHANnel1:OFFSet -10;
CHANnel2:DISPlay ON;CHANnel2:PROBe 1;CHANnel2:SCALe 0.5;CHANnel2:OFFSet -1.5;
CHANnel3:DISPlay ON;CHANnel3:PROBe 1;CHANnel3:SCALe 0.5;CHANnel3:OFFSet -1.5;
CHANnel4:DISPlay OFF;CHANnel4:PROBe 1;CHANnel4:SCALe 0.2;CHANnel4:OFFSet 0;
TIMebase:DELay:ENABle OFF;TIMebase:MAIN:SCALe 2e-3;TIMebase:MAIN:OFFSet 9e-3;
:STOP;:CLEAR;
:SYSTem:KEY:PRESs MOFF
:TRIGger:EDGE:SOURce CHANnel1;TRIGger:EDGE:SLOPe POS;TRIGger:EDGE:LEVel 6;
:MATH1:DISPlay ON;:MATH1:OPERator AXB; :MATH1:SOURce1 CHANnel1;:MATH1:SCALe 1; :MATH1:OFFSet 2
"|$scope_address
}


function PostDischargeAnalysis
{
#    Call $SW/Devices/RelayBoards/Quido8-a/TokamakSouth CurrentClampOFF
    GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s $basedir/Devices/`dirname $DAS/` DAS_raw_data_dir

    cp DAS_raw_data_dir/*.csv .
    
    issue="I_PotentDiff"
    paste -d, U_R-BiasEl.csv U_R-PowSup.csv |awk -F "," '{print $1","($4-$2)/20}' > $issue.csv
    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'$issue.csv'" w l t "'${issue/_/\\\\_}'"' > $issue.jpg;

    issue="I_CurrPrb"
    cat U_CurrPrb.csv |awk -F "," '{print $1","$2/(50e-3)}' > $issue.csv
    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "'$issue.csv'" w l t "'${issue/_/\\\\_}'"' > $issue.jpg;

    gnuplot -e 'set datafile separator ",";set terminal jpeg;plot "I_PotentDiff.csv" w l t "I\\_PotentDiff","I_CurrPrb.csv" w l t "I\\_CurrPrb"' > Result.jpg;
    
    homepage_row
      
}



function homepage_row
{
local cesta=`dirname $whoami`

    echo "<tr>
    <td valign=bottom><a href=$cesta/><img src=$cesta/name.png  width='$namesize'/></a></td>
    <td valign=bottom><a href=$cesta/expsetup.svg><img src=$cesta/setup.png /></a></td>
        
   
    <td valign=bottom><a href=Devices/`dirname $DAS`/ScreenShotAll.png><img src='Devices/`dirname $DAS`/ScreenShotAll.png'  width='$iconsize'/></a></td>
    <td>
    <a href=$gitlabpath/Devices/`dirname $DAS` title="Gitlab4`dirname $DAS`">$gitlabicon</a><br></br>
    <a href=Devices/`dirname $DAS` title="Data directory">$diricon</a><br></br>
    </td>
    
    <td>
    <a href=$cesta/Result.jpg><img src='$cesta/Result.jpg'  width='$iconsize'/></a>
    </td>
    <td>
    <a href=$cesta/Parameters/ title="Parameters">$parametersicon</a><br></br>
    <a href=$gitlabpath/$cesta/ title="Gitlab4$cesta">$gitlabicon</a>
    <a href=$cesta/ title="Directory">$diricon</a>
    <a href=$dbpath/Diagnostics/$diag_id/ title="Database">$sqlicon</a>
    </td>

    
    </tr>" > include.html
}

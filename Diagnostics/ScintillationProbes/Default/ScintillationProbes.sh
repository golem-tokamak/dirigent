BasePath=../..;source $BasePath/Commons.sh
source Universals.sh



DAS="Oscilloscopes/TektrMSO58-a/ScintillationProbes"


DeviceList="PowerSupplies/NIMcrate/HXRdiagnostics $DAS AVs/TapoC310-a/NIMcrate"


HXR_probes="CeBr_a YAP_a NaITl_a ZnSAg_a LYSO"
#(x,y,z, theta, phi, HV_PS@NIMcrate, HVchannel@NIMcrate, Voltage)
S_CeBr_a=(380 130 72 180 0 A 0 -700)
S_YAP_a=(380 130 72 180 0 A 1 -800)
S_NaITl_a=(380 130 72 180 0 A 2 -700)
S_ZnSAg_a=(380 130 72 180 0 A 3 -700)


LastChannelToAcq=7

diags=('U_Loop-b' 'YAP-a' 'CeBr-a' 'CeBr-b' 'NaITl-a' 'LYSO' 'GAGG' 'Trigger')

# CreateTable @Commons (Call from here ...)

function SetUpHV@NIMchannel()
{
local Voltage=$1
local Channel=$2

    LogIt "HV@NIM setup $Voltage @ $Channel"
    #ssh bn "cd /golem/Dirigent/Devices/PowerSupplies/NIMcrate/HXRdiagnostics/;source NIMcrate.sh;SetupHV@channel $Voltage $Channel"
    Call $SHM0/Devices/PowerSupplies/NIMcrate SetupHV@channel $Voltage $Channel
    #cd $SHM0/Devices/PowerSupplies/NIMcrate/HXRdiagnostics/;source NIMcrate.sh;SetupHV@channel $Voltage $Channel
}

SetUpHVs@NIM()
{
    for i in `seq 1 4`;do
        if [[ $(<Parameters/u_nim_a$i) > 10 ]]; then SetUpHV@NIMchannel $(<Parameters/u_nim_a$i) $((i-1));fi
    done
}


function GetReadyTheDischarge ()
{
  GeneralTableUpdateAtDischargeBeginning 
  SetUpHVs@NIM
  
  
}

function PostDischargeAnalysis()
{
  
   sleep 4
 #   GeneralDAScommunication $DAS RawDataAcquiring
     if [[ ! -d DAS_raw_data_dir ]]; then ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir; fi

#    ln -s ../../../Devices/$DAS/ DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    notebook='null'
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox
    #GenerateDiagWWWs $diag_id $setup_id $DAS # @Commons.sh
    NoAnalysis
    mkdir -p Results
    ln -s ../CeBr-a.csv Results/CeBr-a.csv
    ln -s ../CeBr-b.csv Results/CeBr-b.csv
    ln -s ../LYSO.csv Results/LYSO.csv
    ln -s ../NaITl-a.csv Results/NaITl-a.csv
    ln -s ../U_Loop-b.csv Results/U_Loop-b.csv
    ln -s ../GAGG.csv Results/GAGG.csv


}

function NoAnalysis()
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}


function Analysis()
{

:

}



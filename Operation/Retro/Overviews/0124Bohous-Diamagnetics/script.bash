from=43515
to=43695
soubor=index.html; 
system=Devices/Oscilloscopes/TektrMSO64-a
url=https://golem.fjfi.cvut.cz/shots/
#url=/shots

echo "<html><body><table>" > $soubor;

for i in `seq $from $to`; do echo -ne '*'$i:; 
if [[ -d "/golem/database/operation/shots/$i/$system" ]] && [[ $(< /golem/shots/$i/Diagnostics/PlasmaDetection/Results/b_plasma) != "0.000" ]]; then
echo -ne yes';'
echo "<tr>
<td><a href=$url/$i>$i</a><br>`cat /golem/database/operation/shots/$i/shot_date`<br>`cat /golem/database/operation/shots/$i/shot_time`</td>
<td><img src=$url/$i/Diagnostics/BasicDiagnostics/graph.png></td>
<td><img src=$url/$i/$system/rawdata.jpg><br/><center><a href=$url/$i/$system/TektrMSO64_ALL.csv>data</a></center></td>
</tr>" >> $soubor; else echo -ne no';';fi;done

echo "</table><hr/><h2>For generation bash script used</h2><pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;

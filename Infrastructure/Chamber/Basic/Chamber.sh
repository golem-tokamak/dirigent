#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source Universals.sh


DeviceList="Vacuums/RotaryPump/Basic Vacuums/TMP-a/Basic Vacuums/Valves-a/Basic  Vacuums/TMP-b/Basic Vacuums/Valves-b/Basic ITs/RasPi4-a/Chamber Interfaces/PapaGo2TC_ETH-a/ChamberTemperature Interfaces/Gnome232-a/VacuumGauge Infrastructure/Racks/Vacuum/Basic PowerSupplies/GWInstekGPR-a/GlowDischarge Accessories/CurrentClampMeters/FlukeI30-a/GlowDischarge PowerSupplies/GWInstekPSW-a/Working_Gas RelayBoards/Quido16-a/Vacuum"


for Service in Baking GlowDischarge Pumping; do
        source $Service.sh 
done

function OpenSession()
{
    echo We have to wait 30s until right time is set-up @ RASP
    for i in `seq 1 6`;do echo $((i*5)) s;sleep 5 ;done
    Call $SHMS/Devices/ITs/RasPi4-a VacuumLog  &
    sleep 5
    UpdateCurrentSessionDataBase "start_chamber_pressure=`cat $SHML/ActualChamberPressurePa`,start_for_vacuum_pressure=`cat $SHML/ActualForVacuumPressurePa`"
    
}

function SecurePostDischargeState()
{
    $LogFunctionGoingThrough
    echo "Discharge end" >>$SHML/PressureLog
    #cp $SHML/ActualChamberPressuremPa $SHM0/$SUBDIR/$ThisDev/final_chamber_pressure
    #echo 25> $SHM0/$SUBDIR/$ThisDev/final_chamber_temperature
    UpdateCurrentShotDataBase "p_chamber_pressure_after_discharge=`cat $SHML/ActualChamberPressuremPa`"
}

function PostDischargeAnalysis()
{

    gnuplot  -e "set xdata time;set timefmt '%H:%M:%S';set xtics format '%tH:%tM' time;set xlabel 'Time [h:m]';set ylabel 'chamber pressure p_{ch} [mPa]';set xrange [*:*];set title 'Session chamber logbook';set yrange [*:100];set y2range [0:200];set y2tics 0, 20,200;set y2label'Chamber temperature T_{ch} [^o C]';set ytics nomirror;set terminal jpeg size 1080,700;plot '$SHM/ChamberLog' using 1:2 title 'pressure' with lines axis x1y1,'$SHM/ChamberLog' using 1:3 title 'temperature' with lines axis x1y2" > SessionChamber_p_T_Logbook.jpg

    local cesta=Infrastructure/$instrument
    GenerateDiagWWWs
}

function GenerateDiagWWWs
{
 
echo "<h3>Chamber</h3><table><tr>

<td><a href=$cesta/port_$port.jpg title="Infrastructure placement"><img src=$cesta/port_$port.jpg width='$iconsize'/></a></td>
<td><img src="$cesta/setup.png" width="$iconsize"></td>

<td><a href=https://www.dropbox.com/$DropBox title="Photogallery@DropBox"><img src=$cesta/Overview-thumb.jpg /></a></td>


<td><a href="$cesta/SessionChamber_p_T_Logbook.jpg"><img src="$cesta/SessionChamber_p_T_Logbook.jpg" width="$iconsize"></a></td>
<td></td>

</tr></table>" > include.html

echo "<html>
  <head>
    <title></title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <style></style>
  </head>
<body>
<h1>Tokamak GOLEM chamber status</h1>
<img src="SessionChamber_p_T_Logbook.jpg">
</body>
</html>" >chamber.html
  


}

function RegisterChamberOpeningInDB()
{
        $psql_password;psql -c "INSERT into infrastructure.chamber (event,date,time, shot_no, session_id, chamber_pressure,  forvacuum_pressure, temperature,setup,mission) VALUES ('chamber:opening','`date '+%y-%m-%d'`','`date '+%H:%M:%S'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,`cat $SHML/ActualChamberPressuremPa`,`cat $SHML/ActualForVacuumPressurePa`, `cat $SHML/ActualChamberTemperature`, '`cat $SHM/session_setup_name`', '`cat $SHMS/session_mission`') " -q -U golem golem_database
        $psql_password;psql -c "INSERT into production.events (tag,timestamp,shot_no, session_id,description) VALUES ('chamber:opening','`date '+%Y-%m-%d'`', `cat $SHM/shot_no`, `cat $SHM/session_id`,'`cat $SHMS/session_mission`') " -q -U golem golem_database
}






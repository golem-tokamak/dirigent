#!/bin/bash


BasePath=../../../..;source $BasePath/Commons.sh


function PowerON()
{
    Call $RelayBoards PowerSupplyHVcontactor@PowerSupplies ON
}

function PowerOFF()
{
    Call $RelayBoards PowerSupplyHVcontactor@PowerSupplies OFF
} 

function ShortCircuitON()
{
    Call $RelayBoards ShortCircuits@PowerSupplies OFF
}

function ShortCircuitOFF()
{
    Call $RelayBoards ShortCircuits@PowerSupplies ON
} 

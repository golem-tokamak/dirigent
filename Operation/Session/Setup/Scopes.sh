#!/bin/bash
# sudo apt-get install gvncviewer

gvncviewer rigolmso5204-c.golem &
#google-chrome --new-window --app=http://192.168.2.132/auth/WebControl.html &
google-chrome --new-window --app=http://192.168.2.143/Tektronix/#/client/c/Tek%20e*Scope &

bash /golem/Dirigent/Operation/Session/Setup/StatusPanel.sh &


sleep 10
wmctrl -i -r `wmctrl -l|grep x11vnc|awk '{print $1}'` -b add,sticky #,above
wmctrl -i -r `wmctrl -l|grep 'Tek e'|awk '{print $1}'` -b add,sticky #,above
wmctrl -i -r `wmctrl -l|grep 'Session'|awk '{print $1}'` -b add,sticky #,above
wmctrl -i -r `wmctrl -l|grep 'Operational parameters'|awk '{print $1}'` -b add,sticky #,above
wmctrl -i -r `wmctrl -l|grep 'Operational state'|awk '{print $1}'` -b add,sticky #,above


exit

string="Toto je velmi dlouhý řetězec, který chceme zkrátit."
max_length=20  # Maximální délka řetězce
if (( ${#string} > max_length )); then
    prefix=${string:0:8}                     # Prvních 8 znaků
    suffix=${string: -8}                     # Posledních 8 znaků
    result="${prefix}...${suffix}"           # Spojení s "..."
else
    result="$string"
fi
echo "$result"

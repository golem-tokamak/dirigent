
#!/bin/bash

BasePath=../../../;source $BasePath/Commons.sh

source Universals.sh


function GetReadyTheDischarge ()
{
    mkdir -p Parameters
    # GeneralDiagnosticsTableUpdateAtDischargeBeginning $diag_id #@Commons.sh

    ssh -f golem@$ThisDev.golem 'python read_scope.py --ch0_range 1 --ch1_range 1 --samplerate 15000000 --ms_samples 30' > GetReadyTheDischarge.out 2> GetReadyTheDischarge.err

}

function Arming()
{
    ssh -f golem@$ThisDev.golem 'python read_scope.py --arming' > /dev/null  2> Arming.err
}


function RawDataAcquiring()
{ 
   
    scp golem@$ThisDev.golem:data.csv .
    ssh golem@$ThisDev.golem "del data.csv"

    python3 - <<'END_SCRIPT'

import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv('data.csv')
df.time*=1e3
df.set_index('time', inplace=True)

df.plot()

plt.xlabel("Time (ms)")
plt.ylabel("Amplitude [V]")
plt.grid(True)
plt.savefig('ScreenShotAll.png')

END_SCRIPT


}

#!/bin/bash


COMMAND="netcat -w 3 $ThisDev.golem 5555"
scope_address="nc -w 1 $ThisDev.golem 5555"


function Wake-upCall(){ 
:
 } 

function Sleep-downCall(){ 
:
}

function Arming()
{
	echo ":SINGLe"|$scope_address
}



function GetOscScreenShot()
{
	echo ":SYSTem:KEY:PRESs MOFF"|$scope_address
	mRelax
    echo ":DISPLAY:DATA?  ON,OFF,BMP"|$COMMAND|tail -c +12 > ScreenShotAll.bmp
    convert ScreenShotAll.bmp ScreenShotAll.png
    convert -resize 200x200 ScreenShotAll.png rawdata.jpg
    rm ScreenShotAll.bmp 
}


function AdjustHorizontalScale() {
    local recordMS=$1
        horizontalScale=`echo ":TIMebase:MAIN:SCALe?" | $COMMAND`
        horizontalOffset=`echo ":TIMebase:MAIN:OFFSet?" | $COMMAND`
    if [[ "$horizontalScale" != "" ]] && [[ "$horizontalOffset" != "" ]] ; then
        preTrigger=`python3 -c "hs=float('$horizontalScale');ho=float('$horizontalOffset');print('{:.3e}'.format(5*hs-ho))"`
        newHorizontalScale=`python3 -c "pt=float('$preTrigger');rec=float('$recordMS')/1e3;print('{:.3e}'.format((pt+rec)/10))"`
        newHorizontalOffset=`python3 -c "pt=float('$preTrigger');hs=float('$newHorizontalScale');print('{:.3e}'.format((5*hs-pt)))"`
        echo $newHorizontalOffset
        echo ":TIMebase:SCALe $newHorizontalScale" | $COMMAND
        echo "Setting TIMebase:SCALe $newHorizontalScale @ $ThisDev"
        echo ":TIMebase:OFFSet $newHorizontalOffset" | $COMMAND
        echo "Setting TIMebase:OFFSet $newHorizontalOffset @ $ThisDev"
    else
        echo "AdjustHorizontalScale @ $ThisDev FAILED"
    fi   
}




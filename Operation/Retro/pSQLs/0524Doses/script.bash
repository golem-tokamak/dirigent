export PGPASSWORD=$(</golem/production/psql_password);
for i in `seq 45000 45130`; do \
    dir=/golem/shots/$i/Diagnostics/BasicDiagnostics/Results;\
    echo -ne $doing $i;\
    if [[ -s $dir/d_fluke ]]; then \
        Dose=`echo $(<$dir/d_fluke)*100/1|bc`;\
        echo '->' $Dose; \
        psql -c "UPDATE operation.discharges SET "\"D_integral_dose\"=`echo $(<$dir/d_fluke)*100/1|bc`" WHERE shot_no=$i" -q -U golem golem_database;
    fi;
    echo
done


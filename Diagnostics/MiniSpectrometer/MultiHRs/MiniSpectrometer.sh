BasePath=../..;source $BasePath/Commons.sh 
source Universals.sh

DAS="Radiation/MiniSpectrometer/HRs"

#DeviceList="$DAS ITs/Nb-Master/MiniSpectrometer Tools/Lights/Ceiling/Darkness"
DeviceList="$DAS ITs/Nb-Master/MiniSpectrometer"







function PostDischargeAnalysis()
{
    notebook=RawDataGraph
    HotFix=svoboda@Nb-Master


    ln -s ../../Devices/`dirname $DAS` DAS_raw_data_dir
    cd DAS_raw_data_dir

    #scp golem@$PC:/home/golem/MiniSpectrometers/VIS_TG_0.h5 Spectrometer_vis_0.h5
    scp golem@$PC:/home/golem/MiniSpectrometers/*.h5 .
    scp golem@$PC:/home/golem/MiniSpectrometers/SettingOut.json .
    scp golem@$PC:/home/golem/MiniSpectrometers/DumpedCommunication.txt .
    cat /golem/shm_golem/shot_no > ShotNo

    scp $notebook.ipynb *.h5 Setting.json ShotNo $HotFix:/home/svoboda/Downloads/HotFix.Dg/
    ssh $HotFix "cd /home/svoboda/Downloads/HotFix.Dg; /home/svoboda/.local/bin/jupyter-nbconvert --execute $notebook.ipynb --to html --output $notebook.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)"
    scp $HotFix:/home/svoboda/Downloads/HotFix.Dg/*.png .
    scp $HotFix:/home/svoboda/Downloads/HotFix.Dg/*.html .
    convert -resize $icon_size ScreenShotAll.png rawdata.jpg
    for i in *.h5;do echo $i;h5dump $i > `basename $i .h5`.txt; done;

    cd $OLDPWD




    cp DAS_raw_data_dir/RawDataGraph.html analysis.html
    cp DAS_raw_data_dir/RawDataGraph.ipynb .
#    notebook='null'
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox
    #Analysis
}

#CO=Kvartet;source Commons.sh;Call $SHMS/Diagnostics/BasicDiagnostics4TrainCourses/ TeacherScopesSetting

function DoItEverywhere()
{
DoWhat=$2
Where=$1
    for i in $DeviceList; do 
    (echo Doing $DoWhat @ $(dirname $i): ;cd $Where/Devices/$(dirname $i);source $(dirname $i|xargs basename).sh;$DoWhat  ;cd - ) &
    sleep 0.2s
    done
    wait

}

function OpenSession()
{
    mkdir -p $SHMS/Diagnostics/BasicDiagnostics4TrainCourses/commons/
    cp $SW/Diagnostics/BasicDiagnostics4TrainCourses/commons/*.* $SHMS/Diagnostics/BasicDiagnostics4TrainCourses/commons/
    cp $SW/Diagnostics/BasicDiagnostics4TrainCourses/*.* $SHMS/Diagnostics/BasicDiagnostics4TrainCourses/
#    DoItEverywhere  $SW OpenSession #BACHA, hodne nbezpecne
}


function GetReadyTheDischarge
{
    mkdir -p $SHM0/Diagnostics/BasicDiagnostics4TrainCourses/commons/
    cp $SW/Diagnostics/BasicDiagnostics4TrainCourses/commons/*.* $SHM0/Diagnostics/BasicDiagnostics4TrainCourses/commons/
    cp $SW/Diagnostics/BasicDiagnostics4TrainCourses/*.* $SHM0/Diagnostics/BasicDiagnostics4TrainCourses/
}


#function Arming()
#{
#    DoItEverywhere $SW Arming #BACHA, hodne nbezpecne
#}


function CommonPostDischargeAnalysis()
{
    ln -s ../../Devices/`dirname $DAS/` DAS_raw_data_dir-$line
    Analysis
    convert -resize $icon_size icon-fig-$line.png graph-$line.png
}

function NoAnalysis()
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig-$line.png
}

function Analysis()
{
local ThisDev=$1

    sed "s/shot_no\ =\ 0/shot_no\ =\ `cat /dev/shm/golem/shot_no`/g" /golem/Dirigent/Diagnostics/BasicDiagnostics4TrainCourses/commons/StandardDAS.ipynb > StandardDAS.ipynb
    sed -i "s/Rigol_line\ =\ 'xy'/Rigol_line\ =\ \'$ThisDev\'/g" StandardDAS.ipynb
    jupyter-nbconvert --execute StandardDAS.ipynb --to html --output analysis-$ThisDev.html > >(tee -a jup-nb_stdout.log) 2> >(tee -a jup-nb_stderr.log >&2)

}

# golem@Dirigent>BP=Diagnostics/BasicDiagnostics4TrainCourses;cp /golem/Dirigent/$BP/Universals.sh /golem/database/operation/shots/41503/$BP/;cp /golem/Dirigent/$BP/commons.* /golem/database/operation/shots/41503/$BP/commons/;Dg -r pda 41503 Diagnostics/BasicDiagnostics4TrainCourses/Kvartet




function TeacherScopesSetting()
{
    DoItEverywhere  $SHMS CommonSampleSetting
}


function HtmlGenerat()
{
echo "<h3><b>Training courses</b></h3>"> diagrow.html

for i in $DeviceList; 
do TheDevice=$(basename $i)
SHOT_NO=`cat $SHM/shot_no`
#echo $TheDevice

ln -s ../../../Devices/Oscilloscopes/$TheDevice DAS_raw_data_dir-$TheDevice


NoAnalysis $TheDevice


echo "<table><tr>
<td><img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/setup.png'/></td>
<td><img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/das.jpg'/></td>
<td><a href=http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/ScreenShotAll.png>
<img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`"/rawdata.jpg'/></a></td>
<td><a href=http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Devices/"`dirname $i`">$diricon</a></td>">>diagrow.html;


if [ -f /golem/database/operation/shots/$SHOT_NO/Diagnostics/BasicDiagnostics4TrainCourses/Kvartet/icon-fig-$TheDevice.png ]; then 
echo "<td><a href=http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/Kvartet/icon-fig-$TheDevice.png>
<img src='http://golem.fjfi.cvut.cz/shotdir/`cat $SHM/shot_no`/Diagnostics/BasicDiagnostics4TrainCourses/Kvartet/icon-fig-$TheDevice.png' width=120/></a></td>">>diagrow.html;
fi

echo "</tr></table>">>diagrow.html;

done



}


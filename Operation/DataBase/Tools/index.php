<html>
<body>
<h2>Tag functions</h2>
QUERRY:<b>SELECT  * FROM diagnostics.basicdiagnostics WHERE $condition ORDER BY $order $direction</b>
<table border=1>
<tr>
<th>Name</th><th>order</th><th>direction</th><th>condition</th><th>how many figs<br></br> with width</th><th>link</th><th>count</th>
</tr>
<?php
require_once('../db_header.php');

function CountIt($condition)
{
global $db;
return pg_num_rows(pg_query($db,'SELECT shot_no FROM diagnostics.basicdiagnostics WHERE t_plasma_duration IS NOT  NULL '.$condition ));
}

// CountIt('AND tag_valid = true AND tag_standard = false AND tag_strange = false');

$direction='DESC';
// ============ ============
echo "<tr><td colspan='6'><b>Tag Discharges utils</b></td></tr>";
$condition="AND tag_valid = true AND tag_standard = false AND tag_strange = false";$howmany=1;$width=30;
$order='t_plasma_duration';
echo "<tr><td>Sorted by Longest discharge</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
$order='shot_no';
echo "<tr><td>Sorted by Latest discharge</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
// ============ ============
echo "<tr><td colspan='6'><b>Tagged lists:</b></td></tr>";
$howmany=20;$width=20;
$condition="AND tag_valid = true AND tag_standard = true AND tag_strange = false";
$order='t_plasma_duration';
echo "<tr><td>Sort by Longest discharge</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
$order='shot_no';
echo "<tr><td>Sort by Latest discharge</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
$condition="AND tag_valid = true AND tag_dbl_bd = true AND tag_strange = false";
echo "<tr><td>Double breakdowns</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
$condition="AND tag_valid = true AND tag_dbl_trp = true AND tag_strange = false";
echo "<tr><td>Tripple breakdowns</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
// ============ ============
echo "<tr><td colspan='6'><b>UnTagged lists:</b></td></tr>";
$condition="";$howmany=20;$width=20;
$order='t_plasma_duration';
echo "<tr><td>Longest discharge</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
$order='shot_no';
echo "<tr><td>Latest discharge</td><td>$order</td><td>$direction</td><td>$condition</td><td>$howmany with $width%</td><td><a href='http://golem.fjfi.cvut.cz/DataBase/Tools/TagPlasmaRegimes.php?order=$order&direction=$direction&howmany=$howmany&width=$width&condition=$condition'>here</a></td><td>".CountIt($condition)."</td></tr>";
?>
</table>
</body>
</html>


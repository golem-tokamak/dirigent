#!/bin/bash

DAS="DASs/Papouch-Ko/RakeProbe"
BasePath=../..;source $BasePath/Commons.sh
source Universals.sh


diag_id=RakeProbe

#DeviceList="RelayBoards/Energenie_LANpower-a/PowerSupply4DASs $DAS"
DeviceList="$DAS Tools/SilverBox-a/RakeProbe Tools/ElectricalManipulator-a/Basic AVs/D-Linkcamera-a/ManipulatorVert AVs/D-Linkcamera-b/ManipulatorAng Interfaces/Motor_Controller-a/ElectricalManipulator-a Interfaces/Motor_Controller-b/ElectricalManipulator-a"

LastChannelToAcq=10
# () oznaceni kabelu z RP
diags=('RP01' 'RP02' 'RP03' 'RP04' 'RP05' 'RP06' 'RP07' 'RP08' 'RP09' 'RP10')


function GetReadyTheDischarge
{
    GeneralTableUpdateAtDischargeBeginning
    Call $SHM0/Devices/Tools/ElectricalManipulator-a SetPosition $(<Parameters/r_manipulator)
}



function PostDischargeAnalysis() 
{

    #GeneralDAScommunication $DAS RawDataAcquiring $LastChannelToAcq
    ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done        

    Analysis
    notebook='null'
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox

    Call $SHM0/Devices/Tools/ElectricalManipulator-a Homing

   
}

function NoAnalysis
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png

}


function Analysis
{
    local whoami="FilPapRakeProbe"
    # finds 'shot_no = 0' and replaces the 0 with the current shot number
    # in the specified jupyter notebook
    sed -i "s/shot_no\ =\ 0/shot_no\ =\ $(cat $SHMS/shot_no)/g" $whoami.ipynb

    # creates analysis.html from the given .ipynb file
    # appends stdout to jup-nb_stdout.log and print it in console
    # appends stderr to jup-nb_stderr.log and print it in console
    
    IfNoPlasmaThenReturn
    
    jupyter-nbconvert --ExecutePreprocessor.timeout=30 \
                      --to html \
                      --execute $whoami.ipynb \
                      --output analysis.html \
                        > >(tee -a jup-nb_stdout.log) \
                       2> >(tee -a jup-nb_stderr.log >&2) #Lokalni vypocet

    convert -resize $icon_size icon-fig.png graph.png
}


BasePath=../..;source $BasePath/Commons.sh


function RetroCorrection ()
{
filetoreplace=BasicDiagnostics.ipynb
shotdir=analysis_wave_i/BasicDiagnostics
dirigentdir=Analysis/BasicDiagnostics
script=BasicDiagnostics.sh
command=PostDischargeFinals 
do=echo #to see/test
do= #to run

  for i in `seq 32223 32231`; do
    echo Doing ... $i;
    $do cp $shot_dir/$i/$shotdir/$filetoreplace $shot_dir/$i/$shotdir/$filetoreplace.VersionUpTo`date '+%y%m%d'`
    $do cp $shot_dir/$i/$shotdir/$script $shot_dir/$i/$shotdir/$script.VersionUpTo`date '+%y%m%d'`
    $do cp $dirigent_dir/$dirigentdir/$filetoreplace $shot_dir/$i/$shotdir/$filetoreplace
    $do cp $dirigent_dir/$dirigentdir/$script $shot_dir/$i/$shotdir/$script
    $do cd $shot_dir/$i/$shotdir/
    $do source `basename $script`;
    $do $command
    $do cd ~-
  done
}
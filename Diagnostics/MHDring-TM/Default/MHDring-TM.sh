#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh 
source Universals.sh

DAS="DASs/NI-squid/MHDring-TM"
#DAS_ADD="DASs/Papouch-St/MHDring-TM-ADD"

DeviceList="$DAS"
#$DeviceList="$DAS $DAS_ADD"

LastChannelToAcq=6
#diags=('U_loop' 'U_rogowski' 'U_Bt_HFS' 'U_Bt_LFS' 'U_diam_inner' 'U_diam_outer')

function PostDischargeAnalysis()
{
    #16 coils RING:
    if [[ ! -d DAS_raw_data_dir ]]; then ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir; fi
    # Bacha, karty jsou zrejme prehozene!
    #for i in `seq 2 17`; do tail -n +2 DAS_raw_data_dir/rawData.csv | awk -F ',' '{print $1","'\$$i'}' > ch$((i-1)).csv;done
    for i in `seq 2 9`; do tail -n +2 DAS_raw_data_dir/rawData.csv | awk -F ',' '{print $1","'\$$i'}' > ring_$((i-1+8)).csv;done
    for i in `seq 10 17`; do tail -n +2 DAS_raw_data_dir/rawData.csv | awk -F ',' '{print $1","'\$$i'}' > ring_$((i-1-8)).csv;done

    #Additional coils:
    #if [[ ! -d DAS_raw_data_dir-ADD ]]; then ln -s $BasePath/Devices/`dirname $DAS_ADD` DAS_raw_data_dir-ADD; fi
    #for i in `seq 1 $LastChannelToAcq` ; do
    #    cp DAS_raw_data_dir-ADD/ch$i.csv ${diags[$i-1]}.csv
    #done

    GenerateDiagWWWs
    #add_coils

}


function add_coils
{

echo "<table><tr>
    <td valign=bottom>&nbsp;
    <a href=Diagnostics/MHDring-TM/port_N_C.jpg title=Diagnostics placement><img src=Diagnostics/MHDring-TM/port_N_C.jpg width='200'/></a>
    </td>
    <td valign=bottom>&nbsp;<a href=Diagnostics/MHDring-TM/expsetup.svg><img src=Diagnostics/MHDring-TM/setup.png width='200' /></a></td>
    <td valign=bottom>&nbsp;
    <a href=https://www.dropbox.com/ title=Photogallery@DropBox><img src=Diagnostics/MHDring-TM/Overview-thumb.jpg /></a>
    </td>

    <td valign=bottom>&nbsp;<a href=Devices/DASs/Papouch-St/das.html><img src=Devices/DASs/Papouch-St/das.jpg  width='200'/></a></td>
    <td valign=bottom>&nbsp;<a href=Devices/DASs/Papouch-St/ScreenShotAll.png><img src=Devices/DASs/Papouch-St/rawdata.jpg  width='200'/></a></td>
</tr></table>">> diagrow.html

}





















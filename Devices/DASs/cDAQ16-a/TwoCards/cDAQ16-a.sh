#!/bin/bash

source ../../../Commons.sh

function Wake-upCall { Call $RelayBoards FastSpectrometry@TokamakSockets ON; }

function GetReadyTheDischarge { :; }

function SecurePostDischargeState { :; }

function Sleep-downCall { Call $RelayBoards FastSpectrometry@TokamakSockets OFF; }

DataFile=/home/amos/nicDAQ/outfile.csv


function Arming()
{
  ssh amos@TimepixPC -f "python3 nicDAQ/read_cdaq.py --card '/cDAQ9185-22002ABMod1' --columns 'ch1' 'ch2' 'ch3' 'ch4' --nsamples 3000 --samplerate 100000 -o nicDAQ/outfileCard1.csv" >StandardLog 2>ErrorLog
  ssh amos@TimepixPC -f "python3 nicDAQ/read_cdaq.py --card '/cDAQ9185-22002ABMod3' --columns 'ch5' 'ch6' 'ch7' 'ch8' --nsamples 3000 --samplerate  50000 -o nicDAQ/outfileCard3.csv" >>StandardLog 2>>ErrorLog
}


function RawDataAcquiring()
{
    scp amos@TimepixPC:nicDAQ/outfileCard1.csv .
    scp amos@TimepixPC:nicDAQ/outfileCard3.csv .
    ssh amos@TimepixPC "rm /home/amos/nicDAQ/outfileCard1.csv"
    ssh amos@TimepixPC "rm /home/amos/nicDAQ/outfileCard3.csv"

    for i in `seq 2 5`; do tail -n +2 outfileCard1.csv|awk -F ',' '{printf "%5.4g,%5.3f\n", $1, '\$$i'}' > ch$((i-1)).csv;done
    for i in `seq 2 5`; do tail -n +2 outfileCard3.csv|awk -F ',' '{printf "%5.4g,%5.3f\n", $1, '\$$i'}' > ch$((i+3)).csv;done

}

function RawDataAcquiring_ZMB()
{
    local shot_no=$(<$SHMS/shot_no)
    
    timeout=30
    while ssh amos@TimepixPC.golem "! test -s $DataFile";
    do
        if [ "$timeout" == 0 ]; then
        LogItColor 1 "ERROR: Timeout while waiting for the file from TimepixPC"
        exit 1
    fi
    sleep 1
    echo $timeout s to wait for TimepixPC files
    ((timeout--))
    done

        
    #shot_no=`cat $BASEDIR/shot_no`
    LogItColor 4 "TimepixPC: Start of acquiring"
    scp -o ConnectTimeout=1 amos@TimepixPC.golem:$DataFile .; \
     
    
    rm tmp
    echo  "set terminal png size 1600, 1000;set output 'ScreenShotAll.png';set bmargin 0;set tmargin 0;set lmargin 10;set rmargin 3;set xtics;unset xlabel;set multiplot layout 4,1 columnsfirst title 'GOLEM Shot XY';set xrange [*:*];set yrange [*:*];set style data dots;set ytics;set datafile separator ','"> tmp; for i in `seq 2 3`; do echo "set ylabel '$((i-1))';plot 'outfile.csv' u 1:$((i)) t '' w l lc 1;" >> tmp;done;cat tmp|gnuplot 1>/dev/null 2>/dev/null
   	convert -resize 200x200 ScreenShotAll.png rawdata.jpg

    LogIt "TimepixPC: End of acquiring"


}

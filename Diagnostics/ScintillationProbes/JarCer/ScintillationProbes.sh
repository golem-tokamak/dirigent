BasePath=../..;source $BasePath/Commons.sh



DAS="Oscilloscopes/TektrMSO58-a/ScintillationProbesHiRes2"


DeviceList="PowerSupplies/NIMcrate/HXRdiagnostics $DAS AVs/TapoC310-a/NIMcrate"


HXR_probes="CeBr_a YAP_a NaITl_a ZnSAg_a LYSO"
#(x,y,z, theta, phi, HV_PS@NIMcrate, HVchannel@NIMcrate, Voltage)
S_CeBr_a=(380 130 72 180 0 A 0 -700)
S_YAP_a=(380 130 72 180 0 A 1 -800)
S_NaITl_a=(380 130 72 180 0 A 2 -700)
S_ZnSAg_a=(380 130 72 180 0 A 3 -700)


LastChannelToAcq=6

diags=('U_Loop-b' 'YAP-a' 'CeBr-a' 'CeBr-b' 'NaITl-a' 'LYSO' 'Trigger')

# CreateTable @Commons (Call from here ...)

function SetUpHV@NIMchannel()
{
local Voltage=$1
local Channel=$2

    LogIt "HV@NIM setup $Voltage @ $Channel"
    Call $SHM0/Devices/PowerSupplies/NIMcrate SetupHV@channel $Voltage $Channel

    #ssh bn "cd /golem/Dirigent/Devices/PowerSupplies/NIMcrate/HXRdiagnostics/;source NIMcrate.sh;SetupHV@channel $Voltage $Channel"
}

SetUpHVs@NIM()
{
    for i in `seq 1 4`;do
        if [[ $(<Parameters/u_nim_a$i) > 10 ]]; then SetUpHV@NIMchannel $(<Parameters/u_nim_a$i) $((i-1));fi
    done
}


function GetReadyTheDischarge ()
{
  GeneralTableUpdateAtDischargeBeginning $family.$diag_id #@Commons.sh
  SetUpHVs@NIM
  
  
}

function PostDischargeAnalysis()
{
  
   sleep 4
 #   GeneralDAScommunication $DAS RawDataAcquiring
    ln -s ../../../Devices/$DAS/ DAS_raw_data_dir

    for i in `seq 1 $LastChannelToAcq` ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-1]}.csv
    done    

    GenerateDiagWWWs $instrument $setup $DAS
    #GenerateDiagWWWs $diag_id $setup_id $DAS # @Commons.sh
    NoAnalysis
}

function NoAnalysis()
{
    cp $SW/Management/imgs/Commons/WithoutAnalysis.png icon-fig.png
    convert -resize $icon_size icon-fig.png graph.png
}


function Analysis()
{

:

}



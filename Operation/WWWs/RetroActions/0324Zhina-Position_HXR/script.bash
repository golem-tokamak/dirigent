from=44000
to=44164
soubor=index.html; 
url=https://golem.fjfi.cvut.cz/shots/
t_plasma_duration_limit=7
title='Shots with plasma position measurement & HXR activity'
#url=/shots

echo "<html><body>
<h1>$title</h1>
<h2>From #$from to #$to with t_plasma_duration_limit=$t_plasma_duration_limit</h2>
<table>" > $soubor;
echo "<tr><th>ShotNo</th><th>Basic diagnostics</th><th>HXR diagnostics</th><th>Fast cameras</th><th>Plasma position</th></tr>" >> $soubor;
Counter=0
for i in `seq $from $to`; do echo -ne '*'$i:; 
if [[ `echo $(</golem/shots/$i/Diagnostics/PlasmaDetection/Results/t_plasma_duration)|bc|tr -d -` > $t_plasma_duration_limit ]] \
&& [[ -e "/golem/shots/$i/Devices/Oscilloscopes/TektrMSO58-a/ScreenShotAll.png" ]] \
&& [[ -e "/golem/shots/$i/Devices/AVs/PhotronMiniUX100-a/ScreenShotAll.png" ]] \
&& [[ -e "/golem/shots/$i/Diagnostics/FastCameras/icon-fig.png" ]] \
; then
    let Counter++ #increment
    echo -ne yes';'
    echo "<tr>
    <td><a href=$url/$i>$i</a><br>`cat /golem/shots/$i/shot_date`<br>`cat /golem/shots/$i/shot_time`</td>
    <td><img src=$url/$i/Diagnostics/BasicDiagnostics/graph.png></td>
    <td><img src=$url/$i/Devices/Oscilloscopes/TektrMSO58-a/rawdata.jpg></td>
    <td><img src=$url/$i/Devices/AVs/PhotronMiniUX100-a/rawdata.jpg></td>
    <td><img src=$url/$i/Diagnostics/FastCameras/graph.png></td>
    </tr>" >> $soubor;
else echo -ne no';';fi;done
echo "</table>Alltogether $Counter discharges<hr/>
<h2>For generation bash script used</h2>
<pre>
`cat script.bash|sed 's/</\&lt;/g'|sed 's/>/\&gt;/g'`
</pre></body></html>" >> $soubor;




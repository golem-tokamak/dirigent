	  <?php
if (bare_psql_shots_request('b_plasma') != 0) {
    echo "<h2>&nbsp;&nbsp;&nbsp;&nbsp;Plasma: <a href=".$gitlabpath."/Diagnostics/PlasmaDetection/DetectPlasma.ipynb title=\"Python code@Jupyter notebook\">".$pythonicon."</a>
    ".$rightarrowicon."
    <a href=Diagnostics/PlasmaDetection/DetectPlasma.html title=\"Results\">".$resultsicon."</a>
    <a href=Diagnostics/PlasmaDetection/Results title=\"Results\">".$diricon."</a>";
    echo "</h2><ul>";
    echo "<li>Plasma: <a href=Diagnostics/PlasmaDetection/Results/b_plasma title=\"Plasma Existence YES(1)/NO(0)\">yes or no: &#9745</a></li>";
    echo "<li>Time parameters:";    
    printf_href_express_quantity ('Diagnostics/PlasmaDetection/Results','t_plasma_duration','%3.2f', 'operation.discharges');
    echo '<a href="https://golem.fjfi.cvut.cz/tools/Chart/chart.php?schema=diagnostics&table=basicdiagnostics&column=t_plasma_duration&from=';
    echo ($shot_no-100);
    echo '&to=';
    echo ($shot_no);
    echo '&b_plasma=0" title="values from 100 last discharges">  '.$historyicon_small.'</a>';
    echo " (from:";    
    printf_href_express_quantity ('Diagnostics/PlasmaDetection/Results','t_plasma_start','%3.2f', 'operation.discharges');
    echo ",to:";    
    printf_href_express_quantity ('Diagnostics/PlasmaDetection/Results','t_plasma_end','%3.2f', 'operation.discharges');
    echo ")</li></ul>";
    echo"<h2>&nbsp;&nbsp;&nbsp;&nbsp;Plasma parameters:<a href=".$gitlabpath."/Diagnostics/BasicDiagnostics/Basic/StandardDAS.ipynb title=\"Python code@Jupyter notebook\">".$pythonicon."</a>
    ".$rightarrowicon."
    <a href=Diagnostics/BasicDiagnostics/Basic/analysis.html title=\"Results\">".$resultsicon."</a>
    <a href=Diagnostics/BasicDiagnostics/Basic/Results title=\"Results\">".$diricon."</a> ";
    echo "</h2><ul>";
    echo "<li>Loop voltage:";
    echo "\(\overline{U\sub{loop}}\)"; # Hnusny workarround, fakt nevim, co se deje ...
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','U_loop_mean','%3.2f', 'diagnostics.basicdiagnostics');
    echo "; ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','U_loop_max','%3.2f', 'diagnostics.basicdiagnostics');
    echo "; ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','U_loop_breakdown','%3.2f', 'diagnostics.basicdiagnostics');
    echo "</li>";
    echo "<li>Toroidal magnetic field: ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','Bt_mean','%3.2f', 'diagnostics.basicdiagnostics');
    echo "; ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','Bt_max','%3.2f', 'diagnostics.basicdiagnostics');
    echo "</li>";
    echo "<li>Plasma current: ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','Ip_mean','%3.2f', 'diagnostics.basicdiagnostics');
    echo "; ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','Ip_max','%3.2f', 'diagnostics.basicdiagnostics');
    echo "; ";
    printf_href_express_quantity('Diagnostics/BasicDiagnostics/Basic/Results','t_Ip_max','%3.2f', 'diagnostics.basicdiagnostics');
    echo "</li></ul>";

    //parameters_quantity_item ('power_balance');
    //parameters_quantity_item ('safety_factor');
    //parameters_quantity_item ('electron_temperature');
    //parameters_quantity_item ('electron_density_mean');
    }
    else
    {
    echo "<h2>&nbsp;&nbsp;&nbsp;&nbsp;<a href=Diagnostics/PlasmaDetection/Results/b_plasma title=\"Plasma Existence YES(1)/NO(0)\">&#9746</a>";
    echo"No Plasma:
    <a href=".$gitlabpath."/Diagnostics/PlasmaDetection/DetectPlasma.ipynb title=\"Python code@Jupyter notebook\">".$pythonicon."</a>
    ".$rightarrowicon."
    <a href=Diagnostics/PlasmaDetection/DetectPlasma.html title=\"Results\">".$resultsicon."</a>";
    }
    ?>

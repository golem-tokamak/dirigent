#!/bin/bash

BasePath=../..;source $BasePath/Commons.sh
source Universals.sh
DAS="DASs/cDAQ16-a/FullSteam"
DeviceList="ITs/TimepixPC/cDAQ16-a $DAS"

notebook='null'
columncount=4
columns="1 2 3 4"
firstcolumn=1
diags=('U_whole' 'U_Halpha' 'U_Hbeta' 'U_HeI')






function PostDischargeAnalysis()
{

     if [[ ! -d DAS_raw_data_dir ]]; then ln -s $BasePath/Devices/`dirname $DAS` DAS_raw_data_dir; fi
    
    for i in $columns ; do
        cp DAS_raw_data_dir/ch$i.csv ${diags[$i-$firstcolumn]}.csv
    done
    
    Gnuplot_BasicGraph #@Commons
    GenerateDiagWWWs $instrument $setup $DAS $notebook $port $DropBox

    mkdir -p Results
    for file in "${diags[@]}"; do
         ln -s ../$file.csv Results/$file.csv
    done
}





import sys
import json
from functools import partial
import pathlib
import argparse

import jinja2


def find_folder(root_obj, name):
    if (root_obj.get('type') == 'folder'
        and root_obj.get('name') == name):
        return root_obj
    elif 'children' in root_obj:
        descended = filter(lambda o: o is not None,
                           map(partial(find_folder, name=name), root_obj['children']))
        return next(descended, None)  # return first reult if there is one, None otherwise
    else:
        return None             # it is not this one


parser = argparse.ArgumentParser(description='Convert Chrom(ium) bookmarks to html',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('input', help='input file in JSON format')
parser.add_argument('--output', help='output file name', default='bookmarks.html')
parser.add_argument('--folder', help='bookmarks folder to select', default='Experiments')
parser.add_argument('--root', help='bookrams root source', default='bookmark_bar')
parser.add_argument('--template', help='output file name', default='bookmarks_tpl.html')

if __name__ == '__main__':
    args = parser.parse_args()
    data = json.load(open(args.input))
    showroom_folder = find_folder(data['roots'][args.root], args.folder)

    this_dir = pathlib.Path(__file__).parent.resolve()
    jenv = jinja2.Environment(loader=jinja2.FileSystemLoader(str(this_dir)),
                              trim_blocks=True)
    tpl = jenv.get_template(args.template)
    with open(args.output, 'w') as fout:
        fout.write(tpl.render(tree=showroom_folder))




